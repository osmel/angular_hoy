tsc --target ES2016 --experimentalDecorators

Decoradores
  https://luisjordan.net/angular/decoradores-angular-cosas-necesarias-debemos-conocer/
  https://videotutoriales.com/illasaron/2016/08/24/015-curso-de-angular-2-material-de-apoyo/

  https://code.i-harness.com/es/docs/typescript/handbook/decorators

  https://desarrolloweb.com/articulos/decorador-componentes-angular2.html
  https://www.mmfilesi.com/blog/typescript-y-7-decorators/
  

  http://blog.enriqueoriol.com/2018/02/decorador-analytics.html
  https://code.i-harness.com/es/docs/typescript/handbook/decorators
  https://es.linkedin.com/learning/ionic-esencial/los-decoradores-de-typescript


Como esta en fase de experimentación, hay que habilitar
  "experimentalDecorators": true, 

Qué es un decorador
        Son un modo de añadir metadatos a declaraciones de clases para hacer usados por inyecciones de dependencias o compilaciones de directivas.
        Al crear decoradores lo que estamos es definiendo anotaciones especiales, que pueden tener un impacto en el modo en el que se comportan nuestras clases, o los metodos o funciones que incluyan, pueden tambien simplemente alterar los datos que hemos definido en campos o parametros de esas clases. 
        Por tanto podemos considerar los decoradores como un modo muy poderoso de aumentar las funcionalidades nativas de nuestros tipos, sin tener para ellos que crear sus clases o que tengan que heredar de otros tipos

        Son reconocidos porque comienzan por una "@" y a continuación tienen un nombre. Localizados como declaraciones independientes que se colocan sobre el elemento que van a decorar(clases, métodos, accessor, propiedades y parámetros), incluyendo dentro del decorador un metodo o no. 


        Podemos definir 4 tipos diferentes de decoradores, dependiendo del tipo de elemento que esten destinados a decorar:
         - clases
         - propiedades 
         - métodos
         - parámetros
         - accesorios



     Decorador de clases(Class Decorator): 
         - Los decoradores de clase se aplican para la clase.
         - Se aplica al constructor de la clase y se puede usar para observar, modificar o reemplazar una definición de clase. 
         - Un decorador de clase no se puede usar en un archivo de declaración ni en ningún otro contexto ambiental (como en una clase de declare ).

     	 - La expresión para el decorador de clase se llamará como una función en tiempo de ejecución, con el constructor de la clase decorada como su único argumento.
	
		 -Si el decorador de clase devuelve un valor, reemplazará la declaración de clase con la función de constructor proporcionada.
















         Ese nombre es el de aquello que queramos decorar, que ya tiene que existir previamente. Podríamos decorar una función, una propiedad de una clase, una clase, etc.


		Un decorador es un tipo especial de declaración que se puede adjuntar a una declaración de clases, métodos, accessor, propiedades y parámetros . Los decoradores usan la forma @expression , donde la expression debe evaluar a una función que se llamará en el tiempo de ejecución con información sobre la declaración decorada.


		function sealed(target) {
		  // algo con 'target' ...
		}


Fábrica de decoradores(Decorador Factories)

	   Si queremos personalizar cómo se aplica un decorador a una declaración, podemos escribir una "fábrica decoradora". Una Decorator Factory es simplemente una función que devuelve la expresión que el decorador llamará en el tiempo de ejecución.


	   function color(value: string) { // esta es una fabrica de decoradores
		  
		  return function (target) { // este es el decorador 
		    // algo con 'target' y 'value'...
		  }

		}


Composición del decorador
	   Se pueden aplicar múltiples decoradores a una declaración, como en los siguientes ejemplos:
	     @f
		 @g
		 x









	Básicamente es una implementación de un patrón de diseño de software que en sí sirve para extender una función mediante otra función, pero sin tocar aquella original, que se está extendiendo. El decorador recibe una función como argumento (aquella que se quiere decorar) y devuelve esa función con alguna funcionalidad adicional.

	En esencia vienen a ser una serie de metadatos adicionales que se pueden añadir a clases, métodos, propiedades y parámetros para modificar su comportamiento. 

	Las funciones decoradoras comienzan por una "@" y a continuación tienen un nombre. Ese nombre es el de aquello que queramos decorar, que ya tiene que existir previamente. Podríamos decorar una función, una propiedad de una clase, una clase, etc.


	


Por ejemplo, dado el decorador @sealed , podríamos escribir la función sealed siguiente manera:



	




Qué es un decorador de componentes, qué función tiene y cómo se implementa en un componente básico de Angular 2.
	Ahora, una de las funciones básicas que vas a tener que realizar en todo desarrollo con Angular 2 es la decoración de componentes. En sí, no es más que una declaración de cómo será un componente y las diversas piezas de las que consiste.

En el artículo de introducción a los componentes explicamos solo una de las partes que tiene el archivo .ts con el código Javascript / TypeScript principal de un componente. Lo que vimos hasta ahora era la clase que, decíamos, hacía las veces de controlador, que se exporta hacia afuera para que el flujo principal de ejecución de una aplicación sea capaz de conocer al componente. Además, contiene lo que se llama una función decoradora que conoceremos a continuación.







	Mira la primera línea del código del archivo .ts de tu componente principal.

	import { Component } from '@angular/core';

	Ese import nos está trayendo la clase Component. En la siguiente línea se decora a continuación, con el correspondiente "decorator". No es nuestro objetivo hablar sobre el patrón decorator en sí, ni ver las posibilidades de esta construcción que seguramente tendremos en el futuro ES7, así que vamos a centrarnos en lo que conseguimos hacer con Angular 2 mediante estos decoradores.

	Nota: Uno de los motivos por los que Angular 2 ha tomado TypeScript como lenguaje es justamente por permitir usar decoradores. Con TypeScript podemos realizar la decoración de código de ES7 ya mismo, lo que facilita la decoración del código.

