/*
es necesario correrlo de esta manera porque el compilador, no esta teniendo en cuenta el --target

 tsc --target ES2016 --experimentalDecorators
  
https://www.mmfilesi.com/blog/typescript-y-7-decorators/

  Decoradores de métodos puede recibir varios argumentos, target, nombre y descriptor, 
    y a su vez también puede devolver un descriptor al objeto de destino

  Decoradores de métodos:
    -Se declara justo antes de una declaración de método. 
    -El decorador se aplica al "Descriptor de propiedad para el método" y se puede usar 
     para observar, modificar o reemplazar una definición de método. 
     
     - Un decorador de métodos no se puede usar en un archivo de declaración, sobrecarga o en 
     cualquier otro contexto ambiental (como en una clase de declare ).
    - La expresión del decorador de métodos se llamará como una función en tiempo de ejecución, 
    con los siguientes tres argumentos:
        1- O bien la función de constructor de la clase para un miembro estático, o el prototipo de la clase para 
      un miembro de instancia.
        2- El nombre del miembro.
        3- El descriptor de propiedad para el miembro.


    Pueden ser definido mediante la creación de una funcion 
      -dispone de 3 parámetros (target: any, propertyKey: string, descriptor: PropertyDescriptor)
    
      target:any                  => la clase a la que pertenece el método decorado (lo tipamos como Object).
                                     Es una referencia al objeto que se va a modificar, 
                                     y dependiendo del uso puede ser el constructor del objeto o el prototipo.

      propertyKey:string          => Una cadena con el nombre del método decorado.
      
      descriptor: PropertyDescriptor => las propiedades descriptoras del método decorado.Explica cómo esta 
                                      se comporta. La descripción tiene los siguientes puntos:
                                      Se trata de un objeto que describe el objeto destino. 
                                      Consta de varias propiedades; configurable, enumerable y writable, además de value, 
                                      quizá la más interesante, ya que hace referencia a el valor o la propiedad que vamos a decorar.

                                      Object {
                                          configurable: true,
                                          enumerable: true,
                                          value: function functionName () { ... },
                                          writable: true
                                        }
            
            * configurable: si es true la propiedad puede ser modificada;
            * enumerable: si es true esta propiedad aparece cuando hagamos un for of del objecto;
            * value: es el valor asociado a la propiedad. Puede ser desde un tipo simple hasta una función;
            * writable: indica si la propiedad puede ser cambiada con una asignación;
            * get: si la propiedad es un getter del objeto, aquí podemos escribir una función. Lo devuelto por esa 
                  función se asociará al valor del atributo. En caso de no ser una función y ser undefined, se usará value,
                   como vimos antes;
            * set: similar a lo anterior, pero en este caso la función recibirá el nuevo valor del atributo.
      
      Veamos un ejemplo:

        1- Supón que quieres enviar un mensaje a la consola cada vez que un método es ejecutado.
           Para ello vamos a crear un decorador llamado logger. 
           Como es un decorador, no es más que una función que recibe tres argumentos.


//folio: 382988
*/
/*

     Ejemplo1:  LEYENDO METODOS
       Decorador que puede ser aplicado a cualquier método, para mostrar por consola:
         -la clase a la que pertenece el método
         - el nombre del método
         - el valor que tiene las propiedades descriptoras del método
*/


function mostrarDatosDelMetodo(claseMetodo: any, nombreMetodo: string, descriptor: PropertyDescriptor) {
      console.log('clase a la que pertenece el método: ', claseMetodo);
      console.log('Nombre de la Clase: ', claseMetodo.constructor.name);
      console.log('nombre del método: ', nombreMetodo);
      console.log('valor que tiene las propiedades descriptoras del método: ', descriptor);
}


/*
     Ejemplo1:  MODIFICANDO METODOS
       para MODIFICAR UN METODO hay que retornar algo, pero -dado que estamos modificando un método- 
       ese algo debería ser una función, por ejemplo el propio descriptor con algún cambio o un equivalente. 
       Por ejemplo:
         -cambiaríamos la propiedad "writable" del método:
*/
function cambiarPropWritableDelMetodo(claseMetodo: any, nombreMetodo: string, descriptor: PropertyDescriptor) {
      descriptor.writable = false; //modificamos la propiedad writable
      // Devolvemos nuestro nuevo descriptor modificado
      return descriptor;  
}

/*      Modificar el value-> para las propiedades de la clase
    Para modificar el value, que a priori es más interesante que las propiedades, es algo más complicado, 
    ya que también en este caso debemos retornar una función (y ojo, no hay que usar aquí las arrows ()=>,
    ya que perderíamos la referencia del this de la clase). 
    Por ejemplo, de esta manera modificaríamos el método responder para que la propiedad respuesta 
    siempre fuera «osmel!» independientemente de lo que se le pase por parámetro.
*/
function cambiarPropRespuestadelMetodo(claseMetodo: any, nombreMetodo: string, descriptor: PropertyDescriptor) {
    descriptor.value = function() {
        console.log(this); //this->tiene todas las propiedades 
        this.resultado = '«osmel!';
    };
    // Devolvemos nuestro nuevo descriptor modificado
    return descriptor;
}    

/*      Modificar el value-> para los parametros del método

Con ecmaScript 6 podemos recuperar todos los parámetros que le llegan a una función con la notación de tres puntos,
podemos hacer cosas más interesantes, como incluir en nuestro decorador los argumentos que llegan, 
*/

function MostrarArgumentodelMetodo(claseMetodo: any, nombreMetodo: string, descriptor: PropertyDescriptor) {

      /* guardamos el método original para no perderlo */
      let descriptorOriginal = descriptor.value;

      descriptor.value = function(...args: any[]) {
          /* Recuperamos el método original */
          let result = descriptorOriginal.apply(this, args);
          /* Trazamos lo que queramos */
          console.log('Clase: ', claseMetodo.constructor.name);
          console.log('Método: ', nombreMetodo);
          console.log('Características del método: ', descriptor);

          args[0] = 'duvi alex fidel'; //cambiamos el primer parametro  del metodo
          
          console.log('Argumentos', args);

          /* Devolvemos el método original */
          return result;
      };

      // Devolvemos nuestro nuevo descriptor modificado
    return descriptor;
}    

/*  
    fabrica decoradores, pasando un parametro
    writable evita que se pueda sobreescribir mi método
*/

function PermitirSobreescribir(value: boolean) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    descriptor.writable = value;
  };
}

class clasePrueba {
  resultado: string; //propiedad de la clase  
  preg: string; //propiedad de la clase  
  
  @mostrarDatosDelMetodo
  @cambiarPropWritableDelMetodo
  foo() {
    console.log('prob método');
  }

  @cambiarPropRespuestadelMetodo
  responder(respuesta:string) {
    this.resultado = respuesta;
  }

  @MostrarArgumentodelMetodo
  metodoArgumentos(nomb:string, apell:string, edad:number) {
    //this.resultado = respuesta;
  }

  @PermitirSobreescribir(false)
  funcionACambiar() {
    console.log('no me pueden cambiar, porque mi decorador no lo permite descriptor.writable =false');
  }




}

let p = new clasePrueba();
/*
p.foo();
//p.foo();
p.responder("hola que tal");
console.log(p.resultado);

p.metodoArgumentos('osmel', 'calderon', 42);*/

//aqui sobreescribirmos el "metodo funcionACambiar"
p.funcionACambiar = function() {
   console.log('si pude sobreescribir el metodo, mi decorador lo permite descriptor.writable =true');
}

p.funcionACambiar();



