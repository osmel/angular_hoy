
<!DOCTYPE html>
<!-- saved from url=(0055)http://gugggly.com/templates/doc-web/index.html#welcome -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css">
    
    .code{
      font-family: Liberation Mono;
      font-size: 14px;
    }

</style>
</head>
	<body>
	osmel

	</body>
</html>


<script type="text/javascript">


var p1 = Promise.resolve({ 
  then: function(onFulfill, onReject) { onFulfill('fulfilled!'); }
});
console.log(p1 instanceof Promise) // true, object casted to a Promise

p1.then(function(v) {
    console.log(v); // "fulfilled!"
  }, function(e) {
    // not called
});




/*

function httpGetWheather(){
  return new Promise(function (resolve, reject){
     setTimeout(function() {
       resolve('{"temperatura":10,"lluvia":0}');  //aqui solo estoy pasando una cadena, por eso cdo se recibe se parsea(JSON.parse). Para convertirlo en un objeto json
     }, 200);
  });
}




//1-
httpGetWheather().then(function(value){
  console.log('La promesa ha devuelto:' , value);
});

//2-
	   El método then() devuelve otra promesa permitiendo que podamos encadenar múltiples promesas.
	   Nuestra promesa devuelve información dentro de un string que contiene un objeto JSON. 
	   Imaginemos ahora que solamente queremos mostrar la temperatura:
	    1- de forma que en un primer paso parseamos el json
	    2- y en un segundo paso, mostramos el resultado por consola. 

		Por tanto: En el 1er callback creamos un objeto json a partir del valor devuelto por la primera promesa y devolvemos la temperatura que se pasará como parámetro al siguiente then().

httpGetWheather()
.then(function(jsonResponse) {
    var jsonObject = JSON.parse(jsonResponse); // creamos un objeto json a partir del valor devuelto por la primera promesa
    return jsonObject.temperatura; // retonrnamos la temperatura, Este valor se pasa al siguiente then!
})
.then(function(temperatura){
    console.log('La temperatura es: ' + temperatura);
    return 'osmel';
}).then(function(nombre){
    console.log('Tu nombre es : ' + nombre);
});

//3-
	El valor que se devuelve dentro de un then puede ser una promesa.
	De forma que "javascript esperará a que se:"  resuelva antes de ejecutar el siguiente then().


httpGetWheather()
.then(function(value){
  console.log('La promesa1 ha devuelto : ' , value);  //devuelve promesa
  return 2;  //valor que se devuelve es una promesa
})

.then(function(value){
  console.log('La promesa2 ha devuelto : ' , value); //devuelve 2
  return httpGetWheather();  //valor que se devuelve es una promesa
})
.then(function(value) {
  console.log('La promesa3 devuelve: ', value); //devuelve la promesa inicial
});


//4-

Un ejemplo más complicado, recordemos que:
  si se devuelve una promesa, "no se ejecutará el then hasta tener el valor resuelto por la promesa anterior" y que then() en si devuelve una promesa.


 httpGetWheather()
.then(function(value){
  console.log('La promesa1 ha devuelto : ' , value);
  return httpGetWheather().then(function(jsonValue){   
    return JSON.parse(jsonValue).temperatura;
   });
})
.then(function(value) {  //no se ejecutará el then hasta tener el valor resuelto por la promesa anterior
  console.log('La promesa2 devuelve: ', value); // 10
});



//5-
Si un "callback no devuelve nada", se ejecuta la siguiente función
Por lo tanto si un then no devuelve nada se pasa al siguiente 
	SIN ESPERAR A QUE LA FUNCION ANTERIOR CONCLUYA!! Esto es un error bastante común ya que mucha gente piensa que siempre se ejecutan en orden.


 httpGetWheather()
.then(function(value){
  console.log('La promesa1 ha devuelto : ' , value);
  // NO SE ESPERA A QUE SE TERMINE!
  //Este es una promesa, pero debio antecederle un "return httpGetWheather()". Como no le antecedio, eso significa que no devuelve nada,
  //y por tanto se ejecuta el siguiente then, antes de que concluya esta funcion
 	httpGetWheather().then(function(valor){
  		console.log('Promesa intermedia');
    });
    

})
.then(function(value) {
  console.log('La promesa2 devuelve: ', value); // undefined
});


//6-
 Si no le pasas una función a un then(NULL) o then(), en lugar de fallar pasa al siguiente then (con el valor de promesa q traia) .
 Esto es un caso algo más raro, pero que esta bien conocer, dado el siguiente código:


httpGetWheather()
.then(null)
.then(function(value) {
  console.log(value); // {"temperatura":10,"lluvia":0}
});

*/


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	 /////////////////////////////Errores en promesas/////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////







/*

//1

Cuando una promesa no tiene éxito, se utilizan dos opciones CASI equivalentes. 
	.then(onSuccess, onError) 
	.catch(onError) 

Sin embargo hay que tener en cuenta algunos factores.
- Los errores dentro del fragmento de código asíncrono desaparecen!
- En el siguiente caso, el error se maneja correctamente y será detectado en la funcion catch.


function httpGetWheather(){
  return new Promise(function (resolve, reject){
     
     setTimeout(function() {
       resolve('{"temperatura":10,"lluvia":0}');
     }, 200);
     throw 'KABOOOOM'; //error se maneja correctamente y será detectado en la "funcion catch", porque esta fuera del  código asíncrono
  });
}

httpGetWheather()
.then(function(value) {
  console.log('La promesa devuelve: ', value);
})
.catch(function(err){
  console.log('Error! ' + err); // KABOOOOM
})

//2- .catch es equivalente a   .then(onSuccess, onError) 
httpGetWheather()
.then(function(value) {
  console.log('La promesa devuelve: ', value);
},function(err){
  console.log('Error: ' + err); // KABOOOOM
});


//3- mio propio
httpGetWheather()
.then(function(value) {
  console.log('La promesa devuelve: ', value);
},function(err){
  console.log('Error1: ' + err); // Priorisa primero este KABOOOOM
})
.catch(function(err){
  console.log('Error2! ' + err); 
});

//4- APP DA ERROR. Sin embargo, si el error se produce dentro del código asíncrono no pasará por los error handlers.
function httpGetWheather(){
  return new Promise(function (resolve, reject){
     setTimeout(function() {
        throw 'KABOOOOM'; // Uncaught error . Este error desaparece por estar dentro del  código asíncrono, y por tanto la app, si da error que no podra ser manejado por la funcion catch
        resolve('{"temperatura":10,"lluvia":0}');
     }, 200);
  });
}
httpGetWheather()
.then(function(value) {
  console.log('La promesa devuelve: ', value);
},function(err){
  console.log('Error: ' + err); // Este error nunca se ejecuta... y la app se va a detener, porq es un ERROR DE APP
})

//5- 
Se arregla EL ERROR DE APP, poniendo un "try catch" que en caso de error llame explícitamente al "reject callback"
 function httpGetWheather() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            try {
                throw 'KABOOOOM';  //como esta dentro de un código asíncrono throw(da error), por tanto usar reject
            } catch (err) {
                reject(err);  //reject no da error dentro de código asíncrono
            }
            resolve('{"temperatura":10,"lluvia":0}');
        }, 200);

    });
}

httpGetWheather().then(function(value) {
    console.log('La promesa devuelve: ', value);
}).catch(function(err) {
    console.log('Error: ' + err); // KABOOOOM
});


 //6- 
//Si un "then falla se ejecutará el siguiente catch", pero no se interrumpe la ejecución.
//Es decir que si a continuación del catch ponemos otro then(), este se ejecutará normalmente con el resultado que devuelva el catch()

function httpGetWheather() {
    return new Promise(function(resolve, reject) {
        resolve('{"temperatura":10,"lluvia":0}');

    });
}


httpGetWheather()
.then(function(value) {
    throw 'BOOM'  //este .then falla throw, por tanto se ejecuta el siguiente .cath
    return 'v1';
})
.catch(function(err) {
    return 'e1';  
})
.then(function(value){
  console.log(value); // e1
})


// 7- Y en caso de que se ejecuten correctamente, se ignora el catch.

httpGetWheather()
.then(function(value) {
    return 'v1';
})
.catch(function(err) {
    return 'e1';
})
.then(function(value){
  console.log(value); // v1
})

*/












/*

https://iagolast.github.io/blog/2016/11/07/promesas-javascript.html

var p1 = new Promise(function(resolve, reject) { 
    setTimeout(resolve, 500, "one"); 
});
var p2 = new Promise(function(resolve, reject) { 
    setTimeout(resolve, 100, "two"); 
});

Promise.race([p1, p2]).then(function(value) {
  console.log(value); // "two"
  // Both resolve, but p2 is faster
});



var p3 = new Promise(function(resolve, reject) { 
    setTimeout(resolve, 100, "three");
});
var p4 = new Promise(function(resolve, reject) { 
    setTimeout(reject, 500, "four"); 
});

Promise.race([p3, p4]).then(function(value) {
  console.log(value); // "three"
  // p3 is faster, so it resolves               
}, function(reason) {
  // Not called
});



var p5 = new Promise(function(resolve, reject) { 
    setTimeout(resolve, 500, "five"); 
});
var p6 = new Promise(function(resolve, reject) { 
    setTimeout(reject, 100, "six");
});

Promise.race([p5, p6]).then(function(value) {
  // Not called              
}, function(reason) {
  console.log(reason); // "six"
  // p6 is faster, so it rejects
});



function addToArray (data, array, callback) {
  if (!array) {
    return callback(new Error('No existe el array', null))
  }
  setTimeout(function() { 
    array.push(data)
    callback(null, array)
  }, 1000)
}


var array = [1,2,3];

addToArray(4, array, function (err) {

  if (err) 
  addToArray(5, array, function (err) {
    if (err) 
    addToArray(6, array, function (err) {
      if (err) 
      addToArray(7, array, function () {
        // Hasta el infinito y más allá...
      })
    })

  })

});



var array = [1,2,3];

addToArray(4, array, function (err) {
  if (err) return console.log(err.message)
  console.log(array)
})


*/
/*
var p1 = Promise.resolve(3);
var p2 = 1337;
var p3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 100, "foo");
}); 

Promise.all([p1, p2, p3]).then(values => { 
  console.log(values); // [3, 1337, "foo"] 
});


promesa1 = new Promise(function (resolve, reject) {
	setTimeout(function() { 

		 resolve("exito 1000");
         //reject("fallo 1000");
	}, 1000)


});

promesa2 = new Promise(function (resolve, reject) {
	setTimeout(function() { 
		//console.log('aa');
		 resolve("exito 3000");
         //reject("fallo 3000");
	}, 500)

});


Promise.all([promesa2, promesa1]).then(function(resultado) {
	console.log(resultado);
	

}).catch(function(error) {
	console.log(error);
});



var p1 = new Promise((resolve, reject) => { 
  setTimeout(resolve, 1000, "one"); 
}); 
var p2 = new Promise((resolve, reject) => { 
  setTimeout(resolve, 2000, "two"); 
});
var p3 = new Promise((resolve, reject) => {
  setTimeout(resolve, 3000, "three");
});
var p4 = new Promise((resolve, reject) => {
  setTimeout(resolve, 4000, "four");
});
var p5 = new Promise((resolve, reject) => {
  reject("rechazado");
});

Promise.all([p1, p2, p3, p4, p5]).then(values => { 
  console.log(values);
}).catch(function(error) {
	console.log(error);
});



Promise.all([p1, p2, p3, p4, p5]).then(values => { 
  console.log(values);
}, reason => {
  console.log(reason)
});

//From console:
//"reject"

// Evenly, it's possible to use .catch
Promise.all([p1, p2, p3, p4, p5]).then(values => { 
  console.log(values);
}).catch(reason => { 
  console.log(reason)
});
*/

/*

Promise.all([promesa1, promesa2]).then(function(results) {
	  console.log(result); 
})
.catch(function(error) {
	  console.log("err");
});


promesa1

.then(
	function(result) {  //callback de exito
		  console.log(result); 
	}, 
	function(err) { //callback de fallo
		  console.log(err); 
	}
);	


promesa2
.then(
	function(result) {  //callback de exito
		  console.log(result); 
	}, 
	function(err) { //callback de fallo
		  console.log(err); 
	}


);
*/


</script>