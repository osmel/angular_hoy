(function() {


 
//Se arregla EL ERROR DE APP, poniendo un "try catch" que en caso de error llame explícitamente al "reject callback"
 function httpGetWheather() {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            try {
                throw 'KABOOOOM';  //como esta dentro de un código asíncrono throw(da error), por tanto usar reject
            } catch (err) {
                reject(err);  //reject no da error dentro de código asíncrono
            }
            resolve('{"temperatura":10,"lluvia":0}');
        }, 200);

    });
}

httpGetWheather().then(function(value) {
    console.log('La promesa devuelve: ', value);
}).catch(function(err) {
    console.log('Error: ' + err); // KABOOOOM
});


})(); 