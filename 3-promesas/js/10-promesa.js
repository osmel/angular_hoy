(function() {


function httpGetWheather(){
  return new Promise(function (resolve, reject){
     setTimeout(function() {
       resolve('{"temperatura":10,"lluvia":0}');  //aqui solo estoy pasando una cadena, por eso cdo se recibe se parsea(JSON.parse). Para convertirlo en un objeto json
     }, 200);
  });
}

/*
 Si no le pasas una función a un then(NULL) o then(),
 en lugar de fallar pasa al siguiente then (con el valor de promesa q traia) .
 Esto es un caso algo más raro, pero que esta bien conocer, dado el siguiente código:
*/

httpGetWheather()
.then(null)
.then(function(value) {
  console.log(value); // {"temperatura":10,"lluvia":0}
});


})(); 