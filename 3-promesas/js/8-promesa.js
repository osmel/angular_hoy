(function() {

function httpGetWheather(){
  return new Promise(function (resolve, reject){
     setTimeout(function() {
       resolve('{"temperatura":10,"lluvia":0}');  //aqui solo estoy pasando una cadena, por eso cdo se recibe se parsea(JSON.parse). Para convertirlo en un objeto json
     }, 200);
  });
}


/*

Un ejemplo más complicado, recordemos que:
  si se devuelve una promesa, 
  "no se ejecutará el then hasta tener el valor resuelto por la promesa anterior" 
  y que then() en si devuelve una promesa.
*/

 httpGetWheather()
.then(function(value){
  console.log('La promesa1 ha devuelto : ' , value);
  return httpGetWheather().then(function(jsonValue){   
    return JSON.parse(jsonValue).temperatura;
   });
})
.then(function(value) {  //no se ejecutará el then hasta tener el valor resuelto por la promesa anterior
  console.log('La promesa2 devuelve: ', value); // 10
});


})(); 