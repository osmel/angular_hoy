(function() {
  
  function getUsuarios(){
    /* 
      Una promesa es un objeto que devuelve en su firma 2 metodos (resolve, reject)
     return new Promise ( function(resolve, reject){

     });
     */

     //declaramos la promesa y usamos 2 parametros que los vamos a tener en cuenta para decir:
     //exito = resolve
     //fallo = reject
     return new Promise ( function(resolve, reject){
            setTimeout(function() {
                console.log("usuarios listos"); 
                //resolve([1,2,3]); //en este caso estamos aclarando que tuvo EXITO
                reject();  //en este caso estamos aclarando que FALLO
            }, 800);


     });

  	  
  }


  function getProyectos(){

     return new Promise ( function(resolve, reject){
        setTimeout(function(){
              console.log('Proyectos listos');
              //resolve();
              reject("mi promesa fue rechazada por motivos xyz");
        }, 400);

     });

  	
  }
  function getCuestiones(){

     return new Promise ( function(resolve, reject){
        setTimeout(function(){
            console.log('Cuestiones listoa');
            resolve();
        }, 700);
     });
  	  

  }


/*
https://www.youtube.com/watch?v=9im-5iDgH54
https://promisesaplus.com/

 sincronizarlas o encadenarlas:
 algo asi como una dependencia
   es decir pongo en cierto orden, para saber en que momento suceden las cosas
*/

//los estados que pueden regresar una promesa, es "pendiente, resuelto y rechazo" min29
  getUsuarios() //como la funcion de usuario me regresa una promesa con exito, pues entra a then y asi sucesivamente
  .then(function(respuesta){
     console.log('manipulando respuesta de Usuarios '+respuesta);
     //throw " excepcion forzada";
     return getProyectos();  //aqui llamo a la funcion getProyectos(), para que me analice la promesa de proyectos, 
  },

 function(respuesta){
     console.log('error');
     //throw " excepcion forzada";
     //return getProyectos();  //aqui llamo a la funcion getProyectos(), para que me analice la promesa de proyectos, 
  },

  ) 
  //.then(getProyectos)  //getProyectos() no debe ponerse () sino estaria llamando a la funcion, y lo que necesitamos es llamar a un callback
  .then(getCuestiones)
  .catch(function(error){
     console.error('error mio '+ error);
  });

  	/* 

    en caso de no ser cachado(catch) un reject, pues da un error
        Uncaught (in promise) undefined
	
    nota: una vez que una de las promesas falla va directamente al bloque de catch, y ya no continua más, es

Terminology

 *        promesa: es un objeto o una funcion que tiene un metodo then
  objeto thenable: son objetos que tienen un metodo then
  por tanto: una promesa es un thenable

* Cdo tienes una promesa, puedes mandar a traer una funcion o manipular esa respuesta
incluyendo (undefined, un thenable o una promesa)

  .then(function(respuesta){
     console.log(respuesta);
     return getProyectos();
  })  //getProyectos() no debe ponerse () sino estaria llamando a la funcion, y lo que necesitamos es llamar a un callback


* Se puede forzar una excepcion en cualquier momento, y esto implica que ira directamente a un catch
throw " excepcion forzada";

* Una Razon(reason): es el motivo por lo que la promesa fue rechazada
   reject("mi promesa fue rechazada por motivos xyz");


min 50.47
polYfill.io:  codigo que te permite agregar promesas para navegadores viejos
    es6-polYfill
      <script src="js/es-6-promise.js"></script> 



  */	


})();	