(function() {

function httpGetWheather(){
  return new Promise(function (resolve, reject){
     setTimeout(function() {
       resolve('{"temperatura":10,"lluvia":0}');
     }, 200);
     throw 'KABOOOOM'; //error se maneja correctamente y será detectado en la "funcion catch", porque esta fuera del  código asíncrono
  });
}

//3- mio propio
httpGetWheather()
.then(function(value) {
  console.log('La promesa devuelve: ', value);
},function(err){
  console.log('Error1: ' + err); // Priorisa primero este KABOOOOM
})
.catch(function(err){
  console.log('Error2! ' + err); 
});


})(); 