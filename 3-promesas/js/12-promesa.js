(function() {

function httpGetWheather(){
  return new Promise(function (resolve, reject){
     setTimeout(function() {
       resolve('{"temperatura":10,"lluvia":0}');
     }, 200);
     throw 'KABOOOOM'; //error se maneja correctamente y será detectado en la "funcion catch", porque esta fuera del  código asíncrono
  });
}

//2- .catch es equivalente a   .then(onSuccess, onError) 
httpGetWheather()
.then(function(value) { //onSuccess
  console.log('La promesa devuelve: ', value);
},function(err){ //onError
  console.log('Error: ' + err); // KABOOOOM
});


})(); 