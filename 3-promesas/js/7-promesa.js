(function() {


function httpGetWheather(){
  return new Promise(function (resolve, reject){
     setTimeout(function() {
       resolve('{"temperatura":10,"lluvia":0}');  //aqui solo estoy pasando una cadena, por eso cdo se recibe se parsea(JSON.parse). Para convertirlo en un objeto json
     }, 200);
  });
}


/*
	El valor que se devuelve dentro de un then puede ser una promesa.
	De forma que "javascript esperará a que se:"  resuelva antes de ejecutar el siguiente then().
*/

httpGetWheather()
	.then(function(value){
	  console.log('La promesa1 ha devuelto : ' , value);  //devuelve promesa
	  return 2;  //valor que se devuelve es una promesa
	})

.then(function(value){
  console.log('La promesa2 ha devuelto : ' , value); //devuelve 2
  return httpGetWheather();  //valor que se devuelve es una promesa
})
.then(function(value) {
  console.log('La promesa3 devuelve: ', value); //devuelve la promesa inicial
});

})(); 