(function() {
  function getUsuarios(){ //esto es una función que envuelve una promesa, por tanto retorna una promesa
     return new Promise ( function(resolve, reject){
            setTimeout(function() {
                console.log("usuarios listos"); 
                resolve([2,3,4]); //en este caso estamos aclarando que tuvo EXITO
                
            }, 800);


     });
  }


function getProyectos(){

     return new Promise ( function(resolve, reject){
        setTimeout(function(){
              console.log('Proyectos listos');
              resolve('parametro');
              
        }, 400);

     });

    
  }
  function getCuestiones(){

     return new Promise ( function(resolve, reject){
        setTimeout(function(){
            console.log('Cuestiones listo');
            resolve();
        }, 700);
     });
      

  }


  function getOtra(){

     return new Promise ( function(resolve, reject){
        setTimeout(function(){
            console.log('Otra listo');
            resolve();
        }, 700);
     });
      

  }

  

  /*
     Caso de que quieras manipular la respuesta de una promesa, para enviarselo al proximo metodo
  */

  getUsuarios() 
    //.then( getProyectos )  
    .then(function(respuestaUsuario){   //argumento que recibe lo que me envio su resolve
         console.log(respuestaUsuario);  
         return getProyectos();  //aqui si se especifica el argumento ()
                                  // Este valor se pasa al siguiente then!
    })

    .then(getCuestiones)
    .then(getOtra);



})(); 