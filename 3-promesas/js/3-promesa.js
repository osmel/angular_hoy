(function() {
  function getUsuarios(){ //esto es una función que envuelve una promesa, por tanto retorna una promesa
     return new Promise ( function(resolve, reject){
            setTimeout(function() {
                console.log("usuarios listos"); 
                resolve(); //en este caso estamos aclarando que tuvo EXITO
                
            }, 800);


     });
  }


function getProyectos(){

     return new Promise ( function(resolve, reject){
        setTimeout(function(){
              console.log('Proyectos listos');
              reject('parametro');
              
        }, 400);

     });

    
  }
  function getCuestiones(){

     return new Promise ( function(resolve, reject){
        setTimeout(function(){
            console.log('Cuestiones listo');
            resolve();
        }, 700);
     });
      

  }


  function getOtra(){

     return new Promise ( function(resolve, reject){
        setTimeout(function(){
            console.log('Otra listo');
            resolve();
        }, 700);
     });
      

  }

  

  /*
     Si una de las promesa falla, solo se podrá catchar, es decir concatenarla con un .catch, 
    no esperarla con un .then
  
  */

  getUsuarios() 
    .then( getProyectos )  
    /*
    si se trata de tomar con un ".then"--> Uncaught (in promise) undefined
    me devolvera un error especificando que no se ha cachado la promesa
    .then( getCuestiones );  
    */

    //.catch( getCuestiones )   //puedo llamar otra promesa
    .catch(function(error){  //o ir al control de errores
     console.error('error mio '+ error);
    })
    .then(getOtra);



})(); 