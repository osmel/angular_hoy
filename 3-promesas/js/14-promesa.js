(function() {

//4- APP DA ERROR. Sin embargo, si el error se produce dentro del código asíncrono no pasará por
// los error handlers.
function httpGetWheather(){
  return new Promise(function (resolve, reject){
     setTimeout(function() {
        throw 'KABOOOOM'; // Uncaught error . Este error desaparece por estar dentro del  código asíncrono, y por tanto la app, si da error que no podra ser manejado por la funcion catch
        resolve('{"temperatura":10,"lluvia":0}');
     }, 200);
  });
}

httpGetWheather()
.then(function(value) {
  console.log('La promesa devuelve: ', value);
},function(err){
  console.log('Error: ' + err); // Este error nunca se ejecuta... y la app se va a detener, porq es un ERROR DE APP
})


})(); 