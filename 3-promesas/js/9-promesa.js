(function() {

function httpGetWheather(){
  return new Promise(function (resolve, reject){
     setTimeout(function() {
       resolve('{"temperatura":10,"lluvia":0}');  //aqui solo estoy pasando una cadena, por eso cdo se recibe se parsea(JSON.parse). Para convertirlo en un objeto json
     }, 200);
  });
}



/*
Si un "callback no devuelve nada", se ejecuta la siguiente función
Por lo tanto si un then no devuelve nada se pasa al siguiente 
	SIN ESPERAR A QUE LA FUNCION ANTERIOR CONCLUYA!! 
	Esto es un error bastante común ya que mucha gente piensa que siempre se ejecutan en orden.
*/

 httpGetWheather()
.then(function(value){
  console.log('La promesa1 ha devuelto : ' , value);
  // NO SE ESPERA A QUE SE TERMINE!
  //Este es una promesa, pero debio antecederle un "return httpGetWheather()". 
  //Como no le antecedio, eso significa que no devuelve nada,
  //y por tanto se ejecuta el siguiente then, antes de que concluya esta funcion
 	httpGetWheather().then(function(valor){
  		console.log('Promesa intermedia');
    });
    
    

})
.then(function(value) {
  console.log('La promesa2 devuelve: ', value); // undefined
});

})(); 