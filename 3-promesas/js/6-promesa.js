(function() {

/*
En este ejemplo solo hay una promesa("httpGetWheather")  y apartir de esta con los .then
creamos varias promesas, para ir pasando de una en una valores mediante return
*/


function httpGetWheather(){
  return new Promise(function (resolve, reject){
     setTimeout(function() {
       resolve('{"temperatura":10,"lluvia":0}');  //aqui solo estoy pasando una cadena, por eso cdo se recibe se parsea(JSON.parse). Para convertirlo en un objeto json
     }, 200);
  });
}


/*
	   El método then() devuelve otra promesa permitiendo que podamos encadenar múltiples promesas.
	   Nuestra promesa devuelve información dentro de un string que contiene un objeto JSON. 
	   Imaginemos ahora que solamente queremos mostrar la temperatura:
	    1- de forma que en un primer paso parseamos el json
	    2- y en un segundo paso, mostramos el resultado por consola. 

		Por tanto: En el 1er callback creamos un objeto json a partir del valor devuelto por la primera promesa
		 y devolvemos la temperatura que se pasará como parámetro al siguiente then().
*/
	httpGetWheather()
	.then(function(jsonResponse) {
	    var jsonObject = JSON.parse(jsonResponse); // creamos un objeto json a partir del valor devuelto por la primera promesa
	    return jsonObject.temperatura; // retonrnamos la temperatura, Este valor se pasa al siguiente then!
	})
	.then(function(temperatura){
	    console.log('La temperatura es: ' + temperatura);
	    return 'osmel';
	}).then(function(nombre){
	    console.log('Tu nombre es : ' + nombre);
	});


})(); 