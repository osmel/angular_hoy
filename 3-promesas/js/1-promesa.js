/* En este caso como no usa promesas, pues depende exactamente de los tiempos definidos en cada asincrono
para devolver los valores, y no con la secuencia que se esperaba que era esta:


  getUsuarios();
  getProyectos();
  getCuestiones();
  
   Por tanto como tiene en cuenta los tiempo la respuesta es: 
    Proyectos listos (400milisegundos)
   Cuestiones listoa (700milisegundos)
    usuarios listos  (800milisegundos)

  

*/

(function() {
  
  function getUsuarios(){
  	  setTimeout(function() {
  	  		console.log("usuarios listos");
  	  }, 800);
  	  
  }

  function getProyectos(){
  	  setTimeout(function(){
  	  		console.log('Proyectos listos');
  	  }, 400);
  	
  }
  function getCuestiones(){

  	  setTimeout(function(){
  	  		console.log('Cuestiones listoa');
  	  }, 700);
  	
  }

  getUsuarios();
  getProyectos();
  getCuestiones();
  	/* la respuesta es 
  	Proyectos listos
	 Cuestiones listoa
		usuarios listos

	*/	


})();	