(function() {
  //polyfill de promesa ->para que funcione en otros navegadores, 
    //inyecta exactamente las cosas que nos hace falta


  function getUsuarios(){ //esto es una función que envuelve una promesa, por tanto retorna una promesa
    /* 
      Una promesa es un objeto que devuelve en su firma 2 metodos (resolve, reject)
     return new Promise ( function(resolve, reject){

     });
     */

     //declaramos la promesa y usamos 2 parametros que los vamos a tener en cuenta para decir:
     //exito = resolve
     //fallo = reject
     return new Promise ( function(resolve, reject){
            setTimeout(function() {
                console.log("usuarios listos"); 
                resolve(); //en este caso estamos aclarando que tuvo EXITO
                
            }, 800);


     });
  }


function getProyectos(){

     return new Promise ( function(resolve, reject){
        setTimeout(function(){
              console.log('Proyectos listos');
              resolve();
              
        }, 400);

     });

    
  }
  function getCuestiones(){

     return new Promise ( function(resolve, reject){
        setTimeout(function(){
            console.log('Cuestiones listo');
            resolve();
        }, 700);
     });
      

  }


  

  /*
  De esta forma logro el encadenamiento de promesas.
  La magia esta en que cada función que llamo me retorna una promesa, y al retornar una promesa, pues puedo
  encadenarlo con un .then (siempre y cuando me devuelva exito)
    OJO: (las funciones no se le especifica el argumento)
    
   then() devuelve otra promesa permitiendo que podamos encadenar múltiples promesas.

  */

  getUsuarios() 
    .then( getProyectos )  
    .then( getCuestiones )




})();	