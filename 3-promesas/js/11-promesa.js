(function() {

/* Errores en promesas

Cuando una promesa no tiene éxito, se utilizan dos opciones CASI equivalentes. 
	.then(onSuccess, onError) 
	.catch(onError) 

Sin embargo hay que tener en cuenta algunos factores.
- Los errores dentro del fragmento de código asíncrono desaparecen!
- En el siguiente caso, el error se maneja correctamente y será detectado en la funcion catch.
*/

function httpGetWheather(){
  return new Promise(function (resolve, reject){
     setTimeout(function() {
       resolve('{"temperatura":10,"lluvia":0}');
     }, 200);
     throw 'KABOOOOM'; //error se maneja correctamente y será detectado en la "funcion catch", porque esta fuera del  código asíncrono
  });
}

httpGetWheather()
.then(function(value) {
  console.log('La promesa devuelve: ', value);
})
.catch(function(err){
  console.log('Error! ' + err); // KABOOOOM
})


})(); 