(function() {



/*Si un "then falla se ejecutará el siguiente catch", pero no se interrumpe la ejecución.
Es decir que si a continuación del catch ponemos otro then(), 
este se ejecutará normalmente con el resultado que devuelva el catch()
*/

function httpGetWheather() {
    return new Promise(function(resolve, reject) {
        resolve('{"temperatura":10,"lluvia":0}');

    });
}


httpGetWheather()
.then(function(value) {
    throw 'BOOM'  //este .then falla throw, por tanto se ejecuta el siguiente .cath
    return 'v1';
})
.catch(function(err) {
    return 'e1';  
})
.then(function(value){
  console.log(value); // e1
})



})(); 