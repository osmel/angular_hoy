(function() {



//Resolviendo una promesa directamente "Promise.resolve"

	var p1 = Promise.resolve({ 
	  then: function(exito, fallo) {
	   		exito('fulfilled!'); 
	   }
	});

	console.log(p1 instanceof Promise); // true, objeto casteado con una Promesa

	p1.then(function(v) {
	    console.log(v); // "fulfilled!"
	  }, function(e) {
	    // not called
	});


})(); 