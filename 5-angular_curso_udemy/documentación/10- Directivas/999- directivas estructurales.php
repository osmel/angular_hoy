///////////////////////////////////////////////////////////////////////////////////////
////////////////////cap38 - "Directivas estructurales"  ngIF ngFOR/////////////////////
/////////////////////////////////////////////////////////////////////////////////////
  ngOnInit() { //se usa para cargar cdo toda la pagina esta renderizada, es decir cdo esta listo el DOM
  }


		Angular interpreta por igual
		[src]="heroe.img" o src="{{heroe.img}}"
		<img class="card-img-top img-fluid" [src]="heroe.img" [alt]="heroe.nombre">  				


*ngIf="false" -->que no muestre nada. Ademas el "elemento no existe lo destruye del dom"
				--Aparece una referencia

*ngFor : ciclo "let variable of arreglo" : repite la cantidad del arreglo
			<li *ngFor="let varPersona of personajes" class="list-group-item">Cras justo odio</li>

		variable conteo de ciclo "index"	
		<li *ngFor="let persona of personajes; let i = index" class="list-group-item">
        	{{i}}.{{persona}}
        </li>			

invocando evento click desde html. Tomando en cuenta la "variable mostrar" q cree
(click) ="mostrar=!mostrar">				

  	<button type="button" class="btn btn-outline-primary btn-block"
      (click) ="mostrar=!mostrar">
      Mostrar/Ocultar
    </button>
