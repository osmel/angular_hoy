
/////////////////////////////////////////////////////////////////////////////////////
////////////////////52- Servicios/(es como un MODELO) //////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

		vamos a trabajar desde un servicio, porq cdo trabajemos a traves de api, todo deberia ser gestionado a traves de un servicio, porq es lo más práctico
		crearemos un servicio "heroe", q será encargado de mostrar o manejar la información referente a los heroes de nuestra APP
		
		Caracteristica de los servicios:
		 - Brindar información a quien lo necesite
		 - Realizar peticiones CRUD (crear, leer, actualizar, eliminar)
		 - Mantener la data de forma persistente ( datos listo siempre, para q los clientes entren y obtengan la informacion. Por ejemplo para hacer peticiones por http)
		 - Servir como recurso re-utilizable para nuestra APP(varios puedan acceder a la misma data)


 Creando nuestro 1er servicio.
  		ng g s servicios/heroe  //Creo o Defino el servicio "heroes.service.ts"
 	     import { Injectable } from '@angular/core';
			@Injectable()  
			export class HeroesService {
			  constructor() {
			    console.log('servicios listo para usar!!!');
			  }
			}

		2- Le digo a angular q disponga de ese servicio "app.module.ts"
		 //servicios
			import { HeroesService } from './servicios/heroes.service';	
			  providers: [ //providers=services
			    HeroesService
  			  ],

  		3- Usar el servicio
  		   -voy al componente en este caso "heroes.components.ts"	  
  		     - importo el servicio
	  		     import { HeroesService } from '../../servicios/heroes.service';	
	  		 - lo paso como parametro al constructor (injecto)
	  		    constructor(private _miservicio:HeroesService) { 

  				}    
  		nota: cuando termino aqui inmediatamente dispara el constructor de servicio
  		