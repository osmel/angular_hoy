https://angular.io/cli/generate

/////////////////////////////////////////////////////////////////////////////////////
////////////////////cap33 - Creando Componente con cli////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

creo componente de forma automatica
  g(generame)
  c(componente)
 
 ng g c footer
 		
 		is: "inline style" -->para q no salga archivo de estilo (.css)
   		ng g c componets/heroes -is

   		-is -> para evitar que cree el .css
   		--spec=false-> para evitar que cree el fichero de testeo. spec.ts

		 	esto me genera cada elemento del componente
		 	y ademas me lo actualiza en el "app.module.ts"  (ASEGURARNOS DE Q SI LO ACTUALIZO)
		 	
			  create src/app/footer/footer.component.css   
			  create src/app/footer/footer.component.html
			  create src/app/footer/footer.component.spec.ts
			  create src/app/footer/footer.component.ts
			  update src/app/app.module.ts


 creando nuevo componente
   1 crear carpeta
      src/app/carpetaComponente
   2- crear archivo
       header.component.ts
   3-    
         import { Component } from '@angular/core';  //libreria de componente

         //crear el decorador del componente
			@Component({
			  selector: 'app-header',
			  template:`<h1> Este es mi header <h1>`, //manual el template "template"
			  //templateUrl: './header.component.html',  //usando el archivo "templateUrl"
			  styleUrls: ['./header.component.css']
			})

			export class HeaderComponent {
			  Titulo = 'Mi encabezado';  
			}

	4-  Como angular no sabe que mi nuevo componente existe, el "app.module.ts"  es quien le dice a angular por tanto:
		 src/app/app.module.ts

		 -Importar tengo que importar mi clase 
		    import { HeaderComponent } from './componentes/header.component';
		 - Declararla 
			@NgModule({
			  declarations: [ //min 8.10 listado de componentes q tendra nuestra app
			    AppComponent,
		    	HeaderComponent
		  	],		

	5- Ahora si puedo hacer uso de este componente	  	
   html
    <app-mibarra> </app-mibarra>




/////////////////////////////////////////////////////////////////////////////////////
////////////////////Generando componentes y otros https://github.com/angular/angular-cli/////////////////////
/////////////////////////////////////////////////////////////////////////////////////

https://angular.io/cli/generate

Componente			ng g c my-new-component
Directiva			ng g d my-new-directive
Pipe(tuberias)		ng g pipe my-new-pipe     			//ng g p pipes/capitalizado
Servicio(Provider)	ng g service my-new-service
Clase				ng g class my-new-class
Guardia(guard)		ng g guard my-new-guard
Interfaz			ng g interface my-new-interface
Enum				ng g enum my-new-enum
Módulo				ng g module my-module

		Nota:
		# admiten generación de ruta relativa 
		# agregará referencia en app.module.ts. 