/////////////////////////////////////////////////////////////////////////////////////
////////////////////Instalando BootStrap (cap 45)////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
  https://angular.io/guide/using-libraries

 	3 tipos de formas de instalar bootstrap:

		1- CDN (declarandolo en index.html)
		2- Descargandolo y poniendolo en assets (declarandolo en index.html)

		3- instalar bootstrap y sus dependencia(jquery, popper) 
			 		
				-> https://getbootstrap.com/
				-> http://jquery.com/download/
				-> https://github.com/FezVrasta/popper.js

				npm install bootstrap jquery popper.js --save

			 			nota:Segun se va instaalando va agregando todas las dependencia en  "package.json"
			 			--save: porque necesitamos q este archivo se ponga en el paquete json ("package.json")
			

			"angular-cli.json" o  "angular.json":luego manualmente nosotros especificamos los ficheros con los que va a trabajar las dependencias
			    
			    "styles": [
		          "styles.css",
		          "../node_modules/bootstrap/dist/css/bootstrap.min.css"
		        ],
		        "scripts": [
		          "../node_modules/jquery/dist/jquery.min.js",
		          "../node_modules/popper.js/dist/umd/popper.min.js",
		          "../node_modules/bootstrap/dist/js/bootstrap.min.js"
		        ],


			      nota: https://medium.com/codingthesmartway-com-blog/using-bootstrap-with-angular-c83c3cee3f4a
