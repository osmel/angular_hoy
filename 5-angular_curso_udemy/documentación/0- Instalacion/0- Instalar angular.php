Instalar angular

////////////////NPM y node//////Node.js(requiere versión mayor a  8.xo.)
  
	 - comprobar version de nodejs
	     sudo npm --version
	- Limpiar la cache que genera npm.
	     sudo npm cache clean -f
	- instalar n, una herramienta de administración de Node,
	     sudo npm install -g n
	- instalar la ultima versión estable.
	    sudo n stable
	-instalar alguna versión en concreto
	     sudo n 0.10.18
	- Instalar la última versión de npm.
	    sudo npm update npm -g

	npm –-version
	node --version


//////////////////////Actualizar el git 
	- comprobar version de git
	    git --version
	- Para instalar Git desde el código fuente necesitas tener las siguientes librerías de las que Git depende: 
	    apt-get install libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev
	- sitio para descargar la ultima version de git
		https://github.com/git/git/releases 
	- proceso de actualizacion
		 tar -zxf git-2.16.1.tar.gz 
		cd git-2.16.1 
		make configure 
		./configure --prefix=/usr 
		make all doc info 
		sudo make install install-doc install-html install-info
	CÓMO USAR GIT PARA HACER DEPLOY DE NUESTRO WEBSITE 
	http://www.jaimeolmo.com/2015/02/como-usar-git-para-hacer-deploy-de-nuestro-website/ 

/////////////////////////////////////////TypeScript
	- Para la última versión estable:
	 	sudo npm install -g typescript 
	- comprobar version de typescript
		tsc -v  

		//****si fueras a usar typescript
		--crear un proyecto TS(este crea un fichero tsconfig.json)
			 cd typescript
			 tsc -init

		--para escuchar(cada vez que haya un cambio se compile automaticamente) todos los archivos, puedo hacerlo con 
		   tsc *.ts -w  (esto es posible porq hay un proyecto ts creado con init)
		-- compilar 
			  tsc proyecto.ts   
				Es necesario compilar el archivo .ts, para que genere un archivo .js porque los navegadores solo entiende .js,       no        .ts

////////////////////////////////angular  -> angular-cli (Command Line Interface) Interfaz de comando de angular
//Instalar de forma global las dependencias. (Instalación desde cero)
		sudo npm install -g @angular/cli
		ng -v

//Caso en que quiera instalar una nueva version borrando los paquetes anteriores de angular
	Paso 1. Desinstalar los paquetes anteriores de Angular CLI 
		sudo npm uninstall -g angular-cli 
		sudo  npm uninstall -g @angular/cli 
	Paso 2. Vaciar la caché del gestor de paquetes npm 
		sudo  npm cache clean 
	Paso 3. Instalar la última versión de Angular CLI 
	    sudo npm install -g @angular/cli
		sudo  npm install -g @angular/cli@latest 
		ng -v

	Ahora ya tenemos actualizado Angular CLI y ya podemos generar un nuevo proyecto de Angular x estable
		ng new proyecto
		   * (que no construya el router)
		   * (que utilice css)
		cd new proyecto
		ng serve



Verificar nuestro angular
	ng --version

					Package                           Version
					-----------------------------------------------------------
					@angular-devkit/architect         0.12.1
					@angular-devkit/build-angular     0.12.1
					@angular-devkit/build-optimizer   0.12.1
					@angular-devkit/build-webpack     0.12.1
					@angular-devkit/core              7.2.1
					@angular-devkit/schematics        7.2.1
					@angular/cli                      7.2.1
					@ngtools/webpack                  7.2.1
					@schematics/angular               7.2.1
					@schematics/update                0.12.1
					rxjs                              6.3.3
					typescript                        3.2.2
					webpack                           4.23.1

Crear un nuevo proyecto.
	ng new my-app
Iniciamos nuestra aplicación.
	cd my-app
	ng serve --open
			ng serve --host 0.0.0.0 --port 4201




