nube de audio gratis
		https://covalenciawebs.com/10-plataformas-de-almacenamiento-en-la-nube/
		
musica (subir contenido a SoundCloud es gratis o es comercial?)
 Api de soundCloud
 https://developers.soundcloud.com/docs/api/guide#uploading
 3hrs o 180min
 https://help.soundcloud.com/hc/es/articles/115003452847-Requisitos-para-subir

/////////////
Modulos:
-- Cada aplicación Angular tiene al menos una clase de módulo, que es el módulo raíz (root), por convención es llamado AppModule. 
	--cada modulo es un bloque dedicado a una tarea concreta(cada uno de ellos es un bloque cohesivo de código dedicado a un dominio de aplicación, un flujo de trabajo o un conjunto de capacidades estrechamente relacionados).	
	--módulo es una clase con el decorador @NgModule

					Decoradores:
						   son "funciones que modifican las clases JavaScript". 
   							Angular tiene muchos decoradores que adjuntan metadatos a las clases para especificar lo que significan esas clases y cómo deben funcionar.

		NgModule: es una función de decorador que toma un solo objeto de metadatos cuyas propiedades describen al módulo. Las propiedades más importantes de NgModule son:
			- declarations: definen las clases de view que pertenecen a este módulo. Angular tiene tres tipos de clases de view: 
				**componentes, directivas y pipes.
			- exports: el subconjunto de las declaraciones que deben ser visibles y utilizables en el componente de las plantillas de otros módulos.
			- imports: otros módulos cuyas clases exportadas son necesarios por componente de las plantillas declaradas en este módulo.
			- providers: son los "servicios" que se van a usar de forma global. Éstos serán accesibles en cualquier parte de la aplicación.
			-  bootstrap: la "vista principal de la aplicación", llamado componente raíz, que aloja todos los otros puntos de vista de la aplicación. Sólo el módulo raíz debe establecer esta propiedad.
						src/app/app.module.ts:
						import { NgModule } from '@angular/core';
						import { BrowserModule } from '@angular/platform-browser';
						@NgModule({
						  imports: [ BrowserModule ],
						  providers: [ Logger ],
						  declarations: [ AppComponent ],
						  exports: [ AppComponent ],
						  bootstrap: [ AppComponent ]
						})
						export class AppModule { }


   	archivo main.ts: es el encargado de arrancar el módulo AppModule(modulo raiz).
///////////////

Bibliotecas de Angular:  "prefijo @angular".
		Cada nombre de biblioteca de angular comienza con el "prefijo @angular".
		 Los puedes "instalar con el gestor de paquetes npm e importar partes de ellos con las instrucciones import de JavaScript".
    		
			- Por ejemplo, importa el decorador de Component de Angular  de la biblioteca @angular/core, así:
					*import { Component } from '@angular/core';

			-- También puedes importar "módulos Angular(NgModules) " desde las bibliotecas de Angular utilizando declaraciones import de JavaScript:
					* import { BrowserModule } from '@angular/platform-browser';

			En el ejemplo anterior del módulo raíz, el módulo de la aplicación necesita materiales que están dentro de BrowserModule. Para acceder a ese material, se lo agrega al array imports de los metadatos de @NgModule de este modo.
					imports: [ BrowserModule ],
				

///////////////
Componentes:   (controlador)
		controla una parte de lo que se muestra en la pantalla, que se denomina vista. 

		"Defines la lógica de la aplicación" de un componente y la funcionalidad que cumple para la vista, dentro de una clase. 
			La clase interactúa con la vista a través de una API de propiedades y métodos. (El componente interactúa con la view a través de una serie de propiedades y métodos, en los que se define la lógica de negocio.)

					Por ejemplo, el "componente HeroListComponent" tiene una propiedad heroes que devuelve un array de hero[] que adquiere de un servicio. HeroListComponent también tiene un "método selectHero()" que establece una propiedad selectedHero cuando el usuario hace clic para elegir un héroe de la lista.

					export class HeroListComponent implements OnInit {
					  heroes: Hero[];
					  selectedHero: Hero;

					  constructor(private service: HeroService) { }

					  ngOnInit() {
					    this.heroes = this.service.getHeroes();
					  }

					  selectHero(hero: Hero) { this.selectedHero = hero; }
					}

					Angular crea, actualiza y destruye componentes a medida que el usuario se mueve a través de la aplicación.
					 Tu aplicación puede reaccionar en cada momento del ciclo de vida mediante los hooks opcionales (optional lifecycle hooks), como "ngOnInit() " declarado anteriormente, el cual ejecuta un fragmento de código cuando el componente es creado.




///////////////
Plantillas:  (vista)
    	Contiene un fragmento de HTML que indica a Angular cómo renderizar el componente. 

				    	<h2>Hero List</h2>

						<p><i>Pick a hero from the list</i></p>
						<ul>
						  <li *ngFor="let hero of heroes" (click)="selectHero(hero)">
						    {{hero.name}}
						  </li>
						</ul>

						<hero-detail *ngIf="selectedHero" [hero]="selectedHero"></hero-detail>	

							sintaxis de la plantilla de Angular:  *ngFor, {{hero.name}}, (click), [heroe] y <hero-detail>
																 <hero-detail>: un elemento personalizado que representa un nuevo componente


///////////////////////////
Metadatos: 
		le indican a Angular cómo procesar una clase.
		Ejemplo: HeroListComponent realmente es sólo una clase. "****No es un componente hasta que se lo digas a Angular.********"
								Para decirle a Angular que HeroListComponent es un componente, debes adjuntar metadatos a la clase.
								En TypeScript, se adjuntan metadatos utilizando un decorador. 

					Los metadatos contienen la información que angular necesita para procesar una clase. Una clase no es un componente hasta que se declare como tal. Para poder hacer esto, será necesario añadirle metadatos a la clase. Adjuntar metadatos a una clase se hace mediante el decorador @Component y pasándole un objeto que lo configurará. 

					****Veamos un ejemplo de clase:
						export class HeroListComponent implements OnInit {
						  /* . . . */
						}

				    **Veamos la misma clase convertida en Componente:
						
							//decorador justo encima de la clase 
						@Component({  //aqui junto al decorador los metadatos, que le indicaran a angular que esta clase es un componente
						  selector: 'hero-list',					   //nombre que recibirá el selector
						  templateUrl: './hero-list.component.html',   //plantilla debe utilizar
						  providers: [ HeroService ]				   //proveedores de inyección de dependencias para los servicios que requiera el componente.
						})
						export class HeroListComponent implements OnInit {
						  /* . . . */
						}

						

  


			Algunas de las opciones de configuración de @Component más útiles:

				selector: 
				templateUrl: dirección relativa al archivo de la plantilla HTML de este componente.
				
				providers: conjunto de proveedores de "inyección de dependencia" para los servicios que el componente requiere. Esta es una forma de decirle a Angular que el constructor del componente requiere un HeroService para que pueda obtener la lista de héroes que se mostrarán.

	
					nota: Por tanto La plantilla, los metadatos y los componentes describen una vista.
					  		Los metadatos en @Component le indican a Angular dónde obtener los principales bloques de construcción que especifiques para el componente.	

					otros decoradores:  
						@Injectable, @Input y @Output 


///////////////////
Enlaces de datos(Data binding): 

			Angular soporta la "vinculación de datos": 
				un mecanismo para coordinar partes de una plantilla con partes de un componente. 
						Agrega un markup de enlace de datos a la plantilla HTML para decirle a Angular cómo conectar ambos lados.

				4 formas de sintaxis para producir la  vinculación de datos: 
						Cada forma tiene una dirección – hacia el DOM, desde el DOM, o en ambas direcciones.

						<li>{{hero.name}}</li>   						   --> interpolación: desde el componente hacia el Dom(plantilla)
						
						<hero-detail [hero]="selectedHero"></hero-detail>  --> (property binding):  igual que el anterior , pero ahora se recupera como un 	
																									atributo de una etiqueta.
						<li (click)="selectHero(hero)"></li>				--> (event binding): Desde el Dom(Plantilla) hacia el componente.
																									llama a un método del componente a través de un evento determinado y pasar un valor(hero)

						<input [(ngModel)]="hero.name">                     -->	(Two-way data binding): En ambos sentidos con [(ng-model)] = “propiedad”	
																										   * asocia una propiedad y un evento de una sola vez. 
																										   	Cualquier cambio que haga el usuario se pasará al componente y viceversa.
																										   	combina la propiedad y la vinculación de eventos en una sola notación, utilizando la directiva ngModel.


																										   	-- fluye al elemento input desde el componente como un enlace de propiedad. []

																										   	 -- Los cambios del usuario también fluyen de nuevo al componente, restableciendo la propiedad al valor más reciente, como con el enlace de evento. ()



							**Desde el componente hacia el DOM ”La interpolación” mediante {{hero.name}}.
								 En este caso se está recuperando directamente el valor de una variable que existe en el componente. Por tanto  muestra el valor de la propiedad hero.name del componente dentro del elemento <li>

							**Desde el componente hacia el DOM mediante [propiedad] = “valor” (property binding). Igual que el anterior , pero ahora se recupera como un atributo de una etiqueta. El enlace de propiedad [heroe] pasa el valor de selectedHero desde el componente HeroListComponent padre a la propiedad hero del componente hijo HeroDetailComponent.

							**Desde el DOM hacia el componente mediante (evento) = “manipulador” (event binding): En este caso se puede llamar a un método del componente a través de un evento determinado y pasar un valor. El enlace de evento (click) llama al método selectHero del componente cuando el usuario hace clic en el nombre de un héroe.

 							**En ambos sentidos con [(ng-model)] = “propiedad” (Two-way data binding). Esto asocia una propiedad y un evento de una sola vez. Cualquier cambio que haga el usuario se pasará al componente y viceversa. La vinculación de datos de dos vías es una cuarta forma importante que combina la propiedad y la vinculación de eventos en una sola notación, utilizando la directiva ngModel.
								nota:En este enlace de dos vías, el valor de la propiedad hero.name fluye al elemento input desde el componente como un enlace de propiedad. Los cambios del usuario también fluyen de nuevo al componente, restableciendo la propiedad al valor más reciente, como con el enlace de evento.


    nota:
		El data binding es importante para la comunicación entre la plantilla y el componente, y también es importante para la comunicación entre los componentes principales y los secundarios.
		El enlace de datos juega un papel importante en la comunicación entre una plantilla y su componente.
		El enlace de datos también es importante para la comunicación entre los componentes padre e hijo.



///////////////////
Directivas
		Las plantillas en Angular son dinámicas. Cuando Angular las renderiza, convierte al DOM de acuerdo con las instrucciones dadas por las directivas.

		Una directiva es una clase con un decorador @Directive. Un componente es una directiva-con-una-plantilla; Un decorador @Component es en realidad un decorador @Directive extendido con características orientadas a plantillas.

		Mientras que un componente es técnicamente una directiva, los componentes son tan distintivos y centrales para las aplicaciones Angular que al describir la arquitectura de una aplicación, se separa a los componentes de las directivas.

		Existen otros dos tipos de directivas: las estructurales y las de atributos.
			Tienden a aparecer dentro de una etiqueta de elemento como lo hacen los atributos, a veces por su nombre pero más a menudo como destino de una asignación o vinculación.

					Las directivas estructurales: alteran el diseño añadiendo, eliminando y reemplazando elementos en el DOM.

						directivas estructurales:
							<li *ngFor="let hero of heroes"></li>    -->*ngFor le dice a Angular que imprima un elemento <li> por cada héroe en la lista 
																			de heroes.
							<hero-detail *ngIf="selectedHero"></hero-detail>  -->*ngIf incluye el componente HeroDetail sólo si existe un héroe seleccionado.

					Las directivas de atributo: alteran la apariencia o el comportamiento de un elemento existente. 
												En las plantillas se ven como atributos HTML normales, de ahí el nombre.

							directivas de atributos:												
					<input [(ngModel)]="hero.name">  --> ngModel modifica el comportamiento de un elemento existente (normalmente un <input>) 
														 estableciendo su propiedad de valor de visualización y respondiendo a eventos.

		nota:
		otras directivas: ngSwitch, ngStyle y ngClass.
		se puede crear directivas personalizadas.

////////////////
  Servicios: (modelos)
		Los servicios abarcan una amplia categoría que incluye cualquier valor, función o característica que tu aplicación necesite.
		Casi cualquier cosa puede ser un servicio. Un servicio es una clase con un propósito pequeño y bien definido. Debe hacer algo específico y hacerlo bien.
			 Algunos ejemplos pueden ser:
					Servicio de logging
					Servicio de datos
					Bus de mensajes
					Calculadora de impuestos
					Configuración de la aplicación


					Es una clase a la que le hemos definido determinadas funcionalidades. Ahora podremos incluirlo en cualquier parte de nuestra aplicación en la que nos hagan falta estas funcionalidades.

						Los servicios están en todas partes.
						Los componentes deben estar lo más limpios posibles, delegando todo lo que se pueda a los servicios (validación de entrada del usuario, mostrar mensajes por consola, obtener datos del servidor…). El componente se centrará en la experiencia de usuario, presentando propiedades y métodos para el data binding, y dejará todo lo trivial a los servicios.

						El trabajo de un componente es permitir la experiencia del usuario y nada más. Media entre la vista (representada por la plantilla) y la lógica de la aplicación (que a menudo incluye alguna noción de un modelo). 
						Un buen componente presenta propiedades y métodos para la unión de datos, y delega todo lo no trivial a los servicios.

						Angular no hace cumplir estos principios. No se quejará si escribes un caótico componente con 3000 líneas.
						Cuanto más abusemos de los servicios, más organizado tendremos el código, será más fácil de mantener y de testear.
						
						Angular te ayuda a seguir estos principios haciendo fácil de modificar tu lógica de aplicación en servicios y "poner esos servicios a disposición de los componentes a través de la inyección de dependencias".		



////////////////////
Inyección de dependencias
				

					La inyección de dependencias es una manera de proporcionar una nueva instancia de una clase con las dependencias que requiere. La mayoría de las dependencias son servicios. Angular utiliza la inyección de dependencias para proporcionar nuevos componentes con los servicios que necesitan.

					Angular puede saber qué servicios necesita un componente mirando los tipos de sus parámetros del constructor. Por ejemplo, el constructor de tu HeroListComponent necesita un HeroService:

						constructor(private service: HeroService) { }


						Para que Angular pueda crea un componente, primero solicita un inyector (injector) para los servicios que requiere el componente.

						Un inyector mantiene un contenedor de instancias de servicios que has creado previamente. Si una instancia de servicio solicitada no está en el contenedor, el inyector crea uno y lo añade al contenedor antes de devolver el servicio a Angular. Cuando todos los servicios solicitados han sido resueltos y devueltos, Angular puede llamar al constructor del componente con esos servicios como argumentos. Esta es la inyección de dependencias.

El proceso de inyección de HeroService se parece un poco a esto:

Si el inyector no tiene un HeroService, ¿cómo sabe cómo hacer uno?
En resumen, debes haber registrado previamente un proveedor (provider) de HeroService en el inyector. Un proveedor es algo que puede crear o devolver un servicio, normalmente la clase del servicio en sí.
Puedes registrar proveedores en módulos o en componentes.
En general, agrega proveedores al módulo raíz (AppModule) para que la misma instancia de un servicio esté disponible en todas partes.
src/app/app.module.ts (module providers):
providers: [
  BackendService,
  HeroService,
  Logger
],
Alternativamente, regístrate a nivel de componente en la propiedad providers de los metadatos @Component:
src/app/hero-list.component.ts (component providers):
@Component({
  selector: 'hero-list',
  templateUrl: './hero-list.component.html',
  providers: [ HeroService ]
})
Registrarte a nivel de componente significa que obtendrás una nueva instancia del servicio con cada nueva instancia de ese componente.


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
Router
  src/index.html (base-href)
	<base href="/">  --> si la "carpeta app" es la raíz de la aplicación. Esto indica al al enrutador cómo componer las URL de navegación.


	import { RouterModule, Routes } from '@angular/router';



	La aplicación Angular enrutada:  tiene una instancia singleton del servicio Router. 
									Cuando la URL del navegador cambia, ese enrutador busca un correspondiente Route desde el que
									 puede determinar el componente que se va a mostrar.


						src/app/app.module.ts (excerpt) 

						const appRoutes: Routes = [
						  { path: 'crisis-center', component: CrisisListComponent },
						  { path: 'hero/:id',      component: HeroDetailComponent },
						  {
						    path: 'heroes',
						    component: HeroListComponent,
						    data: { title: 'Heroes List' }  //almacenar datos arbitrarios asociados con esta ruta específica.
						    								//propiedad data es accesible dentro de cada ruta activada
						    								//elementos como títulos de página, texto breadcrumb y otros datos estáticos de sólo lectura 
						  },
						  { path: '',   		 			//path vacío(empty path) o (Home) representa la "ruta predeterminada para la aplicación", el lugar 
						  									donde ir cuando la ruta en la URL está vacía, como suele ser al principio(home). Esta ruta predeterminada redirecciona a la ruta para las URL /heroes 


						    redirectTo: '/heroes',
						    pathMatch: 'full'
						  },
						  { 
						   path: '**',					  	//un comodín '**': enrutador seleccionará esta ruta si la URL solicitada no coincide con ninguna de 								//las rutas definidas anteriormente en la configuración. Esto es útil para mostrar una página "404 - 
						   								   //	Not Found" o para redirigir a otra ruta.

						   component: PageNotFoundComponent 
						   }
						];

						@NgModule({
						  imports: [
						    RouterModule.forRoot(
						      appRoutes,
						      { enableTracing: true } // <-- debugging purposes only
						    )
						    // other imports here
						  ],
						  ...
						})
						export class AppModule { }


						nota: Si necesita ver qué "eventos están ocurriendo durante el ciclo de vida de la navegación", existe la opción enableTracing como parte de la configuración predeterminada del enrutador. Esto genera cada evento de enrutador que tuvo lugar durante cada ciclo de vida de navegación a la consola del navegador. Esto sólo debe usarse para propósitos de depuración . Establece la "opción enableTracing: true" en el objeto pasado como el segundo argumento al método RouterModule.forRoot().



       <router-outlet></router-outlet>:
		<!--Cuando cambia la url del navegador, el enrutador checa a que path coincide y 
		 después de un Router-Outlet que ha colocado en el HTML. Se muestra el componente especificado -->						



		 ¿cómo navegas?:
		      - La URL puede llegar directamente desde la barra de direcciones del navegador
		      - Resultado de alguna acción del usuario, como el clic de una etiqueta <a>.

					      <li class="nav-item" routerLinkActive="active" > 
						        	<a class="nav-link" [routerLink]="['home']" >Home</a> 
						  </li>

						  directivas RouterLink: de las etiquetas <a> proporcionan al router el control sobre dichos elementos
						  directiva RouterLinkActive: como padre de etiqueta <a> ayuda a distinguir visualmente el <a> de la ruta "activa" actualmente seleccionada. El enrutador agrega la "clase CSS active" al elemento cuando el RouterLink asociado se activa

			  - boton : 
						  
						  //cdo quiero usar un <button> y redireccionar con parametros, por programación (navigate)

					      <button  (click)="verHeroe(i)"
					        type="button" class="btn btn-outline-primary btn-block">
					        ver más..
					      </button>

					  	  import { Router } from '@angular/router';
						  constructor(private _miservicio:HeroesService,
						              private ruta:Router   //*************************
						                ) {
						    //this.heroes = _miservicio.getHeroes();
						  }

						   verHeroe(idx:number){
						    //console.log(idx);
						    this.ruta.navigate(['/heroe',idx]);
						  }			  


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
ngModel
	Cuando usamos el ngModel, estamos obligados a poner la "propiedad name"
	Son las "clases que se agregan cuando uso  ngModel en un input"

	ng-untouched: es que el usuario no ha tocado la caja de texto
	ng-pristine: es que la caja esta con su valor por defecto
	ng-valid: Esta pasando todas las reglas de validación impuesta a ese componente

	ng-dirty: Esto significa que el componente esta sucio, es decir ya no esta ng-pristine, sino que la caja cambio su valor por defecto. Esto sucede cuando se escribe en la caja de texto
	ng-touched: Va a suceder cuando cambiemos los valores por defecto y movamos el control fuera de la caja(perdamos el foco).

		Cuando ademas agregamos el "atributo required", agrega el "atributo ng-invalid":
		ng-invalid: Esta pasando todas las reglas de validación impuesta a ese componente
					para quitar el mensaje que envia en el formulario padre especificamos el novalidate

<form novalidate=””>
	<input class=”form-control”
		text=”text”
		placeholder=”nombre”	
		name=”nombre”
		ngModel
		minLength=”5”
		required
	>
<form>

	Posteo en la información(cdo se hace submit). Como mandamos los datos algún lado
			<form (ngSubmit)=”funGuardar()” novalidate=””>
			esto va a llamar una funcion cdo el formulario haga un submit(haga el posteo de información)

						nos creamos en el .ts la funcion, que es la que se va a encargar de manejar el posteado
				guardar(){
				   console.log('Envio de submit');
				}

			Se invoca a la funcion cada vez que se da enter en cada caja de texto que tenga asociado un ngModel,  o cuando se invoca un input que tenga submit
			Hasta aqui hemos hecho submit, pero no hemos enviado datos 

			Como podemos obtener tanto data como formulario completo, es decir los valores que envia el submit. 
			Para esto usando la  "aproximación por template" tendremos que hacer “una referencia local a este objeto” #miform=”ngForm”

			<form (ngSubmit)=”funGuardar(miform)” #miform=”ngForm” novalidate=””>
						guardar(miform:any){
						   console.log(miform);
						}

			Recibe un  Objeto de tipo NgForm con todas las propiedades, entre ellas una de tipo Object que es value:( que tambien es un objeto con los valores que se envian)

					Importando el tipo  NgForm, para que typeScript sepa que propiedades y métodos tiene ese tipo
					Import {NgForm} from '@angular/forms'   
					guardar(miform:NgForm){
					   console.log(miform);
					   console.log(miform.value);
					   console.log(this.usuario);
					}
