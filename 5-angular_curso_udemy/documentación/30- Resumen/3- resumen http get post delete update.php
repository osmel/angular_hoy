
Las aplicaciones front-end se comunican con servicios back-end a través del protocolo HTTP.
Los navegadores modernos admiten dos API diferentes para realizar peticiones HTTP: 
	- la interfaz  XMLHttpRequest 
	- la API fetch().

Angular proporciona una API simplificada para la funcionalidad HTTP (@angular/common/http). Construyendo sobre la interfaz XMLHttpRequest expuesta por los navegadores.


  Configuración: Instalación del Modulo
    --Para poder utilizar el HttpClient, es necesario instalar el HttpClientModule(este es quien lo prrporciona). 

		    // app.module.ts:
			import {NgModule} from '@angular/core';
			import {BrowserModule} from '@angular/platform-browser';

			//Importar HttpClientModule que es quien proporciona trabajar con el HttpClient
			//Con esto ya se podrá inyectar HttpClient en los "componentes y servicios".

			import {HttpClientModule} from '@angular/common/http';

			@NgModule({
			  imports: [
			    BrowserModule,
			    HttpClientModule,  // Incluirlo despues de BrowserModule.
			  ],
			})
			export class MyAppModule {}



**Tipos de solicitudes(request) a los datos
/////////
-- Metodo GET	
		https://angular.io/api/common/http/HttpClient 
		sintaxis: get(url: string, options: {...}): Observable<any>


		{
		  "results": [
		   	 "Item 1",
		  	  "Item 2",
		  ]
		}
		
		/		
		export class MyComponent implements OnInit {
		  results: string[];
		 
		  constructor(private http: HttpClient) {}   // Inyectar HttpClient en el componente o servicio.

		  ngOnInit(): void {
		     
		      this.http.get('/api/items').subscribe(data => {   // Realizar la peticion(request) HTTP:
			   // Leer el campo de resultado de la respuesta JSON.
		           this.results = data['results']; //results es el root
		      });
		  }
		}			


--Verificación de tipo
	
	Evitando que use data['results'] en vez de usar data.results
	Cuando HttpClient analiza(parsea) la respuesta JSON en un Object, no sabe qué forma tiene ese objeto.
	Podemos indicarle a HttpClient qué tipo de respuesta esperamos. Definiendo una interfaz con los campos que esperamos:


	interface ItemsResponse {
	  results: string[];
	}


	Luego, al realizar la llamada HttpClient.get, pase un parámetro de tipo "interfaz definida":
		http.get<ItemsResponse>('/api/items').subscribe(data => {
			  this.results = data.results; //data ahora es una instancia de tipo ItemsResponse, por lo que puedes hacer esto:
		});


-- Lectura de respuesta Completa(Reading the full response)
	
	  

	  Puedo indicarle a HttpClient que desea la respuesta completa en lugar de sólo el body con la "opción observe":
		  -encabezados(headers) especiales 
		  -códigos de status 
		  -etc

		http
		  .get<MyJsonData>('/data.json', {observe: 'response'})  //parametro observe que me permite tener una respuesta de tipo HttpResponse
		  .subscribe(resp => {
		     // Aquí, resp es de tipo HttpResponse<MyJsonData>.
		     // Puede inspeccionar sus encabezados:
		    console.log(resp.headers.get('X-Custom-Header'));
		    // Y acceder directamente al body, que se escribe como MyJsonData según el request.
		    console.log(resp.body.someField);
		  });


-- Manejo de errores(Error handling)
	Para manejar los errores en caso de fallo, agregue un controlador de errores a su llamada .subscribe():

		http
		  .get<ItemsResponse>('/api/items')
		  .subscribe(
		    
		    data => {...},  //respuesta con exito
		    err => {  //respuesta con fallos
		      console.log('Something went wrong!');
		    }
		  );



-- Obtener detalles de error(Getting error details)
	Hay dos tipos de errores que pueden ocurrir:
		 - Del "lado del server" devuelve un código de respuesta sin éxito (404, 500, etc.), se retorna como un error. 
		 - Del lado del cliente, como una excepción se lanza en un operador RxJS, o si un error de red impide que la solicitud se complete correctamente, se lanzará un Error real.

		http
		  .get<ItemsResponse>('/api/items')
		  .subscribe(
		    data => {...},
		    (err: HttpErrorResponse) => {  //En cualquier caso de error, se puede mirar el "HttpErrorResponse" para averiguar qué sucedió.
		      if (err.error instanceof Error) {  // Se produjo un error en el lado del cliente o en la red. Manejarlo en consecuencia.
		        console.log('An error occurred:', err.error.message);
		      } else {  // Del lado del server devuelve un código de respuesta fallido.  El cuerpo de respuesta puede contener pistas sobre lo que salió mal,
		        console.log(`Backend returned code ${err.status}, body was: ${err.error}`);
		      }
		    }
		  );



-- Solicitar datos no JSON(Requesting non-JSON data)

				No todas las API retornan datos JSON. Suponga que desea leer un archivo de texto en el servidor. 
				Tienes que decirle a HttpClient que esperas una respuesta textual:
			http
			  .get('/textfile.txt', {responseType: 'text'})   // Opcion para solicitar datos de un tipo especifico, 
				   // El Observable retornado por get () es del tipo Observable<string>
				   // porque se especificó una respuesta de texto. No hay necesidad de pasar
				   // un parámetro de tipo <string> para get(). Es decir no hay necesidad de hacer esto .get<string>
			  .subscribe(data => console.log(data));


//////////
-- Envío de datos al servidor(POST, GET, ETC)			  
		 sintaxis: post(url: string, body: any | null, options: {...}): Observable<any>

		const datosBody = {name: 'Brad'};
		http
		  .post('/api/developers/add', datosBody)
		  .subscribe(...);

--Configuración de otras partes del request(solicitud)
	
	objeto options: para otros aspecto del request de salida que se puede configurar
	

	http
	  .post('/api/items/add', datosBody, {
	    headers: new HttpHeaders().set('Authorization', 'my-auth-token'),   //agregar un encabezado de Authorization a los request salientes
	  })
	  .subscribe();

	  nota: La "clase HttpHeaders es inmutable", por lo que set() retorna una instancia nueva y aplica los cambios.

-- URL Parameters
	

		http
		  .post('/api/items/add', body, {
		    params: new HttpParams().set('id', '3'),  //adición un parámetros a la URL. Ejemplo: /api/items/add?id=3
		  })
		  .subscribe();





////////////////////////////
--Uso avanzado (interceptores)

	  "declarar interceptores": que se sitúan entre su aplicación y el backend
	     						Cuando la aplicación hace un request, los "interceptores la transforman antes de enviarla al servidor", y los interceptores pueden "transformar la respuesta en su camino de regreso antes de que su aplicación lo vea". 

	     						Esto es útil para todo, desde la autenticación hasta el registro.






Para implementar un interceptor: se declara una "clase que implementa HttpInterceptor", que "tiene un solo método intercept()". 


		Ejemplo de un interceptor simple que no hace nada sino que reenvía la petición sin alterarla:

			import {Injectable} from '@angular/core';
			import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';
			@Injectable()
			export class NoopInterceptor implements HttpInterceptor {     //clase que implementa el interceptor
			  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {  //método que intercepta el request, para transformarlo
			    return next.handle(req);  //No hace nada simplemente una llamada next.handle al request original, enviándola sin modificarla en absoluto.
			  }
			}

				nota: recibe request con "req", y una vez transformado lo envia con "next"

						Intercept: Es un método que "transforma una petición" en un Observable que eventualmente retorna la response. 
					   				En este sentido, cada interceptor es completamente responsable de manejar el request por sí mismo.


									La mayoría de las veces, sin embargo, los interceptores harán un pequeño cambio en el request y lo enviarán al resto del encadenamiento. Es ahí donde el parámetro next entra . 
						Next: Es un HttpHandler, una interfaz que, similar a intercept, transforma un request en un Observable para la response. En un interceptor, next siempre representa el siguiente interceptor en la cadena, si es que existe, o el backend final si no hay más interceptores. Así que la mayoría de los interceptores terminarán llamando a next en el request que transformaron.





