///////////////////////////////////////////////////////////////////////////////////////
////////////////////cap38 - "Directivas estructurales"  ngIF ngFOR/////////////////////
/////////////////////////////////////////////////////////////////////////////////////
  ngOnInit() { //se usa para cargar cdo toda la pagina esta renderizada, es decir cdo esta listo el DOM
  }


		Angular interpreta por igual
		[src]="heroe.img" o src="{{heroe.img}}"
		<img class="card-img-top img-fluid" [src]="heroe.img" [alt]="heroe.nombre">  				


*ngIf="false" -->que no muestre nada. Ademas el "elemento no existe lo destruye del dom"
				--Aparece una referencia

*ngFor : ciclo "let variable of arreglo" : repite la cantidad del arreglo
			<li *ngFor="let varPersona of personajes" class="list-group-item">Cras justo odio</li>

		variable conteo de ciclo "index"	
		<li *ngFor="let persona of personajes; let i = index" class="list-group-item">
        	{{i}}.{{persona}}
        </li>			

invocando evento click desde html. Tomando en cuenta la "variable mostrar" q cree
(click) ="mostrar=!mostrar">				

  	<button type="button" class="btn btn-outline-primary btn-block"
      (click) ="mostrar=!mostrar">
      Mostrar/Ocultar
    </button>



/////////////////////////////////////////////////////////////////////////////////////
////////////////////52- Servicios/(es como un MODELO) //////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

		vamos a trabajar desde un servicio, porq cdo trabajemos a traves de api, todo deberia ser gestionado a traves de un servicio, porq es lo más práctico
		crearemos un servicio "heroe", q será encargado de mostrar o manejar la información referente a los heroes de nuestra APP
		
		Caracteristica de los servicios:
		 - Brindar información a quien lo necesite
		 - Realizar peticiones CRUD (crear, leer, actualizar, eliminar)
		 - Mantener la data de forma persistente ( datos listo siempre, para q los clientes entren y obtengan la informacion. Por ejemplo para hacer peticiones por http)
		 - Servir como recurso re-utilizable para nuestra APP(varios puedan acceder a la misma data)


 Creando nuestro 1er servicio.
  		ng g s servicios/heroe  //Creo o Defino el servicio "heroes.service.ts"
 	     import { Injectable } from '@angular/core';
			@Injectable()  
			export class HeroesService {
			  constructor() {
			    console.log('servicios listo para usar!!!');
			  }
			}

		2- Le digo a angular q disponga de ese servicio "app.module.ts"
		 //servicios
			import { HeroesService } from './servicios/heroes.service';	
			  providers: [ //providers=services
			    HeroesService
  			  ],

  		3- Usar el servicio
  		   -voy al componente en este caso "heroes.components.ts"	  
  		     - importo el servicio
	  		     import { HeroesService } from '../../servicios/heroes.service';	
	  		 - lo paso como parametro al constructor (injecto)
	  		    constructor(private _miservicio:HeroesService) { 

  				}    
  		nota: cuando termino aqui inmediatamente dispara el constructor de servicio
  		




/////////////////////////////////////////////////////////////////////////////////////
////////////////////pipe 57 //////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////


PIPES: no es más que una transformación visual de la data.
pipes(tuberías o filtros  | ): Son pequeñas funciones que nos sirven para realizar alguna tarea en nuestra vista o plantilla de Angular 2. Sirven para transformar la data de una manera visual. Pero el valor sigue siendo el mismo.

	Pipes uppercase y lowercase
	Pipe Slice
	Pice Decimal
	Pipe Percent
	Pipe Currency
	Pipe Json
	Pipe Async
	Pipe Date
	Pipes personalizados
	Capitalizar palabras y nombres
	Creación de un pipe, que permite cargar recursos externos de forma segura.
	Al final de la sección tendremos una tarea para afianzar los conocimientos creando un pipe personalizado que nos permitirá ocultar un texto a voluntad.

https://angular.io/guide/pipes
https://angular.io/api?status=stable&type=pipe
http://voidcanvas.com/angular-2-pipes-filters/




Vamos a definir una propiedad publica en nuestra clase del componente:
	texto="osmel calderon";
	public fecha = new Date(2012, 8, 17);
Y en la plantilla de este componente usaremos los filtros de esta forma:
	{{texto | uppercase}}	  --> OSMEL CALDERON
	{{  fecha | date:'fullDate' | uppercase}}  -->MONDAY, SEPTEMBER 17, 2012	

Primero tenemos el dato a transformar que en este caso sería la propiedad fecha, después con una tubería usamos el pipe date y le pasamos un parámetro que en este caso sería fullDate y luego encadenamos otra pipe para convertir el texto a mayúsculas.
Esto nos imprimirá lo siguiente: MONDAY, SEPTEMBER 17, 2012	


  
  
 
*LowerCasePipe (Transforma a Minuscula)
	import { LowerCasePipe } from '@angular/common';

*UpperCasePipe(Transforma a Mayuscula) 
   import { UpperCasePipe } from '@angular/common'; 
 
*SlicePipe (Cortar o eliminar subcadenas
			Crea nueva cadena o lista con un subconjunto de elemento) 

    import { SlicePipe } from '@angular/common';
 
 
*TitleCasePipe(Capitaliza Letra)
  import { TitleCasePipe } from '@angular/common';


*DatePipe  
   import { DatePipe } from '@angular/common';
   https://angular.io/api/common/DatePipe


* DecimalPipe  
  import { DecimalPipe } from '@angular/common';
  number_expression | number[:digitInfo]

  digitInfo es una cadena que tiene el siguiente formato:
	{minIntegerDigits}.{minFractionDigits}-{maxFractionDigits}

	minIntegerDigits: mínimo número de dígitos enteros a utilizar. El valor predeterminado es 1.
	minFractionDigits mínimo número de dígitos después de la fracción. El valor predeterminado es 0.
	maxFractionDigits máximo número de dígitos después de la fracción. El valor predeterminado es 3.



* PercentPipe (Formatea un número como un porcentaje %) 
   import { PercentPipe } from '@angular/common';
   	number_expression | percent[:digitInfo]
   	{{b | percent:'4.3-5'}}

* CurrencyPipe (formatear número como moneda) 
    import { CurrencyPipe } from '@angular/common';

    number_expression | currency[:currencyCode[:symbolDisplay[:digitInfo]]]

    

	currencyCode: Es el codigo USD, EUR
	symbolDisplay(boolean): Indica si se usa simbolo o codigo
		true: Usa simbolo. Ejemplo $
		false:(default): Usa codigo (Ejemplo USD).
	digitInfo: formato {minIntegerDigits}.{minFractionDigits}-{maxFractionDigits}

	WARNING: this pipe uses the Internationalization API which is not yet available in all browsers and may require a polyfill. See Browser Support for details.


*JsonPipe (Convierte el valor en una cadena JSON. Si lo pones entre etiquetas <pre>, se verá mejor formateado)
	import { JsonPipe } from '@angular/common';

	object: Object = {foo: 'bar', baz: 'qux', nested: {xyz: 3, numbers: [1, 2, 3, 4, 5]}};

	 <pre>{{object | json}}</pre>


AsyncPipe: (Desenvuelve un valor desde una primitiva asíncrona.)
import { AsyncPipe } from '@angular/common';

Resumen



pipe en español
	import { LOCALE_ID } from '@angular/core';
	import localeEs from '@angular/common/locales/es';
	import { registerLocaleData } from '@angular/common';
	registerLocaleData(localeEs);
 	providers: [ { provide: LOCALE_ID, useValue: 'es' } ],




https://medium.com/@alebrozzo/mi-empresa-me-pidi%C3%B3-una-presentaci%C3%B3n-general-sobre-angular-2-esto-es-lo-que-arm%C3%A9-b04eeacc3164


Comenzamos el 71