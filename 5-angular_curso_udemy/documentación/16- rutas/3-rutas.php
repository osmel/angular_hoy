El enrutador de Angular es un NgModule externo(es decir es un modulo) opcional llamado "RouterModule".
El enrutador es una combinación de múltiples servicios (RouterModule), directivas (RouterOutlet, RouterLink, RouterLinkActive), y su configuración (Routes). 


/////////////////////////////////////////////////////////////////////////////////////
////////////////////RUTAS EN ANGULAR 47//////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

nota: Me va faltando definir, las rutas que estaran protegidas para un determinado usuario

 	rutas: Nos va a permitir navegar a diferentes paginas(diferentes componentes). Sin hacer refresh del navegador web
 			con snniper saco la sintaxis basicas de routers escribiendo-> 	  "ng2rou" y enter
 	
 	src/app/app.routes.ts
 	
		import { RouterModule, Routes } from '@angular/router';  //Aqui estamos definiendo las rutas "app.routes.ts"
		import { HomeComponent } from './components/home/home.component';
		const APP_ROUTES: Routes = [
		  { path: 'home', component: HomeComponent },  //cada ruta tiene un path y un componente(estos componentes deben importarse aqui)
		  { path: '**', pathMatch: 'full', redirectTo: '' }  //esta es la ruta por defecto, que tomara sino coincide ninguna
		];
		export const app_routing = RouterModule.forRoot(APP_ROUTES);




		//"src/app/app.module.ts": Aqui le decimos a angular que disponga de esas rutas 
		
				import { APP_ROUTING } from './app.routes';  //rutas  "APP_ROUTING" es el nombre que exporta el fichero './app.routes'
				imports: [
					APP_ROUTING,
					...
				]	


		//si usas este sin hash, asegurarse de que en index.html aparezca esto   <base href="/">
			export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
		//useHash:true ->para el uso de parametros
		//export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES,{useHash:true});
		

 </router-outlet>: para hacer uso de las rutas. Fijo en que punto yo quiero renderizar las paginas que tengo declaradas en el router hasta aqui tendria q poner a mano el hash, y funciona, pero implementaremos con <a> la navegacion
				<div class="container">
				  <router-outlet></router-outlet>
				</div>

48- Moviendonos por las rutas

//vamos a implementar la navegacion con <a>, 


sino estamos usando la navegacion HASH esto no va a funcionar
<a class="nav-link" href="#/home">Home</a>
puede ser que se cambie en un futuro la navegacion y lo de encima no funcionaria

vamos a usar elemento [routerLink]: que es un arreglo de parametros
	
	localhost:4200/home/pagina/1/3
	[routerLink]="['home','pagina','1','3']"

	<a class="nav-link" [routerLink]="['home','pagina','1','3']" >Home</a>

	sustituir href por [routerLink]

	  <li class="nav-item">
        	<a class="nav-link" [routerLink]="['home']" >Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" [routerLink]="['heroes']">Heroes</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" [routerLink]="['about']">About</a>
      </li>



routerLinkActive ="nombreClase que necesitamos cdo esta activo": Para que deje activo el elemento
en este caso la clase que necesitamos es "active"
      <li class="nav-item" routerLinkActive="active">
        <a class="nav-link" [routerLink]="['about']">About</a>
      </li>      


nota: angular al encontrar un [routerLink] va a fijarse en el elemento padre superior si tiene un routerLinkActive,
     y en caso de que lo tenga pondra las clases que este tiene cdo se active el elemento



ngOnInit() -> se ejecuta cdo la pagina esta renderizada, es decir el dom esta listo, para usarse 






53- moviendonos a Rutas con parametros


		//crear un componente q nos sirva para mostrar los detalles de cada heroe
		 ng g c componets/heroe

		//router definir q espera un parametro
			import { HeroeComponent } from './components/heroe/heroe.component';
			{path:'heroe/:id',component:HeroeComponent },

		//Cuando quiero hacer un enlace desde html con <a> de simple uso el [routerLink]
		   <a class="nav-link" [routerLink]="['/heroe',i]">Heroes</a>

		//cdo quiero usar un <button> y redireccionar con parametros, por programación (navigate)

		      <button  (click)="verHeroe(i)"
		        type="button" class="btn btn-outline-primary btn-block">
		        ver más..
		      </button>

		  	  import { Router } from '@angular/router';
			  constructor(private _miservicio:HeroesService,
			              private ruta:Router
			                ) {
			    //this.heroes = _miservicio.getHeroes();
			  }

			   verHeroe(idx:number){
			    //console.log(idx);
			    this.ruta.navigate(['/heroe',idx]);
			  }



54- Recibiendo parametros desde la url (hash)
   
   
     import { ActivatedRoute } from '@angular/router';  //primero importamos el paquete "ActivatedRoute"

    constructor(private rutaActiva: ActivatedRoute){
		
		 this.rutaActiva.params.subscribe(parametro => {
		        console.log(parametro); //esto va a regresar un objeto con todos los parametros en este caso id:0
		        
		  });
    }


****this.rutaActiva.params-> devuelve un obserbable al cual nos suscribimos 