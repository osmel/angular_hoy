(function() {
/*

Los Observable: 
				- es una clase que emite un flujo de datos o eventos.
				- están orientados a gestionar flujos asíncronos.

				 - Sintaxis de Creación de un observable 
				 - Rx.Observable.operador

			

  
*/

			//* Create - crear un observable desde cero llamando a los métodos de observer mediante programación

				function miFuncion(observer){
					
					try { 
					    observer.next(1); 
					    observer.next(2); 
					    observer.next(3); 
					    observer.complete(); 
					    observer.next(4); // No se entrega porque violaría el contrato. Ya tuvo complete
					  } catch (err) { 
					    observer.error(err); // Entrega una notificación de “error” si sucede uno
					  } 
				}

				//aqui creo un observable, es decir un flujo de datos apartir de una funcion personalizada
				//en la funcion personalizada invoco los metodos, next, complete y error
  				var miObservable = Rx.Observable.create( miFuncion );

  				/* analogamente al anterior, para no separarlo en 2 pasos hubiese podido ser asi directamente 
  				   incluso el nombre "miFuncion", no es necesario ponerlo
  				var miObservable = Rx.Observable.create(function miFuncion(observer){
					try { 
					    observer.next(1); 
					    observer.next(2); 
					    observer.next(3); 
					    observer.complete(); 
					    observer.next(4); // No se entrega porque violaría el contrato. Ya tuvo complete
					  } catch (err) { 
					    observer.error(err); // Entrega una notificación de “error” si sucede uno
					  } 
				} );

  				*/

	


	/*
	- Suscribirse a un Observable es como llamar a una función, proporcionando devoluciones de llamada donde se entregarán los datos.
	- Cada llamada a observable.subscribe desencadena su propia configuración independiente para cada Observers. 
	   Para este observador hay toda una configuración diferente que para otro observador que este suscrito al mismo sujeto
	- Una llamada al subscribe es simplemente una forma de iniciar una “ejecución observable” y 
		entregar valores o eventos a un observer de esa ejecución.

	Cuando se llama a "observable.subscribe", el observer se une a la ejecución recién creada del observable, 
	pero también esta llamada devuelve un "objeto de suscripción" en este caso   "suscribirme"
	Los Observers son simplemente un conjunto de callbacks, 
	uno para cada tipo de notificación entregado por el Observable: (next, error, y complete).
	
	*/
					var suscribirme = miObservable.subscribe({
					  next : x => console.log('x vale', x),
					  error: err => console.log('algo salio mal', err),
					  complete: () => console.log('se completo'),
					});


					//Los tres tipos de devoluciones de llamada pueden proporcionarse como argumentos:
					var suscribirme2 = miObservable.subscribe(
					     x => console.log('x vale', x),  //Como ocurre con la expresión yield, se pueden lanzar n next, los cuales devuelven un valor.
					   err => console.log('algo salio mal', err), // Se lanza cuando sucede un error.
					    () => console.log('se completo'),  //No devuelve nada y se lanza cuando ha terminado la secuencia de next().
					 );
					 

					//Los tres tipos de devoluciones de llamada pueden proporcionarse como funciones:
					var suscribirme3 = miObservable.subscribe(
		  				  function (x) {   //o   function miFuncion(x) { 
						    console.log('Next: %s', x);
						  },
						  function (err) {
						    console.log('Error: %s', err);
						  },
						  function () {
						    console.log('Completed');
						  }
					 );
					 

/*defer
	var clicksOrInterval = Rx.Observable.defer(function () {
	  if (Math.random() > 0.5) {
	    return Rx.Observable.fromEvent(document, 'click');
	  } else {
	    return Rx.Observable.interval(1000);
	  }
	});
	clicksOrInterval.subscribe(x => console.log(x));
*/

/*Empty
	var result = Rx.Observable.empty();
	
	//Emite el número 7, luego completa.
	var result = Rx.Observable.empty().startWith(7);

	var suscribirme = result.subscribe({
					  next : x => console.log('-- ', x),
					  error: err => console.log('-  ', err),
					  complete: () => console.log('se completo empty'),
					});
	*/

/*never
	//Emita el número 7, luego nunca emita nada más (ni siquiera completo).
	var result = Rx.Observable.never().startWith(7);
	var suscribirme = result.subscribe({
					  next : x => console.log('-- ', x),
					  error: err => console.log('-  ', err),
					  complete: () => console.log('se completo empty'),
					});
*/
/*throw
	//Emite el número 7, luego emite un error.
	var result = Rx.Observable.throw(new Error('oops!')).startWith(7);
	result.subscribe(x => console.log(x), e => console.error(e));
*/

//Convierte un array a un Observable
/* from 
	//array
	var array = [10, 20, 30];
	var result = Rx.Observable.from(array);
	result.subscribe(x => console.log(x));



	//generador iterable
	//Convierta un  iterable infinito (de un generador) a un Observable
	function* generateDoubles(seed) {
	  var i = seed;
	  while (true) {
	    yield i;
	    i = 2 * i; // double it
	  }
	}

	var iterator = generateDoubles(3);
	var result = Rx.Observable.from(iterator).take(10);  //para tomar solo los 10 primeros
	result.subscribe(x => console.log(x));


*/

/*interval
	//Emite números ascendentes, uno por segundo (1000 ms)
	var numbers = Rx.Observable.interval(1000);
	numbers.subscribe(x => console.log(x));
*/

/* of 
	//Emite 10, 20, 30, luego 'a', 'b', 'c', 
	var numbers = Rx.Observable.of(10, 20, 30);
	var letters = Rx.Observable.of('a', 'b', 'c');
*/
/*Range
	var numbers = Rx.Observable.range(1, 10);
	numbers.subscribe(x => console.log(x));

*/
/*Sintaxis: timer(retrasoInicial, periodo, planificador)

	//Emite números ascendentes, uno por segundo (1000 ms), comenzando después de 3 segundos
	var numbers = Rx.Observable.timer(3000, 1000);
	numbers.subscribe(x => console.log("timer: "+x));
	//Emite un número después de cinco segundos
	var numbers = Rx.Observable.timer(5000);
	numbers.subscribe(x => console.log(x));
*/
/*Sintaxis: repeat(numero)
	var numbers = Rx.Observable.of('repito 3 veces').repeat(3);  //repite 3 veces la fuente of()
	numbers.subscribe(x => console.log(x));

*/










})();	  				