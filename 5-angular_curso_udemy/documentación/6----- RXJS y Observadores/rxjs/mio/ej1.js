(function() {
/*

Los Observable: 
				- es una clase que emite un flujo de datos o eventos.
				- están orientados a gestionar flujos asíncronos.

				 - Sintaxis de Creación de un observable 
				 - Rx.Observable.operador

			

  
*/

			//* Create - crear un observable desde cero llamando a los métodos de observer mediante programación

				function miFuncion(observer){
					
					try { 
					    observer.next(1); 
					    observer.next(2); 
					    observer.next(3); 
					    observer.complete(); 
					    observer.next(4); // No se entrega porque violaría el contrato. Ya tuvo complete
					  } catch (err) { 
					    observer.error(err); // Entrega una notificación de “error” si sucede uno
					  } 
				}

				//aqui creo un observable, es decir un flujo de datos apartir de una funcion personalizada
				//en la funcion personalizada invoco los metodos, next, complete y error
  				var miObservable = Rx.Observable.create( miFuncion );

  				/* analogamente al anterior, para no separarlo en 2 pasos hubiese podido ser asi directamente 
  				   incluso el nombre "miFuncion", no es necesario ponerlo
  				var miObservable = Rx.Observable.create(function miFuncion(observer){
					try { 
					    observer.next(1); 
					    observer.next(2); 
					    observer.next(3); 
					    observer.complete(); 
					    observer.next(4); // No se entrega porque violaría el contrato. Ya tuvo complete
					  } catch (err) { 
					    observer.error(err); // Entrega una notificación de “error” si sucede uno
					  } 
				} );

  				*/

	


	/*
	- Suscribirse a un Observable es como llamar a una función, proporcionando devoluciones de llamada donde se entregarán los datos.
	- Cada llamada a observable.subscribe desencadena su propia configuración independiente para cada Observers. 
	   Para este observador hay toda una configuración diferente que para otro observador que este suscrito al mismo sujeto
	- Una llamada al subscribe es simplemente una forma de iniciar una “ejecución observable” y 
		entregar valores o eventos a un observer de esa ejecución.

	Cuando se llama a "observable.subscribe", el observer se une a la ejecución recién creada del observable, 
	pero también esta llamada devuelve un "objeto de suscripción" en este caso   "suscribirme"
	Los Observers son simplemente un conjunto de callbacks, 
	uno para cada tipo de notificación entregado por el Observable: (next, error, y complete).
	
	*/
					var suscribirme = miObservable.subscribe({
					  next : x => console.log('x vale', x),
					  error: err => console.log('algo salio mal', err),
					  complete: () => console.log('se completo'),
					});


					//Los tres tipos de devoluciones de llamada pueden proporcionarse como argumentos:
					var suscribirme2 = miObservable.subscribe(
					     x => console.log('x vale', x),  //Como ocurre con la expresión yield, se pueden lanzar n next, los cuales devuelven un valor.
					   err => console.log('algo salio mal', err), // Se lanza cuando sucede un error.
					    () => console.log('se completo'),  //No devuelve nada y se lanza cuando ha terminado la secuencia de next().
					 );
					 

					//Los tres tipos de devoluciones de llamada pueden proporcionarse como funciones:
					var suscribirme3 = miObservable.subscribe(
		  				  function (x) {   //o   function miFuncion(x) { 
						    console.log('Next: %s', x);
						  },
						  function (err) {
						    console.log('Error: %s', err);
						  },
						  function () {
						    console.log('Completed');
						  }
					 );
					 

/*defer
	var clicksOrInterval = Rx.Observable.defer(function () {
	  if (Math.random() > 0.5) {
	    return Rx.Observable.fromEvent(document, 'click');
	  } else {
	    return Rx.Observable.interval(1000);
	  }
	});
	clicksOrInterval.subscribe(x => console.log(x));
*/

/*Empty
	var result = Rx.Observable.empty();
	
	//Emite el número 7, luego completa.
	var result = Rx.Observable.empty().startWith(7);

	var suscribirme = result.subscribe({
					  next : x => console.log('-- ', x),
					  error: err => console.log('-  ', err),
					  complete: () => console.log('se completo empty'),
					});
	*/

/*never
	//Emita el número 7, luego nunca emita nada más (ni siquiera completo).
	var result = Rx.Observable.never().startWith(7);
	var suscribirme = result.subscribe({
					  next : x => console.log('-- ', x),
					  error: err => console.log('-  ', err),
					  complete: () => console.log('se completo empty'),
					});
*/
/*throw
	//Emite el número 7, luego emite un error.
	var result = Rx.Observable.throw(new Error('oops!')).startWith(7);
	result.subscribe(x => console.log(x), e => console.error(e));
*/

//Convierte un array a un Observable
/* from 
	//array
	var array = [10, 20, 30];
	var result = Rx.Observable.from(array);
	result.subscribe(x => console.log(x));



	//generador iterable
	//Convierta un  iterable infinito (de un generador) a un Observable
	function* generateDoubles(seed) {
	  var i = seed;
	  while (true) {
	    yield i;
	    i = 2 * i; // double it
	  }
	}

	var iterator = generateDoubles(3);
	var result = Rx.Observable.from(iterator).take(10);  //para tomar solo los 10 primeros
	result.subscribe(x => console.log(x));


*/

/*interval
	//Emite números ascendentes, uno por segundo (1000 ms)
	var numbers = Rx.Observable.interval(1000);
	numbers.subscribe(x => console.log(x));
*/

/* of 
	//Emite 10, 20, 30, luego 'a', 'b', 'c', 
	var numbers = Rx.Observable.of(10, 20, 30);
	var letters = Rx.Observable.of('a', 'b', 'c');
*/
/*Range
	var numbers = Rx.Observable.range(1, 10);
	numbers.subscribe(x => console.log(x));

*/
/*Sintaxis: timer(retrasoInicial, periodo, planificador)

	//Emite números ascendentes, uno por segundo (1000 ms), comenzando después de 3 segundos
	var numbers = Rx.Observable.timer(3000, 1000);
	numbers.subscribe(x => console.log("timer: "+x));
	//Emite un número después de cinco segundos
	var numbers = Rx.Observable.timer(5000);
	numbers.subscribe(x => console.log(x));
*/
/*Sintaxis: repeat(numero)
	var numbers = Rx.Observable.of('repito 3 veces').repeat(3);  //repite 3 veces la fuente of()
	numbers.subscribe(x => console.log(x));

*/

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
//En cada clic, emita un array de eventos de intervalo más recientes
//var clicks = Rx.Observable.fromEvent(document, 'click');
var interval = Rx.Observable.interval(1000);
var tiempo = Rx.Observable.timer(2000,3000);
var buffered = interval.buffer(tiempo);
buffered.subscribe(x => console.log(x));


//cada 3 segundo que muestre un array de los valores que genero el intervalo. La primera vez comienza con 2seg
var tiempo = Rx.Observable.timer(2000,3000);
var intervalo = Rx.Observable.interval(1000);
var buffered = intervalo.buffer(tiempo);
buffered.subscribe(x => console.log(x));

*/

/*
	//Emitir los últimos dos eventos de clic como un array
	var clicks = Rx.Observable.fromEvent(document, 'click');
	var buffered = clicks.bufferCount(2);
	buffered.subscribe(x => console.log(x));


	//Emitir los últimos dos valores que emitio intervalo
	var intervalo = Rx.Observable.interval(1000);
	var buffered = intervalo.bufferCount(2);
	buffered.subscribe(x => console.log(x));


	//En cada clic, emita los últimos dos eventos de clic como un array
	var clicks = Rx.Observable.fromEvent(document, 'click');
	var buffered = clicks.bufferCount(2, 1);
	buffered.subscribe(x => console.log(x));


	//En cada segundo, emita los últimos dos valores que emitio intervalo, y luego startBufferEvery comienza en la posicion 1 del buffer anterior
	var intervalo = Rx.Observable.interval(1000);
	var buffered = intervalo.bufferCount(2, 1);
	buffered.subscribe(x => console.log(x));

*/



/*

//Cada segundo, emita una matriz de los recientes eventos de clic
var clicks = Rx.Observable.fromEvent(document, 'click');
var buffered = clicks.bufferTime(1000);
buffered.subscribe(x => console.log(x));

//Cada 5 segundos, emita los eventos de clic de los próximos 2 segundos
var clicks = Rx.Observable.fromEvent(document, 'click');
var buffered = clicks.bufferTime(2000, 5000);
buffered.subscribe(x => console.log(x));
*/



















/* 
http://reactivex.io/rxjs/manual/overview.html#creation-operators

//otros flujos de datos con los que se puede crear nuevos observables


  --finitos: como el caso, de from que es un array, o las funciones personalizadas q usan, completed o error
  --infinitos: que no terminan nunca, que no tienen un completed o error
		var otroobservable = Rx.Observable.interval(400); //cada 0.4segundo va a enviar(next) un numero comenzando por 0..infinito

	var otroobservable = Rx.Observable.from([10, 20, 30]);  //va a enviar(next) cada valor del array 
	var otroobservable = Rx.Observable.range(31,43);  //crea un Observable que emite un rango de enteros secuenciales. desde 31-43

	var button = document.querySelector('button');
var otroobservable = Rx.Observable.fromEvent(button, 'click');
var subscription = otroobservable.subscribe(x => console.log(x));
var otroobservable = Rx.Observable.of('repito 3 veces').repeat(3);
*/

//var otroobservable = Rx.Observable.of('repito 3 veces').repeat(3);

/*
var context = { value: 42 };

var source = Rx.Observable.start(
    function () {
        return this.value;
    },
    context,
    Rx.Scheduler.timeout
);

var subscription = source.subscribe(
    function (x) {
        console.log('Next: ' + x);
    },
    function (err) {
        console.log('Error: ' + err);
    },
    function () {
        console.log('Completed');
    });
*/

//var otroobservable = Rx.Observable.repeat(42);
//var otroobservable =  Rx.Observable.of('repetir esto');
//const source = Rx.Observable.of('Repeat this!');
//repeat items emitted from source 3 times
//


/*

*/

/*
https://gist.github.com/btroncone/d6cf141d6f2c00dc6b35#repeat
https://github.com/ReactiveX/rxjs/blob/master/MIGRATION.md

//Tomar solo el 1er observable para emitir
const otroobservable = Rx.Observable.race( 
  Rx.Observable.interval(1500), //2do- emit every 1.5s
  Rx.Observable.interval(1000).mapTo('1s won!'), //1ro- emit every 1s
  Rx.Observable.interval(2000), //3ro- emit every 2s
  Rx.Observable.interval(2500)  //4to- emit every 2.5s
);

const otroobservable = Rx.Observable.race(
  Rx.Observable.interval(1500), //2do- emit every 1.5s
  Rx.Observable.interval(1000).mapTo('1s won!'), //1ro- emit every 1s
  Rx.Observable.interval(2000), //3ro- emit every 2s
  Rx.Observable.interval(2500)  //4to- emit every 2.5s
);


*/









})();	  				