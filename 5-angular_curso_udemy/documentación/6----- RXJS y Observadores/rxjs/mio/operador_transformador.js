(function() {


/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/*buffer(closingNotifier: Observable)
		//En cada clic, emita un array de eventos de intervalo más recientes
		//var clicks = Rx.Observable.fromEvent(document, 'click');
		var interval = Rx.Observable.interval(1000);
		var tiempo = Rx.Observable.timer(2000,3000);
		var buffered = interval.buffer(tiempo);
		buffered.subscribe(x => console.log(x));


		//cada 3 segundo que muestre un array de los valores que genero el intervalo. La primera vez comienza con 2seg
		var tiempo = Rx.Observable.timer(2000,3000);
		var intervalo = Rx.Observable.interval(1000);
		var buffered = intervalo.buffer(tiempo);
		buffered.subscribe(x => console.log(x));

*/

/*Sintaxis: bufferCount(bufferSize:number, startBufferEvery: number)
		//Emitir los últimos dos eventos de clic como un array
		var clicks = Rx.Observable.fromEvent(document, 'click');
		var buffered = clicks.bufferCount(2);
		buffered.subscribe(x => console.log(x));


		//Emitir los últimos dos valores que emitio intervalo
		var intervalo = Rx.Observable.interval(1000);
		var buffered = intervalo.bufferCount(2);
		buffered.subscribe(x => console.log(x));


		//En cada clic, emita los últimos dos eventos de clic como un array
		var clicks = Rx.Observable.fromEvent(document, 'click');
		var buffered = clicks.bufferCount(2, 1);
		buffered.subscribe(x => console.log(x));


		//En cada segundo, emita los últimos dos valores que emitio intervalo, y luego startBufferEvery comienza en la posicion 1 del buffer anterior
		var intervalo = Rx.Observable.interval(1000);
		var buffered = intervalo.bufferCount(2, 1);
		buffered.subscribe(x => console.log(x));

*/



/* bufferTime(bufferTimeSpan:number,bufferCreationInterval:number,maxBufferSize:number, scheduler:Scheduler)

		//Cada segundo, emita una matriz de los recientes eventos de clic
		var clicks = Rx.Observable.fromEvent(document, 'click');
		var buffered = clicks.bufferTime(1000);
		buffered.subscribe(x => console.log(x));

		//Cada 5 segundos, emita los eventos de clic de los próximos 2 segundos
		var clicks = Rx.Observable.fromEvent(document, 'click');
		var buffered = clicks.bufferTime(2000, 5000);
		buffered.subscribe(x => console.log(x));
*/








/*Sintaxis: bufferToggle(Suscriptor_o_Promesa, Funcion_queEmite_LoQueHayEN_elBuffer_y_LoBorra );
Suscriptor_o_Promesa: Un suscriptor o promesa de notificaciones para iniciar nuevos búferes. 
Funcion_queEmite_LoQueHayEN_elBuffer_y_LoBorra: Una función que toma el valor emitido por la openings observable y devuelve un Suscrito o Promesa, que, cuando emite, indica que el buffer asociado debe emitirse y borrarse.

		//El buffer va reuniendo valores cada 0.5 segundo desde la fuente intervalo
		//Pero este buffer se va a iniciar cada vez que haga un click, y cada 1 segundo emitira todos los valores que se puedieron recopilar en el buffer

		var intervalo = Rx.Observable.interval(500);
		var clicks = Rx.Observable.fromEvent(document, 'click');
		var buffered = intervalo.bufferToggle(clicks, i => Rx.Observable.interval(1000)  );
		buffered.subscribe(x => console.log(x));



		//Cada 1 segundos, emita los eventos de clic de los próximos 500 ms
			var clicks = Rx.Observable.fromEvent(document, 'click');
			var openings = Rx.Observable.interval(1000);
			var buffered = clicks.bufferToggle(openings, i =>  i % 2 ? Rx.Observable.interval(500) : Rx.Observable.empty() );
			buffered.subscribe(x => console.log(x));


*/


/*bufferWhen(closingSelector:function():Observable)
	//Emite una matriz de los últimos clics cada [2] segundos 

	var clicks = Rx.Observable.fromEvent(document, 'click');
	var buffered = clicks.bufferWhen( () =>  Rx.Observable.interval(2000));
	buffered.subscribe(x => console.log(x));

*/

/*concatMap(project: function(value: T, ?index: number): ObservableInput, resultSelector: function(outerValue: T, innerValue: I, outerIndex: number, innerIndex: number): any)
project:  Una función que, cuando se aplica a un elemento emitido por la fuente Observable, devuelve un Observable.
resultSelector: Una función para producir el valor en la salida Observable basado en los valores y los índices de la emisión de fuente (externa) y la emisión observable interna. 
							Los argumentos pasados ​​a esta función son:
								outerValue: el valor que vino de la fuente
								innerValue: el valor que proviene del Observable proyectado
								outerIndex: el "índice" del valor que proviene de la fuente
								innerIndex: el "índice" del valor del Observable proyectado



	//Para cada evento de clic, marque cada segundo de 0 a 3, sin concurrencia
	var clicks = Rx.Observable.fromEvent(document, 'click');
	var result = clicks.concatMap(ev => Rx.Observable.interval(1000).take(4));
	result.subscribe(x => console.log(x));

*/


/*concatMapTo(innerObservable: ObservableInput, resultSelector: function(outerValue: T, innerValue: I, outerIndex: number, innerIndex: number): any)

		//Para cada evento de clic, marque cada segundo de 0 a 3, sin concurrencia
		var clicks = Rx.Observable.fromEvent(document, 'click');
		var result = clicks.concatMapTo(Rx.Observable.interval(1000).take(4));
		result.subscribe(x => console.log(x));




*/

/*sintaxis: map(project: function(value:T, index:number):R, thisArg:any)

	//Asigne cada clic a la posición de clienteX de ese clic
	//transforma el objeto en uno de sus campos
	var clicks = Rx.Observable.fromEvent(document, 'click');
	var positions = clicks.map(ev => ev.clientX);
	positions.subscribe(x => console.log(x));

*/

/* sintaxis: mapTo(value: any)
	//Asigna cada clic a la cadena 'Hola'

	var clicks = Rx.Observable.fromEvent(document, 'click');
	var greetings = clicks.mapTo('hola');
	greetings.subscribe(x => console.log(x));

*/

/* flatMap
map(): devuelve el mismo número de elementos que el Stream de entrada ya que es simplemente una proyección de los elementos de entrada.
	  Es decir cada elemento de entrada se transforma en un elemento de salida.
flatMap(): proyecta una lista de elementos de cada elemento original y los concatena en un único stream.

		//	Rx.Observable.of(1,2,3)
		Rx.Observable.of(
					   {id: 8, name: 'misdel'},
					   {id: 10, name: 'duvi'},
	                   {id: 8, name: 'alex'},
	                   {id: 3, name: 'fidel'},	
	                   {id: 10, name: 'melani'},
	                   {id: 4, name: 'marx'}
                 )

		.flatMap(
		    function (valor, indice) { return [valor,indice]; },  //console.log(x); 
		    function (arg1, arg1_y_indice, arg2, nose) {   return arg1_y_indice ; }  //console.log(iy);
		 )
	  .subscribe( (arg1) => console.log(arg1));
*/



/*sintaxis: scan(accumulator: function(acumulador, valor, indice), valor_inicial_acumulador )
		//Cuenta el número de eventos de clic
		var clicks = Rx.Observable.fromEvent(document, 'click');
		var valor = clicks.mapTo(1); //asigna 1 a todos los click que de
		var valor_inicial_acumulador = 20;
		var count = valor.scan( (suma, numero, i) => i, valor_inicial_acumulador);
		count.subscribe(x => console.log(x));
*/

/**sintaxis: reduce(accumulator: function(acumulador, valor, indice), valor_inicial_acumulador )
		//Cuente el número de eventos de clic que sucedieron en 5 segundos
		var clicks = Rx.Observable.fromEvent(document, 'click').takeUntil(Rx.Observable.interval(5000));
		var valor = clicks.mapTo(1); //asigna 1 a todos los click que de
		var valor_inicial_acumulador = 20;
		var count = valor.reduce( (suma, numero, i) => i, valor_inicial_acumulador);
		count.subscribe(x => console.log(x));
*/


//Datos de pivote en el campo de id.

/* sintaxis: groupBy(keySelector: function(value: T): K, elementSelector: function(value: T): R, durationSelector: function(grouped: GroupedObservable<K, R>): Observable<any>)
	keySelector: Función que extrae la clave para cada elemento.
	elementSelector: Función que extrae el elemento de retorno para cada elemento.
	durationSelector: Función que retorna un Observable para determinar cuánto tiempo debe existir cada grupo.

*/



/*
	Rx.Observable.of(	   {id: 8, name: 'misdel'},
					   {id: 10, name: 'duvi'},
	                   {id: 8, name: 'alex'},
	                   {id: 3, name: 'fidel'},	
	                   {id: 10, name: 'melani'},
	                   {id: 4, name: 'marx'}
                 )
    .groupBy(p => p.id, p => p.name)
    .flatMap( (grupo,indice) =>  grupo.reduce( (total, valor,i) => [...total,  valor], ["" + grupo.key] )  )
    .map(arr => ({  'id': parseInt(arr[0]), 'nombre': arr.slice(1)   })    )
    .subscribe(p => console.log(p));

*/


/*

//Combina dos observables: intervalo de 1 s y clics
*/
var clicks = Rx.Observable.fromEvent(document, 'click');
var timer = Rx.Observable.interval(1000);
var clicksOrTimer = Rx.Observable.merge(clicks, timer);
clicksOrTimer.subscribe(x => console.log(x));

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




/*

					var suscribirme = miObservable.subscribe({
					  next : x => console.log('x vale', x),
					  error: err => console.log('algo salio mal', err),
					  complete: () => console.log('se completo'),
					});


					//Los tres tipos de devoluciones de llamada pueden proporcionarse como argumentos:
					var suscribirme2 = miObservable.subscribe(
					     x => console.log('x vale', x),  //Como ocurre con la expresión yield, se pueden lanzar n next, los cuales devuelven un valor.
					   err => console.log('algo salio mal', err), // Se lanza cuando sucede un error.
					    () => console.log('se completo'),  //No devuelve nada y se lanza cuando ha terminado la secuencia de next().
					 );
					 

					//Los tres tipos de devoluciones de llamada pueden proporcionarse como funciones:
					var suscribirme3 = miObservable.subscribe(
		  				  function (x) {   //o   function miFuncion(x) { 
						    console.log('Next: %s', x);
						  },
						  function (err) {
						    console.log('Error: %s', err);
						  },
						  function () {
						    console.log('Completed');
						  }
					 );

*/






})();	  				