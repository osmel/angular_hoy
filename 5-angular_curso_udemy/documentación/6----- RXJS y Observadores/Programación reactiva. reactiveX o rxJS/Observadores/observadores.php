
<!DOCTYPE html>
<!-- saved from url=(0055)http://gugggly.com/templates/doc-web/index.html#welcome -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style type="text/css">
    
    .code{
      font-family: Liberation Mono;
      font-size: 14px;
    }

</style>
</head>
	<body>
	osmel


	</body>
</html>


<script type="text/javascript">


 var contador=0;
 var contador_suscripciones=0;

 var sujeto = function(){
 	 this.observadores =[];
 	 this.datos =[];
 	 this.prueba ="aaa";

 	 this.suscribir = function(suscriptor) {

 	 	/*Para poner a la escucha a nuestro observador lo que tenemos que hacer es
 	 	que cuando este se suscriba, vamos a crearle un por donde el va a escuchar
 	 	el cual va a recibir por parametro el objeto que el quiere escuchar
 	 	y los va a guardar en 
 	 	*/
	
	 	 suscriptor.escuchar = function(datos) {
	 	 	 suscriptor.datosSuscripcion = datos;
	 	 }

	 	 
	 	 suscriptor.agregaDatos = function(nuevoDato,sujeto) {  //que el suscriptor agregue datos
	 	 	 //var mithis = this;
	 	 	 //console.log("th ---->"+ sujeto.prueba);
	 	 	 //parent().datos.push(3);	 
	 	 	 //suscriptor.datosSuscripcion = datos;
	 	 	 //this.informar(this.datos);
	
	 	 	 sujeto.datos.push(nuevoDato);
	 	 	 //UNa vez que un observador cambie  datos tenemos que informar a los otros suscriptores
	 	 	 sujeto.informar(sujeto.datos);


	 	 }
		

 	 	this.observadores.push(suscriptor); //los observadores son los que se suscriben
 	 	this.observadores[contador_suscripciones].escuchar(this.datos);  
 	 	contador_suscripciones++;
 	 }

 	 this.agregarDatos = function(nuevoDato) {
 	 	 this.datos.push(nuevoDato);
 	 	 //UNa vez que un observador cambie  datos tenemos que informar a los otros suscriptores
 	 	 this.informar(this.datos);
 	 	
 	 }

 	 this.cambiarDatos = function(nuevoDato) {
 	 	 this.datos.push(nuevoDato);
 	 	 //UNa vez que un observador cambie  datos tenemos que informar a los otros suscriptores
 	 	 this.informar(this.datos);
 	 	
 	 }
 	 
 	 this.obtenerDatos = function() {
 	 	 this.datos.push('registro número'+contador);
 	 	 contador++;
 	 	 //UNa vez que un observador cambie  datos tenemos que informar a los otros suscriptores
 	 	 this.informar(this.datos);
 	 	
 	 }
 	 
 	 //este método informara a todos los observadores a traves de su método escuchar(pasandole los datos que han cambiado)
 	 this.informar = function(datos) { //tenemos que decirle a todos los que se han suscrito que ha cambiado y que cosa a cambiado
 	 	for (var i = 0; i < this.observadores.length; i++) {
 	 		//informaremos a cada suscriptor u observador
 	 		//por cada observador, llamamos a su método escuchar y le pasamos por parametros los datos
 	 		this.observadores[i].escuchar(datos); 
 	 	}
 	 	
 	 }
 }

 
//Nos creamos un "objeto sujeto" apartir de la clase creada
 var suj = new sujeto();
 
//Nos creamos un objeto suscriptor (como un objeto javascript{} )
 var suscriptor1 = {'nombre': 'suscriptor '+contador};
 //Lo Suscribimos en el sujeto( Es decir le hacemos saber al sujeto que hay un nuevo suscriptor)
 suj.suscribir(suscriptor1); //suscriptor1: ya con este esta suscripto en el sujeto
 
 //Ver los datos que he guardado en  datosSuscripcion 
 console.log(suscriptor1.datosSuscripcion);   //en este momento esta vacio
 
 //agregamos datos al suscriptor
 suj.agregarDatos({'suj:':10});
  suj.agregarDatos({'suj:':13});

 var suscriptor2 = {'nombre': 'osmel '+contador};
 suj.suscribir(suscriptor2);
 
 suscriptor2.agregaDatos({'2:':22}, suj); 
 suscriptor1.agregaDatos({'1:':11}, suj); 

 console.log(JSON.stringify(suscriptor2.datosSuscripcion));
 console.log(JSON.stringify(suscriptor1.datosSuscripcion));

var suscriptor3 = {'nombre': 'osmel '+contador};
suj.suscribir(suscriptor3);
console.log(JSON.stringify(suscriptor3.datosSuscripcion)); 
 


</script>
