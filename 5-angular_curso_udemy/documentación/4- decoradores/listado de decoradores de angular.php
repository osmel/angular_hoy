Los decoradores son funciones que modifican las clases JavaScript. Angular tiene muchos decoradores que adjuntan metadatos a las clases

--módulo es una clase con el decorador @NgModule

			src/app/app.module.ts:

						import { NgModule } from '@angular/core';   //todas las librerias de angular comienzan con @angular
																	//en el core estan algunas funciones entre ella
																   // el decorador de modulo "NgModule"

						import { BrowserModule } from '@angular/platform-browser';  //importa el modulo BrowserModule
						
						@NgModule({  //decorador usado para los modulos, y tiene un objeto metadato
						  
						  providers: [ Logger ], //servicios que se van a usar de forma global. 
						  						//Éstos serán accesibles en cualquier parte de la aplicación.
						  declarations: [ AppComponent ], //aqui se especifican todas las clases view que pertenen a este modulo
						  								  //clases view: (componentes, directivas, pipes)
						  
						  imports: [ BrowserModule ], //especificamos otros modulos, para poderlos usar en toda la aplicación
						  exports: [ AppComponent ],  //especificamos lo que se quiera exportar para que se vean desde otros modulos
						  
						  bootstrap: [ AppComponent ]  //vista principal de la aplicación, llamado componente raíz, 
						  							  //solo el modulo que va hacer raiz debe establecer esta propiedad

						})
						export class AppModule { }  //esta es la clase


--Componente es una clase con el decorador @Component (que en su interior tiene un objeto metadato)
		@Component({   //decorador
		  selector: 'hero-list',                       //nombre como se reconocera el componente en cualquier plantilla
		  
		  templateUrl: './hero-list.component.html',   //url de la plantilla
		  template   : 	'<h1>hola<h1/>'                //plantilla directamente

		  providers: [ HeroService ]					//proveedores de inyección de dependencia para los servicios 
		  												//que el componente requiera.
		  												//En este ejemplo, le dice a angular que el constructor requiere del "servicio HeroService" para poder obtener de este....
		})
		export class HeroListComponent implements OnInit {  //esta es la clase
		  /* . . . */
		}






--Directiva: es una clase con un decorador @Directive. 
				Las directivas estructurales: alteran el diseño añadiendo, eliminando y reemplazando elementos en el DOM.
	
						directivas estructurales:
							<li *ngFor="let hero of heroes"></li>    -->*ngFor le dice a Angular que imprima un elemento <li> por cada héroe en la lista de heroes.
							<hero-detail *ngIf="selectedHero"></hero-detail>  -->*ngIf incluye el componente HeroDetail sólo si existe un héroe seleccionado.

							otra: ngSwitch

					    Directivas de atributo: modifican la apariencia, aspecto o el comportamiento de un elemento existente. 
								En las plantillas se ven como atributos HTML normales, de ahí el nombre.

							directivas de atributos:	 modifica el comportamiento de un elemento
					          <input [(ngModel)]="hero.name">  --> ngModel modifica el comportamiento de un elemento existente (normalmente un <input>) estableciendo su propiedad de valor de visualización y respondiendo a eventos.

					          otra: ngStyle y ngClass



-- Los sercivios: es una clase con un decorador @Injectable, 

		import { Injectable } from '@angular/core'; 

		@Injectable()
		export class ClienteServicio{
				getClientes(): string[]{
				   return ['alex','duvi','fide'];
				}
		}



		Inyeccion de dependencia: Es donde se suministran objetos a una clase, en lugar de la clase dentro crearse objetos.
		*En el parametro del constructor de una clase, pasamos un objeto  ->  claseA( new ClaseB )

		  @Component({  
		  		selector: 'hero-list',                       //nombre como se reconocera el componente en cualquier plantilla
		  		template   : 	'<h1>hola<h1/>'                //plantilla directamente
				providers: [ ClienteServicio ]	
		  })
		  export class Prueba {  //esta es la clase
		  
		  }


           






-- @Input 
-- @Output 		