import { Injectable } from '@angular/core';
//import { HttpModule } from '@angular/http';



import 'rxjs/add/operator/map';
import { URL_SERVICIOS } from "../../config/url.servicios";

import { Http } from '@angular/http';


@Injectable()
export class ProductosProvider {

  pagina:number = 0;
  productos:any[] = [];
  lineas:any[] = [];
  por_categoria:any[] = [];
  resultados:any[] = [];

  constructor(public http: Http) {
    this.cargar_todos();
    //this.cargar_lineas();

  }

  cargar_todos(){
	    let promesa = new Promise(  (resolve, reject)=>{

	      let url = URL_SERVICIOS + "/todos/" + this.pagina;  ///productos


	      this.http.get( url )
	                .map( resp => resp.json() )
	                .subscribe( data =>{
	                		console.log(data);

	                  if( data.error ){
	                    // Aqui hay un problema
	                  }else{

	                    //let nuevaData = this.agrupar( data.productos, 2 );

	                    //this.productos.push( ...nuevaData );
	                    this.productos.push( ...data.productos );
	                    this.pagina +=1;

	                  }

	                  //resolve();

	                })

	    });
	    console.log(this.productos);
	    return promesa;
  }  



}
