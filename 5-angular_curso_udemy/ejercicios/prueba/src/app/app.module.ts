import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';  

import { LOCALE_ID } from '@angular/core';  // idioma
import localeEs from '@angular/common/locales/es'; // idioma
import { registerLocaleData } from '@angular/common';// idioma
registerLocaleData(localeEs);// idioma


//servicios
import { HeroesService } from './servicios/heroes.service';  


//rutas
import { APP_ROUTING } from './app.routes';  //rutas  "APP_ROUTING" es el nombre que exporta el fichero './app.routes'

//componentes
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroeComponent } from './heroe/heroe.component';
import { TuberiasComponent } from './tuberias/tuberias.component';
import { CapitalizadoPipe } from './pipes/capitalizado.pipe';
import { DomseguroPipe } from './pipes/domseguro.pipe';





@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    NavbarComponent,
    HeroesComponent,
    HeroeComponent,
    TuberiasComponent,
    CapitalizadoPipe,
    DomseguroPipe,
    
    
  ],
  imports: [
    BrowserModule,
    APP_ROUTING

  ],
  providers: [
    HeroesService,
    { provide: LOCALE_ID, useValue: "es" }   // idioma
  ],


 
  bootstrap: [AppComponent]
})
export class AppModule { }
