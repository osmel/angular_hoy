import { Pipe, PipeTransform } from '@angular/core';

//paquete para verificar que es seguro, 
import { DomSanitizer } from '@angular/platform-browser';


@Pipe({
  name: 'domseguro'
})
export class DomseguroPipe implements PipeTransform {

  constructor( private domSaneador:DomSanitizer ){

  }	
  transform(value: string, url: string): any {  //esto regresara un domSanitizerrison
  	
    return this.domSaneador.bypassSecurityTrustResourceUrl(url + value);
  }

}
