import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizado'
})
export class CapitalizadoPipe implements PipeTransform {

  //transform(value: any, args?: any): any {
  	//args: any,args2: any
  	//...args:arg[]
  	//espera el valor y los argumentos
  	//cap 75
  transform(value: string, ...args:any[]): string {	 
  	value = value.toLowerCase();  //convierto primero todo en minuscula
  	let nombre = value.split(" ");  //pico el nombre por su espacio
  	for(let i  in nombre) {  //analizo cada nombre
  		//nombre[i][0].toUpperCase() : Mayuscula a la primera letra
  		//nombre[i].substr(1) : apartir de la 2da letra en adelante
       nombre[i] = nombre[i][0].toUpperCase()+nombre[i].substr(1);
  	}
    return nombre.join(" "); //concateno todo el array pero por medio de un espacio
  }

}
