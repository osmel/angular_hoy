import { Component } from '@angular/core';
import { CapitalizadoPipe } from './../pipes/capitalizado.pipe';

@Component({
  selector: 'app-tuberias',
  templateUrl: './tuberias.component.html',
  styleUrls: ['./tuberias.component.css']
})
export class TuberiasComponent {

nombre:string = "Fernando";
  arreglo = [1,2,3,4,5,6,7,8,9,10];
  PI = Math.PI;
  a=0.234;
  salario=1234.5;

  objeto:Object  = {
      foo: "bar",
      baz: "qux",
      edad: 500,
      nested: {
        xyz: "3",
        numbers: "[1, 2, 3, 4, 5]"
      }
  };

  valorDepromesa = new Promise( (resolve,reject) => {
    setTimeout(()=>resolve('llego la data'),2000);
   } );

  fecha = new Date();

  nombre2:string = "osMel caldeRón Bernal";

  video = 'j30Jytow3dE'; //de esta forma a veces falla, porq exige que primero se verifique, por tanto hay q sanearlo primeramente


  constructor() {
   
 /*
  
  
  */

}

  
}
