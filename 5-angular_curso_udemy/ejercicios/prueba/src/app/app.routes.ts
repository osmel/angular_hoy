import { RouterModule, Routes } from '@angular/router';  //Aqui estamos definiendo las rutas "app.routes.ts"
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {HeroesComponent} from './heroes/heroes.component';
import { HeroeComponent } from './heroe/heroe.component';
import { TuberiasComponent } from './tuberias/tuberias.component';


const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent }, //cada ruta tiene un path y un componente
  { path: 'about', component: AboutComponent }, 
   { path: 'heroes', component: HeroesComponent },
   { path: 'tuberias', component: TuberiasComponent },
   {path:'heroe/:id',component:HeroeComponent, data:{nombre:'osmel'} },

  { path: '**', pathMatch: 'full', redirectTo: 'home' }  //esta es la ruta por defecto, que tomara sino coincide ninguna
];
export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);

		//si usas este sin hash, asegurarse de que en index.html aparezca esto   <base href="/">
		//export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
		//useHash:true ->para el uso de parametros
		//export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES,{useHash:true});
