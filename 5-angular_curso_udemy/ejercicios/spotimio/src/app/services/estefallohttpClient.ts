import { Injectable } from '@angular/core';

//import { Http, Headers} from '@angular/http';

//import { Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';


import 'rxjs/add/operator/map';  //para solo importar el .map

@Injectable()
export class SpotifyService {

	  artistas:any[] =[];	
	  urlBusqueda:string ="https://api.spotify.com/v1/search";
	  urlArtista:string ="https://api.spotify.com/v1/artists";

	  //constructor(private objHttp:Http) { }
	    constructor(private objHttp:HttpClient) { }

	  getArtistas(termino:String ){
	  		//let query= "q=silvio+rodriguez&type=artist";
	  		
	  		let headers = new HttpHeaders();    //headers es una propiedad por eso no le puedo cambiar el nombre
	  		headers.append('Authorization', 'Bearer BQCpY2kpQwHYc7cBi6pwwou_SOm9rjlw8qd4b2kLMB61xrN7F8mplmA1MQyXf5IvET5XllFJ8YY5zy9tFqo');

	  		let query= `?q=${ termino }&type=artists`;
	  		let url = "https://api.spotify.com/v1/search?query=metallica&type=artist"; //this.urlBusqueda + query;

	  		/*hacer una peticion al url 
					
					 asyncronico
				la data no va a regresar de manera inmediata, va a ser la peticion a spotify y quizas demore unas fracciones de segundo
				por tanto debemos regresar algo así como una promesa(pero en este caso vamos a regresar un observable" que es algo similar a la promesa pero con ciertas ventajas" ), 

	  		*/


		  		//return this.objHttp.get( url )  //esto va a regresar un observable, pero nosotros podemos convertir esa respuesta en un objeto,
				 								// usando .map( que viene de rxjs)
				return this.objHttp.get( url,  {headers:headers} ) //, {}
				.map(res=>{ //
					//console.log(res);				
					//console.log(res.json() );				
					//console.log(res.json().artists.items );				
					//this.artistas = res.json().artists.items;
					//this.artistas = res;
					//return res.json().artists.items;
					console.log(res);
				})
	  }

	  getArtista(id:String){
	  }


}
