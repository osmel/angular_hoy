import { Injectable } from '@angular/core';

//import { Http, Headers} from '@angular/http';  este uso era el viejo

import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map';  //para solo importar el .map

@Injectable()
export class SpotifyService {

	  termino:String="";	
	  artistas:any[] =[];	
	  urlBusqueda:string ="https://api.spotify.com/v1/search";
	  urlArtista:string ="https://api.spotify.com/v1/artists";

	  constructor(private objHttp:HttpClient) { }

	  getArtistas(termino:String ){
	  		//let query= "q=silvio+rodriguez&type=artist";
	  		
	  		//let encabezado = new Headers({}); 
	  		//encabezado.append('Authorization', 'Bearer BQCpY2kpQwHYc7cBi6pwwou_SOm9rjlw8qd4b2kLMB61xrN7F8mplmA1MQyXf5IvET5XllFJ8YY5zy9tFqo');

	  		let encabezado = new HttpHeaders({'Authorization': 'Bearer BQDvRxnnBfAj2w8o9xmg2OTIAqlulUxu4S8GY9u-mMmJCFe9vYLYdgyVX5Z4aV-H92zgGRVHg3Enw-QlHws'});    
	  		
	  		let query= `?q=${ termino }&type=artists`;
	  		let url = "https://api.spotify.com/v1/search?query=metallica&type=artist"; //this.urlBusqueda + query;

	  		/*hacer una peticion al url 
					
					 asyncronico
				la data no va a regresar de manera inmediata, va a ser la peticion a spotify y quizas demore unas fracciones de segundo
				por tanto debemos regresar algo así como una promesa(pero en este caso vamos a regresar un observable" que es algo similar a la promesa pero con ciertas ventajas" ), 

	  		*/


		  		//return this.objHttp.get( url )  //esto va a regresar un observable, pero nosotros podemos convertir esa respuesta en un objeto,
				 								// usando .map( que viene de rxjs)
				return this.objHttp.get( url, {headers:encabezado} ) //, {}
				  .map((res:any)=>{ //
					//console.log(res.artists.items);	
					this.artistas = res.artists.items;		
					return	res.artists.items;
					//console.log(res.json() );				
					//console.log(res.json().artists.items );				
					//this.artistas = res.json().artists.items;

					//return res.json().artists.items;
				})
	  }

	  getArtista(id:String){
	  }


}
