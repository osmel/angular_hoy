import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';   //https://angular.io/api/common/http/HttpClientModule
//import { HttpModule } from '@angular/http';  //este era el viejo  
//rutas
import { APP_ROUTING } from './app.routes';
//servicios
import { SpotifyService } from './services/spotify.service';


import { AppComponent } from './app.component'; 
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';






@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, 
    //HttpModule,  //este era el viejo
    HttpClientModule, //este me va a permitir usar httpCliente en futuro
    APP_ROUTING
  ],
  providers: [SpotifyService],  //si yo hago uso de este servicio en toda mi aplicacion va a ser el mismo,
                                  //pero si lo defino en un componente será otra instancia que trabajará en ese componente
                                 // en un componente como metadato:   providers: [SpotifyService],
                                 //cuando esta declarado aqui solo se va a disparar una sola vez, en un constructor q se inyecte

  bootstrap: [AppComponent]
})
export class AppModule { }
