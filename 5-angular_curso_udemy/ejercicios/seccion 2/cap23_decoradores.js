/*
Los decoradores son una caracteristica que esta todavia en experimento
los decoradores no son más q una funcion que reflejan al constructor
*/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
function miConsola(constructor) {
    console.log(constructor);
}
var Animal = /** @class */ (function () {
    function Animal(nombre) {
        this.nombre = nombre;
    }
    Animal = __decorate([
        miConsola //aqui es donde asocia el decorador a la clase
    ], Animal);
    return Animal;
}());
/*

como resultado devolverá:

        function Animal(nombre) {
           this.nombre = nombre;
        }


para q no salga un warning tendrias q ir al
    compilar con:
    tsc --experimentalDecorators

  o ir al "tsconfig.json": y agregar esa caracteristica
    "experimentalDecorators": true,

*/
