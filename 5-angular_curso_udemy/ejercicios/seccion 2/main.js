// import module "fs"
const fs = require("fs");


// export function readFile
module.exports.readFile = function(f) {
    return fs.readFileSync(f);
}