var Personas = /** @class */ (function () {
    function Personas(nombre, edad, sexo) {
        if (sexo === void 0) { sexo = true; }
        this.nombre = "osmel";
        this.edad = 30;
        this.sexo = true;
        /*this: siempre va a ser referencia a la clase*/
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
    }
    return Personas;
}());
var miObjeto = new Personas("duvi alex fidel", 40, true);
console.log(miObjeto);
/*
export class Xmen {
    nombre:string ="osmel";
    edad:number =30;
    
    constructor (nombre:string, edad:number) {
      this.nombre= nombre;
      this.edad= edad;
    }
    imprimir() {
      console.log (` ${this.nombre} - ${this.edad} `);
    };
}

*/
