webpackJsonp([0],{

/***/ 434:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(435);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = (function () {
    function LoginPageModule() {
    }
    return LoginPageModule;
}());
LoginPageModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
        ],
    })
], LoginPageModule);

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_home_home__ = __webpack_require__(152);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//slides
//podemos usar todos los elementos del ciclo de vida de un componente de angular. Ejemplo AfterViewInit




var LoginPage = (function () {
    function LoginPage(navCtrl, provUsuario, loadingCtrl, alertCtrl) {
        this.navCtrl = navCtrl;
        this.provUsuario = provUsuario;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.clave = "osmel-1";
    }
    LoginPage.prototype.ngAfterViewInit = function () {
        this.slides.lockSwipes(true); //bloquear 
        this.slides.freeMode = false; //que no este libre
        this.slides.paginationType = "progress"; //paginacion de tipo barra de progreso  
    };
    LoginPage.prototype.continuar = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Espere por favor..."
        });
        loading.present(); //comienza el loading
        // Verificar si la clave es valida
        this.provUsuario.verifica_usuario(this.clave)
            .then(function (valido) {
            loading.dismiss(); //stop loading
            if (valido) {
                _this.slides.lockSwipes(false); //desbloquear la pantalla
                _this.slides.slideNext(); //deslizamos el slide a la sig pantalla
                _this.slides.lockSwipes(true); //y bloqueamos nuevamente para que no se muevaS 
            }
            else {
                _this.alertCtrl.create({
                    title: "Clave no es correcta",
                    subTitle: "Por favor verifique su clave, o hable con el adminsitrador",
                    buttons: ["Ok!"]
                }).present();
            }
        })
            .catch(function (error) {
            loading.dismiss();
            console.log("ERROR en verifica_usuario: " + JSON.stringify(error));
        });
    };
    LoginPage.prototype.ingresar = function () {
        // tenemos la clave, ir al home
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_home_home__["a" /* HomePage */]);
    };
    return LoginPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Slides */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Slides */])
], LoginPage.prototype, "slides", void 0);
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'page-login',template:/*ion-inline-start:"/var/www/html/seccion10/src/pages/login/login.html"*/'\n\n<ion-content padding>\n\n  <ion-slides pager>\n\n      <ion-slide>\n\n        <ion-toolbar>\n        </ion-toolbar>\n\n        <img src="assets/img/usuario.jpg" class="slide-image"/>\n        <h2 class="slide-title">Ingrese su clave</h2>\n        <p>Para continuar, debe de ingresar su código</p>\n\n        <ion-list>\n          <ion-item>\n            <ion-label>Clave</ion-label>\n            <ion-input type="text" [(ngModel)]="clave"\n                      (keyup.enter)="continuar()"></ion-input>\n          </ion-item>\n        </ion-list>\n\n        <button ion-button\n                [disabled]="clave.length < 3"\n                (click)="continuar()">\n          Verificar\n        </button>\n\n      </ion-slide>\n\n\n      <!-- Ultimo Slide -->\n      <ion-slide>\n        <ion-toolbar>\n        </ion-toolbar>\n\n        <img src="assets/img/check.jpg" class="slide-image"/>\n        <h2 class="slide-title">¿Listo para empezar?</h2>\n\n        <button ion-button large clear icon-right color="primary"\n                (click)="ingresar()">\n          Continuar\n          <ion-icon name="arrow-forward"></ion-icon>\n        </button>\n\n      </ion-slide>\n      <!-- Fin del último slide -->\n\n    </ion-slides>\n\n\n</ion-content>\n'/*ion-inline-end:"/var/www/html/seccion10/src/pages/login/login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_usuario_usuario__["a" /* UsuarioProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ })

});
//# sourceMappingURL=0.js.map