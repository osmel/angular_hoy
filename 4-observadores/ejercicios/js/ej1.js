(function() {
  
//funcion nativa de javascript 
function foo(){ 
    console.log('Hello'); 
    return 42; 
} 

          //func.call(): privitiva significa “darme un valor sincrónicamente”
  var x = foo.call();  //muestra hello
  console.log(x);     //muestra 42
  var y = foo.call();  //muestra hello
  console.log(y); 		 //muestra 42

})();	