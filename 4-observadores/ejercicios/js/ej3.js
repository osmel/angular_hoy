(function() {
  
  /*
	Anatomía de un observable
			Los observables se crean utilizando:
				 Rx.Observable.create o un operador de creación, 
				 		 operadores de creación: "like of, from, interval, etc".
			Se suscriben con un 
				Observer, 
			se ejecutan para entregar 
				“next”/“error”/“complete” al Observer, 
			su ejecución puede eliminarse. 
				cancelacion

		Estos cuatro aspectos están codificados en una instancia Observable, 
		pero algunos de estos aspectos están relacionados con otros tipos, como Observer y Subscription.

		Core Concepts:
				Creación: Observables
				Subscripción: a Observables
				Ejecución:  Observables
				Cancelación: Observables

		

	CREACION
		Rx.Observable.create: es un alias para el constructor Observable, y toma un argumento: la función "suscribiendo".
					la "función de suscribiendo": es la pieza más importante para describir el Observable.
		

		var observable = Rx.Observable.create(function suscribiendo(observer) {  //ejemplo crea un Observable para emitir la cadena ‘hola’ cada segundo a un observador.
		  var id = setInterval(() => { 
		    observer.next('Hola') 
		  }, 1000); 
		});

  */
	
	//rxjs 
	//esta funcion es equivalente a la nativa de "ej1.js"
	// Los observables pueden entregar valores de forma síncrona o asíncrona.

	//Una llamada al subscribe es simplemente una forma de iniciar una “ejecución observable” y entregar valores o eventos a un observer de esa ejecución.
	var observable = Rx.Observable.create(function suscribiendoMIO(observer) {  //ejemplo crea un Observable para emitir la cadena ‘hola’ cada segundo a un observador.
	  //console.log('Hello'); 
	  var id = setInterval(() => { 
	    observer.next('Hola') 
	  }, 1000); 
	});

	    


	//2 llamadas de función desencadenan dos efectos secundarios independientes y 
	//2 suscripciones observables disparan dos efectos secundarios independientes. 	

	//Suscribirse a un Observable es como llamar a una función, proporcionando devoluciones de llamada donde se entregarán los datos.

	observable.subscribe(function(x){   //observable.subscribe():significa “darme cualquier cantidad de valores, de forma síncrona o asíncrona”
	    console.log(x);   //imprime 42
	}); 

	//equivale a la q esta encima
	observable.subscribe( x => console.log(x+1) );


})();	