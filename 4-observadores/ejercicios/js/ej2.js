(function() {
  
	//rxjs 
	//esta funcion es equivalente a la nativa de "ej1.js"
	// Los observables pueden entregar valores de forma síncrona o asíncrona.
	var fooRxjs = Rx.Observable.create(function (observer){ 
	    console.log('Hello'); 
	    observer.next(42);  //un push de forma sincronica 
	    observer.next(30);  //un push de forma sincronica 
	}); 


	//2 llamadas de función desencadenan dos efectos secundarios independientes y 
	//2 suscripciones observables disparan dos efectos secundarios independientes. 	
	fooRxjs.subscribe(function(x){   //observable.subscribe():significa “darme cualquier cantidad de valores, de forma síncrona o asíncrona”
	    console.log(x);   //imprime 42
	}); 
	fooRxjs.subscribe(function(y){ 
	    console.log(y);   //imprime 42
	});


})();	