(function() {
  
  /*
	Ejecución observable
		El código dentro de Observable.create(function subscribe (observer) {…}) 
		representa una “ejecución observable”, un cálculo lazy que sólo ocurre para cada observer que se suscribe.
		La ejecución produce múltiples valores a lo largo del tiempo, ya sea de forma síncrona o asíncrona.

		Hay tres tipos de valores que una ejecución observable puede ofrecer:
			“next”: envía un valor como un "número, una cadena, un objeto, etc".
			“error”: envía un error o excepción de JavaScript.
			“complete”: no envía un valor.


			*notificaciones "next" es el “tipo de valores” más importante y más común: 
					representan datos reales que se entregan a un observer. 
			
			*notificaciones de “error” y “complete”: pueden ocurrir sólo una vez durante la ejecución observable, 
														y sólo puede haber uno de ellos.

		                           Si se entrega una notificación de error o complete, no puede ser entregado después de esta nada más.

  */
	

	//Una llamada al subscribe es simplemente una forma de iniciar una “ejecución observable” y entregar valores o eventos a un observer de esa ejecución.
	var observable = Rx.Observable.create(function suscribiendoMIO(observer) {  //ejemplo crea un Observable para emitir la cadena ‘hola’ cada segundo a un observador.

		  try { 
		    observer.next(1); 
		    observer.next(2); 
		    observer.next(3); 
		    observer.complete(); 
		    observer.next(4); // No se entrega porque violaría el contrato. Ya tuvo complete
		  } catch (err) { 
		    observer.error(err); // Entrega una notificación de “error” si sucede uno
		  } 


	});

	    


	//2 llamadas de función desencadenan dos efectos secundarios independientes y 
	//2 suscripciones observables disparan dos efectos secundarios independientes. 	

	//Suscribirse a un Observable es como llamar a una función, proporcionando devoluciones de llamada donde se entregarán los datos.

	observable.subscribe(function(x){   //observable.subscribe():significa “darme cualquier cantidad de valores, de forma síncrona o asíncrona”
		//try/catch que entregará una notificación de error si detecta una excepción:
	    console.log(x);   //imprime 42
	}); 

	//equivale a la q esta encima
	//observable.subscribe( x => console.log(x+1) );


})();	
