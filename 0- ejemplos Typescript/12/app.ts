/*
	En este caso el decorador @Bienvenida decora a la clase Saludar con un método saludo target.prototype.saludo 
	que como podemos apreciar en el constructor de la clase no lo contiene.
	Los decoradores de clase nos permiten aumentar contenido de la propia clase.
	Destaquemos que las instrucciones del decorador son ejecutadas antes de que la clase sea instanciada.

	-Si el decorador de clase devuelve un valor, reemplazará la declaración de clase con la 
		función de constructor proporcionada.



*/


function Nativo(target: Function): void {  // este es el decorador 
  
	  target.prototype.nativo1=function(): void { // método
	    console.log('nativo1');
	  }

	  target.prototype.nativo2=function(): string { //método
	    return "nativo2";
	  }

}

/*


Fábrica de decoradores(Decorador Factories)
   Si queremos personalizar cómo se aplica un decorador a una declaración,
   podemos escribir una "fábrica decoradora". (No es más que una función que envuelve a un decorador)
   Una Decorator Factory es simplemente una función que devuelve la expresión que el decorador 
   llamará en el tiempo de ejecución.
*/

function Bienvenida(msg: String) {  // esta es una fabrica de decoradores
	return function(target:Function) {  // este es el decorador 
			  target.prototype.saludo=function(): void {  //metodo que tiene el decorador
			    console.log(msg);
			  }
	  }

}


@Bienvenida('que tal!')
@Nativo
class Saludar {
  constructor(){
	//console.log('constructor!');  	
    // Lo que corresponda.
  }


}

var miSaludo = new Saludar();
miSaludo.nativo1();
miSaludo.saludo();
console.log( miSaludo.nativo2() ) ;