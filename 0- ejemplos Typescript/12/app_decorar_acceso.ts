
class Point {
  private _x: number;
  private _y: number;
  constructor(x: number, y: number) {
    this._x = x;
    this._y = y;
  }

  @configurable(false)
  get x() { return this._x; }

  @configurable(false)
  get y() { return this._y; }
}

function configurable(value: boolean) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    //descriptor.configurable = value;
    console.log(descriptor);
  };
}

function hola(target:Object, key:string, descriptor:any) {
    console.log('objeto', target); //objeto
    console.log('objeto nombre', target.constructor.name); //objeto
    
    console.log('llave', key); //propiedad o *metodo
    console.log('descriptor', descriptor);
    
    console.log('kike');
}  


class Person {
  constructor () {
  }

  @hola	
  saludo(){
    return `hola que tal`
  }
  
  
}

const p = new Person()
console.log(p.saludo()) // Hi! I'm Edgar Bermejo . Nice to meet you!
