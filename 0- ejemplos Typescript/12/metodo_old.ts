/*
	https://www.youtube.com/watch?v=lpVr12sUBLg&list=PLEtcGQaT56ch63VgSUCOle6QNBqK8ewvd&index=16

	La simplicidad de la sintaxis de los decoradores contrasta con su funcionamiento. 
	Un decorador es una función que intercepta la definición de propiedades de un objeto. 
	Por lo tanto, permite modificar tal definición en tiempo de ejecución. 
	Lo hace llamando a una función que recibe los mismos parámetros que Object.defineProperty(). 
	Así, el decorador devuelve un objeto que se usará como definición de las propiedades.


	Decoradores de métodos:

	  -Se declara justo antes de una declaración de método. 
	  -El decorador se aplica al Descriptor de propiedad para el método y se puede usar 
	   para observar, modificar o reemplazar una definición de método. 
	   Un decorador de métodos no se puede usar en un archivo de declaración, sobrecarga o en 
	   cualquier otro contexto ambiental (como en una clase de declare ).
	  - La expresión del decorador de métodos se llamará como una función en tiempo de ejecución, 
	  con los siguientes tres argumentos:
		1- O bien la función de constructor de la clase para un miembro estático, o el prototipo de la clase para 
			un miembro de instancia.
        2- El nombre del miembro.
        3- El descriptor de propiedad para el miembro.

	https://es.survivejs.com/react/appendices/understanding-decorators/
	https://carlosvillu.com/decoradores-de-clases-y-propiedades-en-es7/
	https://www.spectory.com/blog/A%20deep%20dive%20into%20TypeScript%20decorators
	https://www.campusmvp.es/recursos/post/JavaScript-ECMAScript-ES6-Existe-ES7-Aclarando-las-diferentes-versiones-del-lenguaje.aspx

		Pueden ser definido mediante la creación de una funcion 
		  -dispone de 3 parámetros (target: any, propertyKey: string, descriptor: PropertyDescriptor)
		  target:any 			 		 => Prototipo de clase que se quiere decorar 
		  propertyKey:string	 	     => Nombre de la propiedad que se quiere decorar
		  
		  descriptor: PropertyDescriptor => La descripción de la propiedad del objeto, 
		  			que explica cómo esta se comporta. La descripción tiene los siguientes puntos:
						
						* configurable: si es true la propiedad puede ser modificada;
						* enumerable: si es true esta propiedad aparece cuando hagamos un for of del objecto;
						* value: es el valor asociado a la propiedad. Puede ser desde un tipo simple hasta una función;
						* writable: indica si la propiedad puede ser cambiada con una asignación;
						* get: si la propiedad es un getter del objeto, aquí podemos escribir una función. Lo devuelto por esa 
						      función se asociará al valor del atributo. En caso de no ser una función y ser undefined, se usará value,
						       como vimos antes;
						* set: similar a lo anterior, pero en este caso la función recibirá el nuevo valor del atributo.
			
			Veamos un ejemplo:

				1- Supón que quieres enviar un mensaje a la consola cada vez que un método es ejecutado.
				   Para ello vamos a crear un decorador llamado logger. 
				   Como es un decorador, no es más que una función que recibe tres argumentos.




*/

     //En este primer ejemplo de Código, vemos como decorar una propiedad de clase con un decorador que 
     //muestra en la consola el valor que le hemos dado a la propiedad cada vez que se instancie un objeto de la Clase:
/*
function enumerable(value: boolean) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
  	descriptor.value = value;
    //descriptor.enumerable = value;
    //console.log(descriptor);
  };
}
*/
function log(target, key, descriptor) {
	console.log(`
					target: ${target}
					key: ${key} fue llamado!, 
					descriptor: ${descriptor}`);

}

//folio: 382988

class P {
	@log
	foo() {
		console.log('método');
	}
}
const p = new P();
p.foo();


function enumerable222(value: boolean) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
  	descriptor.value = value;
    //descriptor.enumerable = value;
    //console.log(descriptor);
  };
}
function enumerable2(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
  	//descriptor.value = false;
    //descriptor.enumerable = value;
    console.log(descriptor);
  };
class P2 {
	//@enumerable
	foo(a, b) {
		console.log(`Do something`);
	}
}
//let p2 = new P2();
//p2.foo('hello', 'world');

class Greeter {
    greeting: string;
    constructor(message: string) {
        this.greeting = message;
    }

    @enumerable(false)
    greet() {
        return "Hello, " + this.greeting;
    }
}

function enumerable(value: boolean) {
    return function (target: any, propertyKey: string, descriptor: any) {
        descriptor.enumerable = value;
    };
}

/*
function enumerable(value: boolean) {
  return function (target, propertyKey, descriptor) {

  		  // Guardamos una referencia al generador de la función    
	  const initializer = descriptor.initializer();

		  // Creamos una copia del descriptor original
	  let desc = Object.assign({}, descriptor)

	  // Modificamos la copia para decorarlo con el log a consola
	  desc.initializer = function(){
	    return function(...args){
	      console.log( `LOGGER: call ${name} with ${args}` )
	      initializer.apply(this, args)
	    }

	  }

	  // Devolvemos nuestro nuevo descriptor modificado
	  return desc
	  

  };
}


class Greeter {
	  greeting: string;
	  constructor(message: string) {
	    this.greeting = message;
	  }

	  @enumerable(false)
	  greet() {
	    return "Hello, " + this.greeting;
	  }
}	

var miGreeter = new Greeter('mensaje');
console.log( miGreeter.greet() );	
*/


















// https://www.mmfilesi.com/blog/typescript-y-7-decorators/ 
// http://blog.builtbyedgar.com/decoradores-en-javascript-con-es7-es2016/

function trazar(target: Object, key: string, descriptor: any) {
	/*
	let a= Object.defineProperty(
			target, // objeto
			key, // nombre de la propiedad
			// propiedades descriptoras:
			{
				value: 'La Ilíada',
				writable: true,
				enumerable: true,
				configurable: true
			}
	);

	descriptor=a;
	console.log(a);
	*/

 			Object.defineProperty(target, key, {
            	 value: 37,
				  writable: true,
				  enumerable: true,
				  configurable: true
            });

	console.log('Clase: ', target.constructor.name);
	console.log('Método: ', key);
	console.log('Características del método: ', descriptor);
}

class Ejercicio {
	respuesta: string;
	@trazar
	responder(respuesta:string) {
		this.respuesta = respuesta;
	}
	getRespuesta(): string {
		return this.respuesta;
	}
}

let ejercicio = new Ejercicio();
ejercicio.responder('jump!');