/*
	Decoradores de propiedad:
		Los decoradores angular de propiedad son decoradores que deben ser declarados dentro de la propia clase.
 		-Se declara justo antes de una declaración de propiedad. 
 		- No se puede usar en un archivo de declaración ni en ningún otro contexto ambiental(como en una clase de declare ).
	    - La expresión del decorador de propiedades se llamará como una función en tiempo de ejecución, 
	     con los dos argumentos siguientes:
		    1- O bien la función de constructor de la clase para un miembro estático, 
		        o el prototipo de la clase para un miembro de instancia.
			2- El nombre del miembro.
		
		Pueden ser definido mediante la creación de una funcion property decorator
		  -dispone de 2 parámetros (target:Object y key:string).
		  target:Object  => Prototipo de clase que se quiere decorar 
		  key:string	 => Nombre de la propiedad que se quiere decorar

		  Ejemplo practico:
		   1- registrar el valor de propiedades, en el momento de instanciar el objeto de dicha clase
		   2- Ejecutar funciones asociadas a propiedades en un determinado momento
*/

     //En este primer ejemplo de Código, vemos como decorar una propiedad de clase con un decorador que 
     //muestra en la consola el valor que le hemos dado a la propiedad cada vez que se instancie un objeto de la Clase:
	function LogCambios(target:Object, key:string) {  //  Declaramos el método del decorador
			  let valorPropiedad: string = this[key];
			  
			  // Borramos antiguos valores y reasignamos el nuevo en caso de ser necesario.
			  if (delete this[key] ) {
				  	  Object.defineProperty(target, key, {	
							  get: function() {
							        // Obtenemos el valor
							        return valorPropiedad;
						      },		
							  set: function(nuevoValor) {
							        //Recuperamos el nuevo valor .
							        valorPropiedad = nuevoValor;

							        //
							        //CallbackObject.onchange.call(this,valorPropiedad);

							        //lanzamos por consola el nuevo valor
							        console.log(`La ${key} es ahora ${valorPropiedad}`);
						      }
					  });    

			  }
	  }


class Fruta {
	@LogCambios
	nombre:string;	
}

var mifruta = new Fruta();
mifruta.nombre='banana';	  //set	
mifruta.nombre='platano';	 //set
console.log(mifruta.nombre); //get




//En este segundo ejemplo creamos un decorar de propiedad que "ejecuta una función 
//cada vez que se detecte un cambio" en el valor de la propiedad al instanciarse un objeto de la clase:

function LogCambios2(callbackObject: any): Function {  // esta es una fabrica de decoradores
    return function(target: Object, key: string): void { // Declaramos el método del decorador
        var valorPropiedad: string = this[key];

        // Borramos antiguos valores y reasignamos el nuevo en caso de ser necesario.
        if (delete this[key]) {
            Object.defineProperty(target, key, {
              get: function() {
                  return valorPropiedad; // Obtenemos el valor
              },
              set: function(nuevoValor) {
                  valorPropiedad = nuevoValor; //Recuperamos el nuevo valor .
                  //ejecuta una función cada vez que se detecte un cambio en el valor de la propiedad al 
                  //instanciarse un objeto de la clase

                  callbackObject.cambios.call(this,        
                                               valorPropiedad);
              }
            });
        }
    }
}

class Fruta2 {
    @LogCambios2({
        
        cambios: function(nuevoValor: string): void {
            console.log(`La fruta es ${nuevoValor} ahora`);
        }

    })
    nombre: string;
}

var fruta = new Fruta();
fruta.nombre = 'banana';    // consola: 'La fruta es banana ahora'
fruta.nombre = 'plátano'; // console: 'La fruta es plátano ahora'



/*


@Bienvenida('que tal!')
@Nativo
class Saludar {
  constructor(){
	//console.log('constructor!');  	
    // Lo que corresponda.
  }


}

var miSaludo = new Saludar();
miSaludo.nativo1();
miSaludo.saludo();
console.log( miSaludo.nativo2() ) ;
*/