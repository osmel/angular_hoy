/*
es necesario correrlo de esta manera porque el compilador, no esta teniendo en cuenta el --target

 tsc --target ES2016 --experimentalDecorators
  

  Decoradores de métodos:
    -Se declara justo antes de una declaración de método. 
    -El decorador se aplica al "Descriptor de propiedad para el método" y se puede usar 
     para observar, modificar o reemplazar una definición de método. 
     
     - Un decorador de métodos no se puede usar en un archivo de declaración, sobrecarga o en 
     cualquier otro contexto ambiental (como en una clase de declare ).
    - La expresión del decorador de métodos se llamará como una función en tiempo de ejecución, 
    con los siguientes tres argumentos:
        1- O bien la función de constructor de la clase para un miembro estático, o el prototipo de la clase para 
      un miembro de instancia.
        2- El nombre del miembro.
        3- El descriptor de propiedad para el miembro.


    Pueden ser definido mediante la creación de una funcion 
      -dispone de 3 parámetros (target: any, propertyKey: string, descriptor: PropertyDescriptor)
    
      target:any                  => la clase a la que pertenece el método decorado (lo tipamos como Object).
      propertyKey:string          => Una cadena con el nombre del método decorado.
      
      descriptor: PropertyDescriptor => las propiedades descriptoras del método decorado.Explica cómo esta 
                                      se comporta. La descripción tiene los siguientes puntos:
            
            * configurable: si es true la propiedad puede ser modificada;
            * enumerable: si es true esta propiedad aparece cuando hagamos un for of del objecto;
            * value: es el valor asociado a la propiedad. Puede ser desde un tipo simple hasta una función;
            * writable: indica si la propiedad puede ser cambiada con una asignación;
            * get: si la propiedad es un getter del objeto, aquí podemos escribir una función. Lo devuelto por esa 
                  función se asociará al valor del atributo. En caso de no ser una función y ser undefined, se usará value,
                   como vimos antes;
            * set: similar a lo anterior, pero en este caso la función recibirá el nuevo valor del atributo.
      
      Veamos un ejemplo:

        1- Supón que quieres enviar un mensaje a la consola cada vez que un método es ejecutado.
           Para ello vamos a crear un decorador llamado logger. 
           Como es un decorador, no es más que una función que recibe tres argumentos.


//folio: 382988


     Ejemplo1: Decorador que puede ser aplicado a cualquier método, para mostrar por consola:
         -la clase a la que pertenece el método
         - el nombre del método
         - el valor que tiene las propiedades descriptoras del método
*/


function mostrarDatosDelMetodo(claseMetodo: any, nombreMetodo: string, descriptor: PropertyDescriptor) {

      console.log('clase a la que pertenece el método ', claseMetodo);
      console.log('nombre del método ', nombreMetodo);
      console.log('valor que tiene las propiedades descriptoras del método ', descriptor);

}



class clasePrueba {
  
  @mostrarDatosDelMetodo
  foo() {
    console.log('método');
  }
}
const p = new clasePrueba();
p.foo();


///////////segundo ejemplo

function enumerable(value: boolean) {
    return function (target: any, propertyKey: string, descriptor: any) {
        descriptor.enumerable = value; // si es true esta propiedad aparece cuando hagamos un for of del objecto;
    };
}

function enumerable333(value: boolean) {
      return function (target: any, propertyKey: string, descriptor: any) {

            // Guardamos una referencia al generador de la función    
        const initializer = descriptor.initializer();

          // Creamos una copia del descriptor original
        let desc = Object.assign({}, descriptor)

        // Modificamos la copia para decorarlo con el log a consola
        desc.initializer = function(){
            return function(...args){
              console.log( `LOGGER: call ${name} with ${args}` )
              initializer.apply(this, args)
            }
        }

        // Devolvemos nuestro nuevo descriptor modificado
        return desc
        

      };
}

class Greeter {
    greeting: string;
    constructor(message: string) {
        this.greeting = message;
    }

    @enumerable(false)
    greet() {
        return "Hello, " + this.greeting;
    }
}


var miGreeter = new Greeter('mensaje');
console.log( miGreeter.greet() );  
