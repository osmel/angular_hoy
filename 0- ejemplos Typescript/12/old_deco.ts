/*
function enumerable(value: boolean) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    descriptor.value = value;
    //descriptor.enumerable = value;
    //console.log(descriptor);
  };
}
*/


function enumerable222(value: boolean) {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    descriptor.value = value;
    //descriptor.enumerable = value;
    //console.log(descriptor);
  };
}
function enumerable2(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    //descriptor.value = false;
    //descriptor.enumerable = value;
    console.log(descriptor);
  };
class P2 {
  //@enumerable
  foo(a, b) {
    console.log(`Do something`);
  }
}
//let p2 = new P2();
//p2.foo('hello', 'world');


/*
function enumerable(value: boolean) {
  return function (target, propertyKey, descriptor) {

        // Guardamos una referencia al generador de la función    
    const initializer = descriptor.initializer();

      // Creamos una copia del descriptor original
    let desc = Object.assign({}, descriptor)

    // Modificamos la copia para decorarlo con el log a consola
    desc.initializer = function(){
      return function(...args){
        console.log( `LOGGER: call ${name} with ${args}` )
        initializer.apply(this, args)
      }

    }

    // Devolvemos nuestro nuevo descriptor modificado
    return desc
    

  };
}


class Greeter {
    greeting: string;
    constructor(message: string) {
      this.greeting = message;
    }

    @enumerable(false)
    greet() {
      return "Hello, " + this.greeting;
    }
}  

var miGreeter = new Greeter('mensaje');
console.log( miGreeter.greet() );  
*/


















// https://www.mmfilesi.com/blog/typescript-y-7-decorators/ 
// http://blog.builtbyedgar.com/decoradores-en-javascript-con-es7-es2016/

function trazar(target: Object, key: string, descriptor: any) {
  /*
  let a= Object.defineProperty(
      target, // objeto
      key, // nombre de la propiedad
      // propiedades descriptoras:
      {
        value: 'La Ilíada',
        writable: true,
        enumerable: true,
        configurable: true
      }
  );

  descriptor=a;
  console.log(a);
  */

       Object.defineProperty(target, key, {
               value: 37,
          writable: true,
          enumerable: true,
          configurable: true
            });

  console.log('Clase: ', target.constructor.name);
  console.log('Método: ', key);
  console.log('Características del método: ', descriptor);
}

class Ejercicio {
  respuesta: string;
  @trazar
  responder(respuesta:string) {
    this.respuesta = respuesta;
  }
  getRespuesta(): string {
    return this.respuesta;
  }
}

let ejercicio = new Ejercicio();
ejercicio.responder('jump!');












function log22(target: any,  key: string, descriptor: PropertyDescriptor) {
  descriptor.enumerable=undefined;
  /*const originalMethod = descriptor.value;
  descriptor.value = function () {
    console.log(`${key} was called with:`,arguments);
    var result = originalMethod.apply(this, arguments);
    return result;
  };
  return descriptor;*/
}



function log23(value: boolean) {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
      console.log("descriptor");
        descriptor.enumerable = value;
    };
}
