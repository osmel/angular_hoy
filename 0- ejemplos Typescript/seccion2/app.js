//Booleanos
var mivariable = false; //(forma recomendada)
var mivariable2;
var mivariable3 = true; //(no recomendada)
console.log(mivariable3);
//Números
var decimal = 6;
var hex = 0xf00d;
var binary = 10;
var octal = 484;
console.log(octal);
//string
var color = "blue";
color = 'red';
//backtick respeta espacio y todo, ademas usa valores como plantillas	
var fullName = "Bob Bobbington";
var age = 37;
var sentence = "Hello, my name is " + fullName + ".  \n\t\t\t\t\t\t\t\tI'll be " + (age + 1) + " years old next month.";
console.log(sentence);
/*template literales    ${variableDentroDeUnBarticks} */
var minombre1 = "duvi";
var minombre2 = 'alex';
//barticks
var mensaje = "1. new mensaje\n\t\t\t2. Hola " + minombre1 + "\n\t\t\t3. Hola " + minombre2 + "\n\t\t";
console.log(mensaje);
//Any (puede ponerse cualquier valor, va mutando de variable según le vas asociando nuevos valores)
var mivariable4;
mivariable4 = "texto";
console.log(mivariable4.charAt(0)); //t
mivariable4 = 150.555;
console.log(mivariable4.toFixed(2)); //150.56
mivariable4 = true;
console.log(mivariable4); //true		
var loquesea = "osmel";
loquesea = 2;
console.log(loquesea);
//con las tuberias podemos tambien restringirlos a varios tipos
var numero_cadena_booleano = 2;
numero_cadena_booleano = "como texto"; //como texto
numero_cadena_booleano = {
    nombre: "osmel",
    edad: 41,
    hijos: ["duvi", "alex", "fidel"],
    getNombre: function () {
        return this.nombre;
    }
};
//numero_cadena_booleano2 = true;  //esto sería un error porque no es any tiene restricciones a los tipos
//Array
var lista3 = [1, 2, 3]; //aqui deduce ts que es un arreglo de tipo numero
var lista = [1, 2, 3]; //arreglo de tipo numero
lista.push(5);
console.log(lista);
var lista1 = ["duvi", "alex", "fide"]; //arreglo de tipo cadena
var lista2 = [1, 2, 3]; //usa un tipo genérico de matriz Array<elemType>
//Tuplas (es el arreglo, pero con varios tipos). Cuando sabemos de antemano, cuales son los valores que va a tener un arreglo
// Declarando tipo de tuplas
var x = ["hello", 10];
x[0] = 'texto';
x[1] = 2;
//Enumeraciones (son pares de valores ordenados, donde me puedo referir tanto al numero como a la cadena)
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {})); //toma valores por defecto  Red=0, Green=1, Blue=2
var micolor = Color.Red;
console.log(micolor); //esto dara "0", 
var objecto;
(function (objecto) {
    objecto[objecto["Blue"] = 1] = "Blue";
    objecto[objecto["Green"] = 2] = "Green";
    objecto[objecto["Red"] = 10] = "Red";
})(objecto || (objecto = {}));
//console.log(objecto.Green); //2	  			
console.log(objecto); //{1: "Blue", 2: "Green", 10: "Red", Blue: 1, Green: 2, Red: 10}			
console.log(objecto[10]); //Red
//Void   opuesto al any=cualquier tipo, y void = ningún tipo
function warnUser() {
    console.log("texto mio");
    //es incorrecto usar un "return"
}
var mensaje = warnUser(); // valor resultado es "undefined"
//Never	 //representa un valor que nunca puede suceder
//es decir, si la aplicación llega a este punto, debe terminar. Porque significa que algo fallo
function error(message) {
    throw new Error(message);
}
//Hace un punto critico en nuestro programa. termina nuestra aplicación
//error('mi aplicacion debe terminar');
//casteando valores (aserciones de tipos)
var mivariable5 = "osmel calderon";
//como ts no sabe que tipo es pues, le indico que lo trabaje como un string, y entonces
//pues puedo usar la función 	length, puedo castear de 2 formas
var largo1 = mivariable5.length; //casteando como generico
var largo2 = mivariable5.length; //otra forma de castear
console.log(largo2);
//Null y Undefined (tipos de datos especificos)
var indefinido = undefined; //es algo así como "nada"
var nulo = null; //algo así como nulo
indefinido = null; //es valido si -> strict :false (opcion que hace restrictivo el ts)
//////////////////////////checando el tipo de datos
var cosa = 123;
console.log(typeof cosa); //esto regresa el tipo de datos en string
if (typeof cosa == "number") {
    console.log("cosa es un número");
}
