//Booleanos
		let mivariable: boolean = false; //(forma recomendada)
		let mivariable2: boolean;
		let mivariable3 = true; //(no recomendada)

		console.log(mivariable3);

//Números
		let decimal: number = 6;
		let hex: number = 0xf00d;
		let binary: number = 0b1010;
		let octal: number = 0o744;

		console.log(octal);



//string
		let color: string = "blue";
		color = 'red';

		//backtick respeta espacio y todo, ademas usa valores como plantillas	
		let fullName: string = `Bob Bobbington`; 
		let age: number = 37;
		let sentence: string = `Hello, my name is ${ fullName }.  
								I'll be ${ age + 1 } years old next month.`; 

		console.log(sentence);



		/*template literales    ${variableDentroDeUnBarticks} */
		let minombre1:string = "duvi";
		let minombre2:string = 'alex';
		   
		   //barticks
		let mensaje:string = `1. new mensaje
			2. Hola ${minombre1}
			3. Hola ${minombre2}
		`;

		console.log(mensaje);



//Any (puede ponerse cualquier valor, va mutando de variable según le vas asociando nuevos valores)

	   let mivariable4: any;

		mivariable4="texto";
		console.log(mivariable4.charAt(0)); //t
		mivariable4=150.555;
		console.log(mivariable4.toFixed(2)); //150.56
		mivariable4=true;
		console.log(mivariable4);  //true		



			//un caso en que se puede restringir a tipos por tuberias

		
/////////////un caso especial en el que se quiera definir una variable de varios tipos
		
		type miObjeto1= { 
		    nombre:string, 
		 	edad:number, 
		 	hijos:string[], 
		 	getNombre:()=>string 
		 };



		let loquesea:any = "osmel";
		    loquesea = 2;
		    console.log(loquesea);


		//con las tuberias podemos tambien restringirlos a varios tipos
		let numero_cadena_booleano: number | string | miObjeto1 = 2;
		    
		    numero_cadena_booleano = "como texto";  //como texto
		    
		    numero_cadena_booleano = {  //como objeto
					    		nombre: "osmel",
							    edad: 41,
							    hijos: ["duvi", "alex", "fidel"],
							    getNombre(){
							    	return this.nombre;
							    }
			}

      //numero_cadena_booleano2 = true;  //esto sería un error porque no es any tiene restricciones a los tipos

    



//Array
		let lista3 = [1, 2, 3];  //aqui deduce ts que es un arreglo de tipo numero

		let lista: number[] = [1, 2, 3];   //arreglo de tipo numero
			lista.push(5);
		console.log(lista);	
		
		let lista1: string[] = ["duvi", "alex", "fide"];   //arreglo de tipo cadena


		let lista2: Array<number> = [1, 2, 3];  //usa un tipo genérico de matriz Array<elemType>

		

//Tuplas (es el arreglo, pero con varios tipos). Cuando sabemos de antemano, cuales son los valores que va a tener un arreglo
	   
	     // Declarando tipo de tuplas
		let x: [string, number]= ["hello", 10];
		x[0] = 'texto';
		x[1] = 2;



//Enumeraciones (son pares de valores ordenados, donde me puedo referir tanto al numero como a la cadena)

	  enum Color {Red, Green, Blue} //toma valores por defecto  Red=0, Green=1, Blue=2
	  let micolor:number = Color.Red;
	  console.log(micolor);  //esto dara "0", 


	  enum objecto {  //pares de valores ordenados
	  				Blue=1,
	  				Green,  //este es el anterior+1 por tanto =2
	  				Red=10
	  			}

		//console.log(objecto.Green); //2	  			
		console.log(objecto); //{1: "Blue", 2: "Green", 10: "Red", Blue: 1, Green: 2, Red: 10}			
		console.log(objecto[10]); //Red




//Void   opuesto al any=cualquier tipo, y void = ningún tipo
		function warnUser(): void {   //esta función no regresa nada, es decir regresa undefined
		    console.log("texto mio");
		    //es incorrecto usar un "return"
		}	  
		let mensaje=warnUser(); // valor resultado es "undefined"





//Never	 //representa un valor que nunca puede suceder
			//es decir, si la aplicación llega a este punto, debe terminar. Porque significa que algo fallo
			function error(message: string): never {
			    throw new Error(message);
			}

			//Hace un punto critico en nuestro programa. termina nuestra aplicación
			//error('mi aplicacion debe terminar');



//casteando valores (aserciones de tipos)
	  
	  let mivariable5: any = "osmel calderon";	
	      //como ts no sabe que tipo es pues, le indico que lo trabaje como un string, y entonces
	      //pues puedo usar la función 	length, puedo castear de 2 formas

	  let largo1: number = (<string>mivariable5).length;  //casteando como generico
	  let largo2: number = (mivariable5 as string).length;  //otra forma de castear
	  console.log(largo2);


//Null y Undefined (tipos de datos especificos)

		let indefinido: undefined = undefined;  //es algo así como "nada"
		let nulo: null = null;               //algo así como nulo

		indefinido=null; //es valido si -> strict :false (opcion que hace restrictivo el ts)


//////////////////////////checando el tipo de datos
	let cosa:any =123;
	console.log(typeof cosa); //esto regresa el tipo de datos en string
	if (typeof cosa =="number") {
		console.log("cosa es un número");
	}

