
/////////interfaz con tipo dentro una función
			interface IMetadato {
				 ano:number;
				 mes:number;
				 regenerar(nombreReal:string):void;
				 otrafuncion?(paso:string):void; //esta es opcional no es obligatoria implementarla

			}	

			interface IEtiqueta {
			  fecha?:string; //este no es obligatorio
			  etiqueta: string;
			  tamano:number;
			  otro: IMetadato;  //tipo interfaz
			}

			function printLabel(objEtiqueta: IEtiqueta) {
			  console.log(objEtiqueta.etiqueta, objEtiqueta.tamano, objEtiqueta.otro.ano);
			  objEtiqueta.otro.regenerar("mensaje de regenerar");
			}

			//tipo de dato que implementa la interfaz
			let myObj: IEtiqueta = {
						tamano: 10, 
						etiqueta: "ropa", 
						otro: {
								ano:2018,
								mes:8,
								regenerar(paso:string):void{
									console.log(paso);

								}
						} 
					};

			printLabel(myObj);


/////////////////////////////interfaz para obligar tipos de parametros, para una función

				interface IDosNumeros{
					(x:number, y:number) :number;
				}

				let sumar1:IDosNumeros;
				let restar1:IDosNumeros;

				sumar1 = function(x:number, y:number) {
					return x+y;
				}

				restar1 = function(x:number, y:number) {
					return x-y;
				}

				//nota: en este caso las funciones tienen que respetar los parametros de su interfaz, no se le puede declarar otro tipo de parametro

console.log('----------------');

/////////interfaz con clases

			interface IUniversidad{
			  ano_graduado:number
			}

			interface IProfesion extends IUniversidad{ //herencia entre interfaces
				 graduado:string;
			}

			interface IEntidad{
				 ubicacion:string;
			}


			interface IUser{
			    nombre: string;
			    salario?: number; //opcional, no es obligatorio
			    getNombre(): string;
			}


			class Personas implements IUser{
			    nombre: string;
			    constructor(name: string) {
			        this.nombre= name;
			    }
			    getNombre(): string{
			        return this.nombre;
			    }    
			}

			class Alumnos extends Personas {

			}

			class Trabajadores extends Personas implements IProfesion, IEntidad{  //multiples implementaciones de multiples interfaces
				graduado:string='informatica';
				ubicacion:string='cdmx';
				ano_graduado:number = 2000;

			    constructor(name: string) {
			        super(name);

			    }

			}


			let mio = new Trabajadores("osmel"); 
			console.log(mio);

