var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
function printLabel(objEtiqueta) {
    console.log(objEtiqueta.etiqueta, objEtiqueta.tamano, objEtiqueta.otro.ano);
    objEtiqueta.otro.regenerar("mensaje de regenerar");
}
//tipo de dato que implementa la interfaz
var myObj = {
    tamano: 10,
    etiqueta: "ropa",
    otro: {
        ano: 2018,
        mes: 8,
        regenerar: function (paso) {
            console.log(paso);
        }
    }
};
printLabel(myObj);
var sumar1;
var restar1;
sumar1 = function (x, y) {
    return x + y;
};
restar1 = function (x, y) {
    return x - y;
};
//nota: en este caso las funciones tienen que respetar los parametros de su interfaz, no se le puede declarar otro tipo de parametro
console.log('----------------');
var Personas = /** @class */ (function () {
    function Personas(name) {
        this.nombre = name;
    }
    Personas.prototype.getNombre = function () {
        return this.nombre;
    };
    return Personas;
}());
var Alumnos = /** @class */ (function (_super) {
    __extends(Alumnos, _super);
    function Alumnos() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Alumnos;
}(Personas));
var Trabajadores = /** @class */ (function (_super) {
    __extends(Trabajadores, _super);
    function Trabajadores(name) {
        var _this = _super.call(this, name) || this;
        _this.graduado = 'informatica';
        _this.ubicacion = 'cdmx';
        _this.ano_graduado = 2000;
        return _this;
    }
    return Trabajadores;
}(Personas));
var mio = new Trabajadores("osmel");
console.log(mio);
