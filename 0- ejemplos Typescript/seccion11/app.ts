	

/*
sino ponemos genericos, ts no nos avisa en tiempo de compilación de los errores, solo nos envia error
en tiempo de ejecución
  ejemplo 
  console.log( regresar("osmel calderon").toFixed(2)  );
  por tanto vamos a convertir nuestra función en funcion generica
    function regresar<T>(arg:T){     
		return arg;
	}

*/


function regresar<T>(arg:T){
		return arg;
}

console.log( regresar(153.3333).toFixed(2)  );
console.log( regresar("osmel calderon").charAt(1) );
console.log( regresar( new Date() ).getTime()  );