// http://www.typescriptlang.org/docs/handbook/functions.html
//Declaraciones básicas de funciones
//función nombrada
function imprime1() {
    return 'mi impresion';
}
//función anonima
var mifuncion1 = function () {
    return "otro datos";
};
console.log(imprime1());
/*
tipos de parametros
      -Obligatorio    -->nombre:string
      -opcional( ? )  -->edad?:string  //puede mandarlo o no
      -por defecto    -->sexo:boolean=true
      -parametros rest  --> ...losDemasParametros:string[] .
                         algo así como todo lo que viene por parametros de forma adicional
                         juntarlo en un arreglo, por tanto me refirire a estos ultimos como parametro[0..n]
*/
function imprime(nombre, edad, sexo) {
    if (sexo === void 0) { sexo = true; }
    var losDemasParametros = [];
    for (var _i = 3; _i < arguments.length; _i++) {
        losDemasParametros[_i - 3] = arguments[_i];
    }
    console.log(losDemasParametros.join(" ")); //unimos con un espacio
    if (edad) { //para controlar si es indefinido. sino paso valor
    }
    console.log(losDemasParametros[1]); //para el caso de parametros adicionales
    return 'cadena';
}
console.log(imprime('osmel', 20, true, "uno", "dos", "tres")); // "uno", "dos", "tres" -->los recibe como un arreglo
//////////Tipo de datos "Function" (31)     ..ES UNA FORMA DE MUTAR LAS FUNCIONES
var funcion1 = function (x, y) { return x + y; };
function sumar(x, y) {
    return x + y;
}
;
//declaramos una variable para que pueda recoger multiples funciones
var funcionMultiple;
//en este caso especificamos el valor que recoge la variable. Y LUEGO accedemos a la funcion con sus parametros
funcionMultiple = funcion1;
console.log(funcionMultiple(2, 3));
//otro ejemplo  
funcionMultiple = imprime;
console.log(funcionMultiple('osmel', 20, true, "uno", "dos", "tres"));
///////Definiendo reglas para las funciones...
/////podemos hacer restricciones a las funciones, para q se limiten a solo el tipo que queremos
//definiendo reglas(restricciones) para la variable de un tipo de funcion que solo aceptará estos tipos
var mifuncion; //solo acepta numero y retorna numero
//esta si me la va aceptar con la restriccion de encima, porque cumple con las expectativa q espera la función
mifuncion = sumar;
console.log(mifuncion(2, 3));
//mifuncion = imprime; ya esta no la acepta porque pusimos restricciones de parametros
//y del tipo que debe devolver la función 
