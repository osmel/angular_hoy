// http://www.typescriptlang.org/docs/handbook/functions.html
//Declaraciones básicas de funciones

//función nombrada
	function imprime1():string {     //funcion retornando una cadena
		return 'mi impresion';
	}
//función anonima
	let mifuncion1 = function():string { 
		return "otro datos";	
	}
	console.log( imprime1()  );

/*
tipos de parametros
	  -Obligatorio    -->nombre:string
	  -opcional( ? )  -->edad?:string  //puede mandarlo o no
	  -por defecto    -->sexo:boolean=true
	  -parametros rest  --> ...losDemasParametros:string[] . 
	                     algo así como todo lo que viene por parametros de forma adicional 
	                     juntarlo en un arreglo, por tanto me refirire a estos ultimos como parametro[0..n]
*/	  


    function imprime(nombre:string,edad?:number, sexo:boolean=true, ...losDemasParametros:string[] ):string {    
		
		console.log( losDemasParametros.join(" ") ); //unimos con un espacio

		if (edad){ //para controlar si es indefinido. sino paso valor

		}
		console.log(losDemasParametros[1]);  //para el caso de parametros adicionales

		return 'cadena';
	}

   console.log(  imprime('osmel', 20, true, "uno", "dos", "tres") );  // "uno", "dos", "tres" -->los recibe como un arreglo


//////////Tipo de datos "Function" (31)     ..ES UNA FORMA DE MUTAR LAS FUNCIONES

 
		  let funcion1 = function(x: number, y: number): number { return x + y; };

		  function sumar(x: number, y: number): number   { 
		  	return x + y; 
		  };


		 //declaramos una variable para que pueda recoger multiples funciones
		  let funcionMultiple;
	    
	    //en este caso especificamos el valor que recoge la variable. Y LUEGO accedemos a la funcion con sus parametros
 	       funcionMultiple = funcion1;
	       console.log( funcionMultiple(2,3) );

	     //otro ejemplo  
		  funcionMultiple = imprime;
  	      console.log( funcionMultiple('osmel', 20, true, "uno", "dos", "tres") );
		  
		

		///////Definiendo reglas para las funciones...
		/////podemos hacer restricciones a las funciones, para q se limiten a solo el tipo que queremos

		//definiendo reglas(restricciones) para la variable de un tipo de funcion que solo aceptará estos tipos
		let mifuncion: (a:number, b:number) => number;   //solo acepta numero y retorna numero
		//esta si me la va aceptar con la restriccion de encima, porque cumple con las expectativa q espera la función
		mifuncion = sumar;
		console.log(  mifuncion(2,3)  );   


		      //mifuncion = imprime; ya esta no la acepta porque pusimos restricciones de parametros
		        //y del tipo que debe devolver la función 
		
		