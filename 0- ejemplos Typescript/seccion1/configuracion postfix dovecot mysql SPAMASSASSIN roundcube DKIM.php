
instalacion de postfix y dovecot
    sudo apt-get install postfix postfix-mysql dovecot-core dovecot-imapd dovecot-lmtpd dovecot-mysql

instalar este paquete para pop3
    sudo apt-get install dovecot-pop3d 



forjadoresdelfuturo.com.mx


     dig MX forjadoresdelfuturo.com.mx +short @ns1.digitalocean.com
     host mail.forjadoresdelfuturo.com.mx ns1.digitalocean.com

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////MYSQL/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

Crear la base de datos cronos
  mysqladmin -p create cronos
Inicie la sesión como usuario root de MySQL
   mysql -u root -p
Tenemos que crear un nuevo usuario, específico para la autenticación de correo, y vamos a darle el permiso SELECT.
    GRANT SELECT ON cronos.* TO 'osmel'@'127.0.0.1' IDENTIFIED BY 'osmel5458';
    //GRANT SELECT ON cronos.* TO 'fantastes'@'127.0.0.1' IDENTIFIED BY 'contraseña';

Necesitamos recargar privilegios de MySQL para garantizar que se aplica con éxito esos permisos:
   FLUSH PRIVILEGES;
En uso la base de datos para crear tablas e introducir nuestros datos:
   USE cronos;
Vamos a crear una tabla para los dominios específicos reconocidos como dominios autorizados.
		CREATE TABLE `virtual_domains` (
		`id`  INT NOT NULL AUTO_INCREMENT,
		`name` VARCHAR(50) NOT NULL,
		PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
Vamos a crear una tabla para presentar a los usuarios. Aquí va a agregar la dirección de correo electrónico y contraseñas. Es necesario asociar a cada usuario con un dominio.
		CREATE TABLE `virtual_users` (
		`id` INT NOT NULL AUTO_INCREMENT,
		`domain_id` INT NOT NULL,
		`password` VARCHAR(106) NOT NULL,
		`email` VARCHAR(120) NOT NULL,
		PRIMARY KEY (`id`),
		UNIQUE KEY `email` (`email`),
		FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
Por último vamos a crear una tabla de alias virtual para especificar todos los correos electrónicos que se van a transmitir al otro correo electrónico.
		CREATE TABLE `virtual_aliases` (
		`id` INT NOT NULL AUTO_INCREMENT,
		`domain_id` INT NOT NULL,
		`source` varchar(100) NOT NULL,
		`destination` varchar(100) NOT NULL,
		PRIMARY KEY (`id`),
		FOREIGN KEY (domain_id) REFERENCES virtual_domains(id) ON DELETE CASCADE
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;


Virtual Domains
   Aquí vamos a introducir a sus dominios dentro de la tabla virtual_domains. Puede agregar todos los dominios que desea, pero en este tutorial vamos a introducir sólo el dominio principal (forjadoresdelfuturo.com.mx) y FQDN (hostname.forjadoresdelfuturo.com.mx).
			INSERT INTO `virtual_domains`
			(`id` ,`name`) VALUES
			('1', 'forjadoresdelfuturo.com.mx'),
			('2', 'forjadoresdelfuturo.com.mx');


Los correos electrónicos virtuales
  Vamos a introducir la dirección de correo electrónico y contraseñas asociadas para cada dominio. Asegúrese de cambiar toda la información con su información específica.
		INSERT INTO `virtual_users` (`id`, `domain_id`, `password` , `email`) VALUES
		('1', '1', ENCRYPT('osmel5458', CONCAT('$6$', SUBSTRING(SHA(RAND()), -16))), 'osmel@forjadoresdelfuturo.com.mx'),
		('2', '1', ENCRYPT('osmel5458', CONCAT('$6$', SUBSTRING(SHA(RAND()), -16))), 'admin@forjadoresdelfuturo.com.mx');

Los alias virtuales
  Vamos a introducir la dirección de correo electrónico (source) que vamos a transmitir a la otra dirección de correo electrónico (destination).
		INSERT INTO `virtual_aliases` (`id`, `domain_id`, `source`, `destination`) VALUES
		('1', '1', 'alias@forjadoresdelfuturo.com.mx', 'osmel@forjadoresdelfuturo.com.mx'),
		('2', '1', 'admin@forjadoresdelfuturo.com.mx', 'admin@forjadoresdelfuturo.com.mx');

 Salir de MYsql
 		mysql > exit


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////Configurar Postfix////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	cp /etc/postfix/main.cf /etc/postfix/main.cf.orig

	pico /etc/postfix/main.cf
		smtpd_tls_cert_file=/etc/apache2/ssl/8807847e49239183.crt
		smtpd_tls_key_file=/etc/apache2/ssl/forjadoresdelfuturo.key
		smtpd_use_tls=yes
		#smtpd_tls_auth_only = yes  //


		smtpd_sasl_type = dovecot
		smtpd_sasl_path = private/auth
		smtpd_sasl_auth_enable = yes
		smtpd_recipient_restrictions =
		                permit_sasl_authenticated,
		                permit_mynetworks,
		                reject_unauth_destination


		mydestination = localhost
		myhostname = forjadoresdelfuturo.com.mx
		#mydestination = localhost
		virtual_transport = lmtp:unix:private/dovecot-lmtp

		virtual_mailbox_domains = mysql:/etc/postfix/mysql-virtual-mailbox-domains.cf
		virtual_mailbox_maps = mysql:/etc/postfix/mysql-virtual-mailbox-maps.cf
		virtual_alias_maps = mysql:/etc/postfix/mysql-virtual-alias-maps.cf

-----------
  pico /etc/postfix/mysql-virtual-mailbox-domains.cf
user = osmel
password = osmel5458
hosts = 127.0.0.1
dbname = cronos
query = SELECT 1 FROM virtual_domains WHERE name='%s'
  service postfix restart
  postmap -q forjadoresdelfuturo.com.mx mysql:/etc/postfix/mysql-virtual-mailbox-domains.cf
-----------

  pico /etc/postfix/mysql-virtual-mailbox-maps.cf
user = osmel
password = osmel5458
hosts = 127.0.0.1
dbname = cronos
query = SELECT 1 FROM virtual_users WHERE email='%s'
  service postfix restart
  postmap -q osmel@forjadoresdelfuturo.com.mx mysql:/etc/postfix/mysql-virtual-mailbox-maps.cf
-----------
  pico /etc/postfix/mysql-virtual-alias-maps.cf
		user = osmel
		password = osmel5458
		hosts = 127.0.0.1
		dbname = cronos
		query = SELECT destination FROM virtual_aliases WHERE source='%s'
  service postfix restart
  postmap -q alias@forjadoresdelfuturo.com.mx mysql:/etc/postfix/mysql-virtual-alias-maps.cf
-----------

  cp /etc/postfix/master.cf /etc/postfix/master.cf.orig
  pico /etc/postfix/master.cf

  	//descomentar estas lineas
	submission inet n       -       -       -       -       smtpd
		-o syslog_name=postfix/submission
		-o smtpd_tls_security_level=encrypt
		-o smtpd_sasl_auth_enable=yes
		-o smtpd_client_restrictions=permit_sasl_authenticated,reject  		  

  service postfix restart

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////Configurar DOVECOT/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

			cp /etc/dovecot/dovecot.conf /etc/dovecot/dovecot.conf.orig
			cp /etc/dovecot/conf.d/10-mail.conf /etc/dovecot/conf.d/10-mail.conf.orig
			cp /etc/dovecot/conf.d/10-auth.conf /etc/dovecot/conf.d/10-auth.conf.orig
			cp /etc/dovecot/dovecot-sql.conf.ext /etc/dovecot/dovecot-sql.conf.ext.orig
			cp /etc/dovecot/conf.d/10-master.conf /etc/dovecot/conf.d/10-master.conf.orig
			cp /etc/dovecot/conf.d/10-ssl.conf /etc/dovecot/conf.d/10-ssl.conf.orig

		archivo de configuración de Dovecot
			pico /etc/dovecot/dovecot.conf 
				!include conf.d/*.conf
				!include_try /usr/share/dovecot/protocols.d/*.protocol
				protocols = imap pop3 lmtp

		archivo de configuración de correo
			pico /etc/dovecot/conf.d/10-mail.conf   
				mail_location = maildir:/var/mail/vhosts/%d/%n
				mail_privileged_group = mail


		Compruebe los permisos
		  ls -ld /var/mail		
		  	   drwxrwsr-x 3 root mail 4096 Jan 24 21:23 /var/mail

		  	    //crear una carpeta para cada dominio 
		  	       mkdir -p /var/mail/vhosts/forjadoresdelfuturo.com.mx


				//Crear un usuario vmail y el grupo con un id de 5000
				groupadd -g 5000 vmail 
				useradd -g vmail -u 5000 vmail -d /var/mail

				//Tenemos que cambiar el propietario de la carpeta /var/mail para el usuario vmail.
				chown -R vmail:vmail /var/mail

		  ls -ld /var/mail				
			  drwxrwsr-x 3 vmail vmail 4096 Apr 24 16:38 /var/mail/


		Editar el archivo /etc/dovecot/conf.d/10-auth.conf:
				pico /etc/dovecot/conf.d/10-auth.conf
					disable_plaintext_auth = yes
					//Modificar parámetro auth_mechanisms:
					auth_mechanisms = plain login
					Comentar esta línea:
					#!include auth-system.conf.ext
					Habilitar la autorización de MySQL,  descomentando esta línea:
					!include auth-sql.conf.ext

		Información para la autenticación:			
				pico /etc/dovecot/conf.d/auth-sql.conf.ext			

						passdb {
						  driver = sql
						  args = /etc/dovecot/dovecot-sql.conf.ext
						}
						userdb {
						  driver = static
						  args = uid=vmail gid=vmail home=/var/mail/vhosts/%d/%n
						} 

			
		Información de nuestra personalizacion de MySQL					
				pico  /etc/dovecot/dovecot-sql.conf.ext
					driver = mysql
					connect = host=127.0.0.1 dbname=cronos user=osmel password=osmel5458
					default_pass_scheme = SHA512-CRYPT
					password_query = SELECT email as user, password FROM virtual_users WHERE email='%u';


		Cambiar el propietario y el grupo de la carpeta dovecot al usuario vmail:
				ls -ld /etc/dovecot
					drwxr-xr-x 4 root root 4096 Apr 24 16:35 /etc/dovecot

				chown -R vmail:dovecot /etc/dovecot
				chmod -R o-rwx /etc/dovecot
				ls -ld /etc/dovecot
					drwxr-x--- 4 vmail dovecot 4096 Apr 24 16:35 /etc/dovecot
 					



		/////////////////////////////////////

			pico /etc/dovecot/conf.d/10-master.conf 

						service imap-login {
						  inet_listener imap {
						    port = 0
						}

						#Create LMTP socket and this configurations
						service lmtp {
						   unix_listener /var/spool/postfix/private/dovecot-lmtp {
						           mode = 0600
						           user = postfix
						           group = postfix
						   }
						  #inet_listener lmtp {
						    # Avoid making LMTP visible for the entire internet
						    #address =
						    #port =
						  #}
						} 

						//Modificar parámetro  unix_listener a service_auth de la siguiente manera:

						service auth {
	unix_listener /var/spool/postfix/private/auth {
	mode = 0666
	user = postfix
	group = postfix
	}

  unix_listener auth-userdb {
	mode = 0600
	user = vmail
  #group =
  }

						  #unix_listener /var/spool/postfix/private/auth {
						  # mode = 0666
						  #}

						  user = dovecot
						}


						Modificar service auth-worker de la siguiente manera:

						service auth-worker {
						  # Auth worker process is run as root by default, so that it can access
						  # /etc/shadow. If this isn't necessary, the user should be changed to
						  # $default_internal_user.
						  user = vmail
						}


		//////////////////////////////////	

		Archivo de configuración SSL de dovecot 
				pico /etc/dovecot/conf.d/10-ssl.conf 
					ssl = required
					ssl_cert = </etc/apache2/ssl/8807847e49239183.crt
					ssl_key = </etc/apache2/ssl/forjadoresdelfuturo.key

			
		Configurando para pop3(este fichero se crea)
				pico /etc/dovecot/conf.d/20-pop3.conf
						  pop3_uidl_format = %08Xu%08Xv
						  pop3_client_workarounds = outlook-no-nuls oe-ns-eoh

			Reiniciar Dovecot

			service dovecot restart
			    
			sudo service postfix restart
			sudo service dovecot restart

			//sudo service sendmail stop


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////ROUNDCUBE////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


1- Instalar roundcube
	sudo apt-get install roundcube roundcube-mysql

2- Configurar Roundcube para su sistema:
    dpkg-reconfigure roundcube-core
	   			ssl://forjadoresdelfuturo.com.mx:993
	   			unix socket
	   			usuario roundcube: roundcube

3- Roundcube enlace a los archivos root de Apache:
	sudo ln -s /usr/share/roundcube/ /var/www/html/roundcube

 Reiniciar el servidor Apache:

 sudo service apache2 restart


 //aqui estan las configuraciones de roundcube
	pico  /etc/roundcube/db.inc.php 
	pico  /etc/roundcube/debian-db.php

 5-NO SE. Instalar Postfix y las librerías de utilidad de correo:

 //sudo apt-get install mailutils

---------------
  			





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////SPAMASSASSIN////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
		Instalar SpamAssassin.
			apt-get install spamassassin spamc
		Crear un usuario para SpamAssassin.
			adduser spamd --disabled-login

		Configurar SpamAssassin		
			pico /etc/default/spamassassin
				//Tenemos que cambiar el parámetro ENABLED para habilitar el demonio(daemon) SpamAssassin.
				ENABLED=1
				//configurar los parámetros home y options 
				SPAMD_HOME="/home/spamd/"
				OPTIONS="--create-prefs --max-children 5 --username spamd --helper-home-dir ${SPAMD_HOME} -s ${SPAMD_HOME}spamd.log" 
				//Entonces tenemos que especificar el parámetro PID_File de la siguiente manera:
				PIDFILE="${SPAMD_HOME}spamd.pid"
				//Por último, tenemos que especificar que las reglas(rules) de SpamAssassin se actualizarán automáticamente.
				CRON=1



		Establecer las reglas anti-spam.
			pico /etc/spamassassin/local.cf

				NOTA: SpamAssassin evaluará a cada correo y si determina que este correo electrónico es mayor que 5.0 en su comprobación de correo no deseado, entonces automáticamente se considerará spam. Usted podría utilizar los siguientes parámetros para configurar las reglas anti-spam:

					rewrite_header Subject ***** SPAM _SCORE_ *****
					report_safe             0
					required_score          5.0
					use_bayes               1
					use_bayes_rules         1
					bayes_auto_learn        1
					skip_rbl_checks         0
					use_razor2              0
					use_dcc                 0
					use_pyzor               0


		cambiar el archivo de Postfix /etc/postfix/master.cf para decirle que cada correo electrónico se comprobará con SpamAssassin.
			pico /etc/postfix/master.cf
				//Entonces tenemos que encontrar la línea siguiente y agregar el filtro SpamAssassin:
				smtp      inet  n       -       -       -       -       smtpd
				   -o content_filter=spamassassin


				//Por último hay que añadir los siguientes parámetros:(SON OBLIGATORIOS LOS ESPACIOS)
spamassassin unix -     n       n       -       -       pipe
    user=spamd argv=/usr/bin/spamc -f -e  
    /usr/sbin/sendmail -oi -f ${sender} ${recipient}

		

		Es necesario comenzar SpamAssassin y reiniciar Postfix para comenzar la verificación de spam desde los emails.
				service spamassassin start
				service postfix restart
				sudo service dovecot restart


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////Instalar OpenDKIM///////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

1- actualización del sistema	
	sudo apt-get update
	sudo apt-get upgrade

2- Instalar OpenDKIM y sus dependencias:
		sudo apt-get install opendkim opendkim-tools

3- Configuracion (Añadir al final)
	sudo pico /etc/opendkim.conf


		AutoRestart             Yes
		AutoRestartRate         10/1h
		UMask                   002
		Syslog                  yes
		SyslogSuccess           Yes
		LogWhy                  Yes

		Canonicalization        relaxed/simple

		ExternalIgnoreList      refile:/etc/opendkim/TrustedHosts
		InternalHosts           refile:/etc/opendkim/TrustedHosts
		KeyTable                refile:/etc/opendkim/KeyTable
		SigningTable            refile:/etc/opendkim/SigningTable

		Mode                    sv
		PidFile                 /var/run/opendkim/opendkim.pid
		SignatureAlgorithm      rsa-sha256

		UserID                  opendkim:opendkim

		Socket                  inet:12301@localhost



4- Conectar el milter a Postfix:
		sudo pico /etc/default/opendkim
			SOCKET="inet:12301@localhost"

5- Configurar postfix para utilizar este milter:
	sudo pico /etc/postfix/main.cf			
	    milter_protocol = 2
		milter_default_action = accept
				//smtpd_milters = unix:/spamass/spamass.sock, inet:localhost:12301
				//non_smtpd_milters = unix:/spamass/spamass.sock, inet:localhost:12301
		//Si los parámetros faltan, definirlos como sigue:
		smtpd_milters = inet:localhost:12301
		non_smtpd_milters = inet:localhost:12301

6- 	Crear una estructura de directorio que contendrá los hosts de confianza( trusted hosts), key tables, signing tables y crypto keys:
		sudo mkdir /etc/opendkim
		sudo mkdir /etc/opendkim/keys
		
		//Especificar los hosts de confianza(trusted hosts):
		sudo pico /etc/opendkim/TrustedHosts		

			127.0.0.1
			localhost
			192.168.0.1/24
			*.forjadoresdelfuturo.com.mx


		//Crear una tabla de llaves(key table):
			sudo pico /etc/opendkim/KeyTable		
				mail._domainkey.forjadoresdelfuturo.com.mx forjadoresdelfuturo.com.mx:mail:/etc/opendkim/keys/forjadoresdelfuturo.com.mx/mail.private
				

		//Crear una tabla de firma(signing table):
			sudo pico /etc/opendkim/SigningTable	
				*@forjadoresdelfuturo.com.mx mail._domainkey.forjadoresdelfuturo.com.mx

		//Generar las llaves public y private 
			cd /etc/opendkim/keys
			sudo mkdir forjadoresdelfuturo.com.mx
			cd forjadoresdelfuturo.com.mx
		//Generar las keys:
			sudo opendkim-genkey -s mail -d forjadoresdelfuturo.com.mx

		//Cambiar el propietario de la clave privada a opendkim:
				sudo chown opendkim:opendkim mail.private

7-
	Añadir la clave pública a los registros DNS del dominio

sudo pico -$ mail.txt


mail._domainkey IN      TXT     ( "v=DKIM1; k=rsa; "
          "p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQChY+wvC6vu2NGTprORTK64EbGwO33YUpZ2xRhn7ATEcMyt9vK9sUdHoav8KpWqE5P4n2r6qJ45bj+PleCqWuHbUr9T6O3L2fZuwZazrtwpoIoyAEFJ
/o4RvGJLVuGQv98NhwmJvsXCsKWmVZbQ2t/ZLTv0LkUrf8xzFjPLk4rJewIDAQAB" )  ; ----- DKIM key mail for forjadoresdelfuturo.com.mx


https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-dkim-with-postfix-on-debian-wheezy
--
1- Registro TXT
hostname: 
		mail._domainkey.forjadoresdelfuturo.com.mx.

value:

"v=DKIM1; k=rsa; "
          "p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQChY+wvC6vu2NGTprORTK64EbGwO33YUpZ2xRhn7ATEcMyt9vK9sUdHoav8KpWqE5P4n2r6qJ45bj+PleCqWuHbUr9T6O3L2fZuwZazrtwpoIoyAEFJ
/o4RvGJLVuGQv98NhwmJvsXCsKWmVZbQ2t/ZLTv0LkUrf8xzFjPLk4rJewIDAQAB"

2- Registro TXT
	hostname: 
	  @
	value:
	  v=spf1 a mx include:ne16.com ~all	  

---

mail._domainkey IN TXT "v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC5N3lnvvrYgPCRSoqn+awTpE+iGYcKBPpo8HHbcFfCIIV10Hwo4PhCoGZSaKVHOjDm4yefKXhQjM7iKzEPuBatE7O47hAx1CJpNuIdLxhILSbEmbMxJrJAG0HZVn8z6EAoOHZNaPHmK2h4UUrjOG8zA5BHfzJf7tGwI+K619fFUwIDAQAB" ; ----- DKIM key mail for example.com
Copiar esa clave y añadir un registro TXT a las entradas de DNS de su dominio:
Name: mail._domainkey.example.com.

	b=bRuVKbPTSpzxTuc/3SlZ8AJ3SBK4a5KqT8LyeoPLM4SqehzJihrrtiU38gfy6GSwk
o0PEAIDWHNtrWQBnq1Eflkk20Lbmz8YbOedVUSVeXxN7R3LWrHJ2Y/+B5LmMex0mP4
fUZzZNtFXcp/qsyhcjCLhnoiKyx5BHwpLuqlQ6gM=




enviar a pana la llave para que la ponga en el registro txt

sudo service postfix restart
sudo service opendkim restart




La configuración se puede probar mediante el envío de un correo en blanco a 
	 check-auth@verifier.port25.com
	 Y recibire una respuesta. Si todo funciona correctamente debería ver "DKIM check: pass" y debajo un  "Summary of Results".
==========================================================
Summary of Results
==========================================================
SPF check:          pass
DomainKeys check:   neutral
DKIM check:         pass
Sender-ID check:    pass
SpamAssassin check: ham

Alternativamente, puede enviar un mensaje a una dirección de Gmail que permite controlar, ver los encabezados del correo electrónico recibido en su bandeja de entrada de Gmail, dkim=pass debe estar presente en el campo de cabecera Authentication-Results.
Authentication-Results: mx.google.com;
       spf=pass (google.com: domain of contact@example.com designates --- as permitted sender) smtp.mail=contact@example.com;
       dkim=pass header.i=@example.com;


-------------
https://www.mail-tester.com/

http://dkimcore.org/c/keycheck
 selector: mail
 Domain: forjadoresdelfuturo.com.mx

	v=DKIM1; k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQChY+wvC6vu2NGTprORTK64EbGwO33YUpZ2xRhn7ATEcMyt9vK9sUdHoav8KpWqE5P4n2r6qJ45bj+PleCqWuHbUr9T6O3L2fZuwZazrtwpoIoyAEFJ/o4RvGJLVuGQv98NhwmJvsXCsKWmVZbQ2t/ZLTv0LkUrf8xzFjPLk4rJewIDAQAB

	"v=DKIM1; k=rsa; "           "p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQChY+wvC6vu2NGTprORTK64EbGwO33YUpZ2xRhn7ATEcMyt9vK9sUdHoav8KpWqE5P4n2r6qJ45bj+PleCqWuHbUr9T6O3L2fZuwZazrtwpoIoyAEFJ/o4RvGJLVuGQv98NhwmJvsXCsKWmVZbQ2t/ZLTv0LkUrf8xzFjPLk4rJewIDAQAB"


enviar un correo y clickear en view your report y esperar 3 minutos
	http://www.isnotspam.com/
		



https://www.mail-tester.com/spf-dkim-check


SPF permite a los administradores especificar cuales hosts están autorizados a enviar correo en nombre de un dominio dado mediante la creación de un registro SPF específico (o registro TXT) en el Sistema de Nombres de Dominio (DNS). 

"v=spf1 mx a ip4:forjadoresdelfuturo.com.mx ~all"
"v=spf1 mx a ~all"
"v=spf1 a mx include:ne16.com ~all"	

"v=spf1 a mx include:forjadoresdelfuturo.com.mx ~all"

 "v=spf1 include:_spf.google.com ~all"
  v=spf1 include:_spf.google.com ~all


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////SPF/////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////otras configuraciones/////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

para checar errores
pico /var/log/syslog



Checar este

		pico /etc/hosts
			127.0.0.1 localhost
			127.0.1.1 forjadoresdelfuturo.com.mx  forjadoresdelfuturo
			127.0.1.1 mail.forjadoresdelfuturo.com.mx  pepsiproduccion

		pico /etc/aliases
			# See man 5 aliases for format
			postmaster:    osmel
	
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	Comprobar que los siguientes puertos estan funcionando
			nmap -sT -O localhost
				PORT     STATE SERVICE
				22/tcp   open  ssh
				25/tcp   open  smtp
				80/tcp   open  http
				110/tcp  open  pop3
				443/tcp  open  https
				587/tcp  open  submission
				783/tcp  open  spamassassin
				993/tcp  open  imaps
				995/tcp  open  pop3s
				3306/tcp open  mysql

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	Este es para enviar un mail
			echo "Hola esta es una prueba" | mail -s "el subjeto de prueba" osmel.calderon@gmail.com

			telnet localhost 587

			sudo service sendmail stop
			sudo service postfix restart
			sudo service dovecot restart

			sudo shutdown -r now
			//sudo apt-get install sendmail


			fecha
			 date --set "2017-04-09 20:08"
			 pico .profile 


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
pico /etc/passwd

pico /etc/default/spamassassin
pico /etc/spamassassin/local.cf
pico /etc/postfix/master.cf
sudo service spamassassin start
sudo service postfix restart
sudo service dovecot restart




/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//cuando cree algun usuario 
	ls -lha /var/mail/
	sudo touch /var/mail/osmel
	sudo chown osmel:vmail /var/mail/osmel
	sudo chmod o-r /var/mail/osmel
	sudo chmod g+rw /var/mail/osmel



.htaccess para poner el wwww

RewriteEngine On
RewriteBase /

RewriteCond %{HTTP_HOST} !^www\.
RewriteRule ^(.*)$ https://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

#Now, rewrite to HTTPS:
RewriteCond %{HTTPS} off
RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
