tsc --target ES2016 --experimentalDecorators


https://www.youtube.com/watch?v=DIz4LmDX7ks
https://www.youtube.com/watch?v=Pdi3oAS9Xtw
https://www.youtube.com/watch?v=3Lh5XZXXsFI
https://www.youtube.com/watch?v=HjuRyTFdQiE
https://www.youtube.com/watch?v=ifI4RIQlje4
https://www.youtube.com/watch?v=rlvRnZkggS4
https://www.youtube.com/watch?v=lpVr12sUBLg&list=PLEtcGQaT56ch63VgSUCOle6QNBqK8ewvd&index=16

ayuda oficial
https://code.i-harness.com/es/docs/typescript/handbook/decorators
http://www.typescriptlang.org/docs/handbook/decorators.html
https://www.typescriptlang.org/docs/handbook/decorators.html
https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Object/defineProperty

online
http://www.typescriptlang.org/play/index.html



tutoriales buenos
https://code.i-harness.com/es/docs/typescript/handbook/decorators
https://www.mmfilesi.com/blog/typescript-y-7-decorators/
http://blog.builtbyedgar.com/decoradores-en-javascript-con-es7-es2016/
https://es.survivejs.com/react/appendices/understanding-decorators/
https://luisjordan.net/angular/decoradores-angular-cosas-necesarias-debemos-conocer/


  
  
  
  


ingles
https://dev.to/scleriot/typescript-method-decorators-example-1imc
https://www.spectory.com/blog/A%20deep%20dive%20into%20TypeScript%20decorators



otras
https://carlosvillu.com/decoradores-de-clases-y-propiedades-en-es7/
http://blog.enriqueoriol.com/2018/02/decorador-analytics.html
http://blog.enriqueoriol.com/2016/06/angular2-aprendo-es6-o-typescript.html
https://www.campusmvp.es/recursos/post/JavaScript-ECMAScript-ES6-Existe-ES7-Aclarando-las-diferentes-versiones-del-lenguaje.aspx
https://desarrolloweb.com/articulos/decorador-componentes-angular2.html
https://medium.com/google-developers/exploring-es7-decorators-76ecb65fb841
https://toddmotto.com/angular-decorators

https://www.spectory.com/blog/A%20deep%20dive%20into%20TypeScript%20decorators
https://www.campusmvp.es/recursos/post/JavaScript-ECMAScript-ES6-Existe-ES7-Aclarando-las-diferentes-versiones-del-lenguaje.aspx



*/


            /*
            var ES51    = Modernizr.es5,
                ES6     = Modernizr.es6object;
                ES7     = false;
            */

/*
var ES1     = !!(Array.prototype && Array.prototype.join),
        ES3     = !!(Array.prototype && Array.prototype.pop),
        ES51    = (function() {'use strict'; return !this; })(),
        ES6     = !!Object.assign,
        ES7     = !!(Array.prototype && Array.prototype.includes);

        if(ES7){
            alert('Tu navegador contiene características mínimas de ECMAScript 7');
        }else if(ES6){
            alert('Tu navegador contiene características mínimas de ECMAScript 6');
        }else if(ES51){
            alert('Tu navegador tiene soporte para ECMAScript 5.1');
        }else if(ES3){
            alert('Tu navegador contiene características de ECMAScript 3');
        }else if(ES1){
            alert('Tu navegador contiene características de ECMAScript 1');
        }else{
            alert('Tu navegador contiene características de ECMAScript desconocida');
        }
*/



Como esta en fase de experimentación, hay que habilitar
  "experimentalDecorators": true, 


Qué es un decorador
        Son un modo de añadir metadatos a declaraciones de clases para hacer usados por inyecciones de dependencias o compilaciones de directivas.
        Al crear decoradores lo que estamos es definiendo anotaciones especiales, que pueden tener un impacto en el modo en el que se comportan nuestras clases, o los metodos o funciones que incluyan, pueden tambien simplemente alterar los datos que hemos definido en campos o parametros de esas clases. 
        Por tanto podemos considerar los decoradores como un modo muy poderoso de aumentar las funcionalidades nativas de nuestros tipos, sin tener para ellos que crear sus clases o que tengan que heredar de otros tipos

        Son reconocidos porque comienzan por una "@" y a continuación tienen un nombre. Localizados como declaraciones independientes que se colocan sobre el elemento que van a decorar(clases, métodos, accessor, propiedades y parámetros), incluyendo dentro del decorador un metodo o no. 


        Podemos definir 4 tipos diferentes de decoradores, dependiendo del tipo de elemento que esten destinados a decorar:
         - clases
         - propiedades 
         - métodos
         - parámetros
         - accesorios (metodos get y set)




    Básicamente es una implementación de un patrón de diseño de software que en sí sirve para extender una función mediante otra función, pero sin tocar aquella original, que se está extendiendo. El decorador recibe una función como argumento (aquella que se quiere decorar) y devuelve esa función con alguna funcionalidad adicional.

    En esencia vienen a ser una serie de metadatos adicionales que se pueden añadir a clases, métodos, propiedades y parámetros para modificar su comportamiento. 

    Las funciones decoradoras comienzan por una "@" y a continuación tienen un nombre. Ese nombre es el de aquello que queramos decorar, que ya tiene que existir previamente. Podríamos decorar una función, una propiedad de una clase, una clase, etc.


    
