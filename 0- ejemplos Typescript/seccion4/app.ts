/*- Objetos básicos
- Crear objetos con tipos específicos
- Crear métodos dentro de objetos
- Tipos personalizados
- Crear variables que soporten varios tipos a la vez.
- Comprobar el tipo de un objeto.

http://www.typescriptlang.org/docs/handbook/classes.html

  */


//definicion de objeto, pero en este caso no se puede especificar los tipos, se inicializan directamente el objeto con sus valores
		 let persona = {  
				nombre:"osmel", 
		 		edad:41, 
		 		hijos:["duvi", "alex", "fidel"], 
		 };

		 persona.nombre = "osmel calderon";
		 console.log(persona);


//para especificar los tipos de cada dato que tiene el objeto se especifica de esta manera

		let perTipo: {   //primero especificamos los tipos
					nombre:string, 
					edad:number, 
					hijos:string[], 
					
					getNombre:()=>string   //definiendo una funcion

			} = {   //segundo inicializamos los valores
				    nombre: "osmel",
				    edad: 41,
				    hijos: ["duvi", "alex", "fidel"],

				    getNombre(){    	//inicializando una funcion
				    	return this.nombre;
				    }
		}

		console.log(perTipo.hijos);
		perTipo.nombre = "osmel calderon";

		console.log( perTipo.getNombre()  );





//type: Nos permite crear tipos personalizados de objeto, para poderlo reutilizar en todos los objetos que se requieran

		type miObjeto= { 
		    nombre:string, 
		 	edad:number, 
		 	hijos:string[], 
		 	getNombre:()=>string 
		 };

		let miotravariable: miObjeto = {
		    nombre: "osmel",
		    edad: 41,
		    hijos: ["duvi", "alex", "fidel"],
		    
		    getNombre(){
		    	return this.nombre;
		    }
		}




/////////////un caso especial en el que se quiera definir una variable de varios tipos

		let loquesea2:any = "osmel";
		    loquesea2 = 2;
		    console.log(loquesea2);


		//con las tuberias podemos tambien restringirlos a varios tipos
		let numero_cadena_booleano2: number | string | miObjeto = 2;
		    
		    numero_cadena_booleano2 = "como texto";  //como texto
		    
		    numero_cadena_booleano2 = {  //como objeto
					    		nombre: "osmel",
							    edad: 41,
							    hijos: ["duvi", "alex", "fidel"],
							    getNombre(){
							    	return this.nombre;
							    }
			}

      //numero_cadena_booleano2 = true;  //esto sería un error porque no es any tiene restricciones a los tipos


