/*- Objetos básicos
- Crear objetos con tipos específicos
- Crear métodos dentro de objetos
- Tipos personalizados
- Crear variables que soporten varios tipos a la vez.
- Comprobar el tipo de un objeto.

http://www.typescriptlang.org/docs/handbook/classes.html

  */
//definicion de objeto, pero en este caso no se puede especificar los tipos, se inicializan directamente el objeto con sus valores
var persona = {
    nombre: "osmel",
    edad: 41,
    hijos: ["duvi", "alex", "fidel"],
};
persona.nombre = "osmel calderon";
console.log(persona);
//para especificar los tipos de cada dato que tiene el objeto se especifica de esta manera
var perTipo = {
    nombre: "osmel",
    edad: 41,
    hijos: ["duvi", "alex", "fidel"],
    getNombre: function () {
        return this.nombre;
    }
};
console.log(perTipo.hijos);
perTipo.nombre = "osmel calderon";
console.log(perTipo.getNombre());
var miotravariable = {
    nombre: "osmel",
    edad: 41,
    hijos: ["duvi", "alex", "fidel"],
    getNombre: function () {
        return this.nombre;
    }
};
/////////////un caso especial en el que se quiera definir una variable de varios tipos
var loquesea2 = "osmel";
loquesea2 = 2;
console.log(loquesea2);
//con las tuberias podemos tambien restringirlos a varios tipos
var numero_cadena_booleano2 = 2;
numero_cadena_booleano2 = "como texto"; //como texto
numero_cadena_booleano2 = {
    nombre: "osmel",
    edad: 41,
    hijos: ["duvi", "alex", "fidel"],
    getNombre: function () {
        return this.nombre;
    }
};
//numero_cadena_booleano2 = true;  //esto sería un error porque no es any tiene restricciones a los tipos
