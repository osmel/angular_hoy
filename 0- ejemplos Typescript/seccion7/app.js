/*
Crear clases en TypeScript
Constructores

Accesibilidad de las propiedades: (controla el lugar donde puede ser accesada una propiedad)
    Públicas: pueden ser accesada desde cualquier lugar
    Privadas : solo puede ser cambiada desde la misma clase
    Protegidas: solo puede ser accesada desde la misma clase o sus clase que la hereda.

Métodos de las clases que pueden ser: (controla el lugar donde puede ser accesada un metodo)
    Públicos
    Privados
    Protegidos
Herencia
Llamar funciones del padre, desde los hijos
Getters
Setters
Métodos y propiedades estáticas
Clases abstractas
Constructores privados.


Public: (por defecto). Sino se le especifica un ambito a  una variable. Por defecto será public. Puede ser accesado desde cualquier parte del programa. Es decir donde la clase esta instanciada, puedo accesar y cambiar los valores
protected: Solo puede ser accesada dentro de su propia clase, o dentro de sus subclases(herencia). Es decir dentro del constructor o sus funciones de esta clase o sus herederas
private: Solo puede ser accesada dentro de su propia clase. Es decir dentro del constructor o sus funciones de esta clase.

Los metodos tambien tienen un  ambito de public, protected o private. Que significa que es el alcance de este dentro de una APP
*/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*clases*/
var Persona = /** @class */ (function () {
    //constructor de la clase. Es ejecutada cuando se crea una nueva instancia.
    function Persona(nomb) {
        this.hijos = ["duvi", "alex", "fidel"]; //solo puede ser cambiada desde la misma clase
        this.nombre = nomb;
    }
    ;
    Object.defineProperty(Persona.prototype, "miEdad", {
        /*
        Get y sets, es una forma de accesar a propiedades pero de una forma controlada, es decir q no sean publicas. Buenas practicas
        - En este caso son funciones especiales
            -     get(publico): "get": aunque le declares que regresa void, TS espera que regrese algo, por tanto no admite void.
            -  setter(publico): "set": Es obligatorio pasar un parametro, Y esta generalmente no retorna nada, no se especifica void
        */
        /* forma antigua
        public  getEdad():number {
            return this.edad;
        };

        public  setEdad(edad):void {
            this.edad=edad;
        };
        */
        get: function () {
            return this.edad;
        },
        set: function (edad2) {
            this.edad = edad2;
        },
        enumerable: true,
        configurable: true
    });
    Persona.prototype.getHijos = function () {
        this.cambiar(); // haciendo referencia a un metodo privado, solo puede ser accesado desde la misma clase
        return "Hola mis hijos son: " + this.hijos;
    };
    ;
    Persona.prototype.cambiar = function () {
        return "cambio ";
    };
    ;
    Persona.prototype.protegida = function () {
        return "cambio ";
    };
    ;
    return Persona;
}());
var minombre = new Persona("osmel");
minombre.nombre = "otros"; //es la que puede ser cambiada, las protegidas y las privadas no
console.log(minombre.nombre);
console.log(minombre.getHijos()); //un get de forma antigua, ahora solo se usa la palabra reservada get, sin declarar ambito, porque siempre serán publico
minombre.miEdad = 42; //error poner minombre.miEdad(42 COMO ARGUMENTO)
console.log(minombre.miEdad);
/*herencia*/
var Alumno = /** @class */ (function (_super) {
    __extends(Alumno, _super);
    function Alumno() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Alumno;
}(Persona));
var Trabajador = /** @class */ (function (_super) {
    __extends(Trabajador, _super);
    function Trabajador() {
        var _this = _super.call(this, "minombre") || this;
        _super.prototype.protegida.call(_this); //esta es la forma de llamar a una funcion del padre que esta protegida.
        return _this;
    }
    return Trabajador;
}(Persona));
var miAlumno = new Alumno("osmel"); //Cuando la clase hija no define un constructor, entonces toma por defecto el constructor de la clase padree
var miTrabajo = new Trabajador(); //esta clase, se encarga de llamar a la padre
console.log(miTrabajo);
/*

type miObjeto= {
    nombre:string,
    edad:number,
 };




//////////////////////////////////


//////////////////////////////////
Los metodos y propiedades estaticas: Son propias de la clase. No se necesita instanciar la clase para usarla
Se usa de esta manera directamente
    clase.propiedad;
    clase.metodo();


    static: Para crear métodos de clase y variables de clase
    Métodos de clase:
     - Tienen acceso solo a los atributos de clase.
     - No es necesario crear un objeto para poder utilizar estos métodos.
     - Para acceder a estos métodos se invoca de la siguiente manera:  Clase.metodo;
    Atributos de clase: Estos solo reservan un espacio de memoria para la clase. Son variables de la clase, no necesitan que los objetos sean creados para que exista.


        class Persona {
            static cantidad: number=0;
            
            constructor() {
              
            };

            static cantidades() {
                this.cantidad++;
                return this.cantidad;
            }
             
        }

        console.log( Persona.cantidades() );
        console.log( Persona.cantidades() );

//////////////////////////////////Clases Abstractas

Estas clases no se pueden instanciar. Son clases abstractas donde se definen propiedades y metodos abstractos,
 que serviran para sus herederos


             Las clases abstractas:
                 Tiene directrices:
             -Si en una clase hay un método abstracto, la clase esta obligada a ser declarada abstracta
             -Todos sus métodos no tienen que ser abstractos.
            - Las subclases que heredan de una clase abstracta, estan obligada a redefinir e implementar los métodos abstractos.
            - Los métodos abstractos no llevan nada en su interior solo se definen
              

              Ahora, el ejemplo más claro. Para que se usan?
*/
var Animal = /** @class */ (function () {
    function Animal() {
    }
    return Animal;
}());
var gato = /** @class */ (function (_super) {
    __extends(gato, _super);
    function gato() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    gato.prototype.ruido = function () {
        console.log("Maulla");
    };
    return gato;
}(Animal));
var perro = /** @class */ (function (_super) {
    __extends(perro, _super);
    function perro() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    perro.prototype.ruido = function () {
        console.log("Ladra");
    };
    return perro;
}(Animal));
var caballo = /** @class */ (function (_super) {
    __extends(caballo, _super);
    function caballo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    caballo.prototype.ruido = function () {
        console.log("Ruge");
    };
    return caballo;
}(Animal));
var persona = /** @class */ (function (_super) {
    __extends(persona, _super);
    function persona() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    persona.prototype.ruido = function () {
        console.log("Habla");
    };
    return persona;
}(Animal));
//1- Declaración de un array de objeto de tipo Animal
var miAnimal = new Array(3); //solo crea una referencia a un tipo abstracto
//referencia a diferentes tipos de objetos que son subclases de Animal
//2- Principio de sustitución
//Estamos almacenando un objeto de la subclase, cuando el programa espera que se almacene un objeto de la superclase
miAnimal[0] = new perro();
miAnimal[1] = new gato();
miAnimal[2] = new caballo();
//3- casting o refundicion de objetos:
//Es la forma que tenemos de usar métodos de la subclase, cuando estamos en presencia de una variable de la superClase. Ejemplo:
miAnimal[0].ruido(); //casting
//Typescript es capaz de saber en tiempo de ejecución a que método llamar al de la clase padre o hijo. Cuando hay metodos redefinidos
for (var _i = 0, miAnimal_1 = miAnimal; _i < miAnimal_1.length; _i++) {
    var variableAuxiliar = miAnimal_1[_i];
    variableAuxiliar.ruido();
}
// singleton (constructores privados)
var Singleton = /** @class */ (function () {
    function Singleton() {
        // 
    }
    Singleton.getInstance = function () {
        if (!Singleton.instance) { //si la clase no se ha creado nunca pues entonces que se cree
            Singleton.instance = new Singleton();
            // ... cualquier iniciación va aquí
            console.log('iniciando');
        }
        return Singleton.instance;
    };
    Singleton.prototype.someMethod = function () { };
    return Singleton;
}());
//let something = new Singleton() // Esto dará Error: el constructor es privado.
//solo se podrá iniciar una sola vez
var instance = Singleton.getInstance(); // Se instanciará solo de esta manera porque es un metodo estatico de la clase.
