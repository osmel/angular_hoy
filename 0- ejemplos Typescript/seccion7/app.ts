/*
Crear clases en TypeScript
Constructores

Accesibilidad de las propiedades: (controla el lugar donde puede ser accesada una propiedad)
	Públicas: pueden ser accesada desde cualquier lugar
	Privadas : solo puede ser cambiada desde la misma clase
	Protegidas: solo puede ser accesada desde la misma clase o sus clase que la hereda.

Métodos de las clases que pueden ser: (controla el lugar donde puede ser accesada un metodo)
	Públicos
	Privados
	Protegidos
Herencia
Llamar funciones del padre, desde los hijos
Getters 
Setters
Métodos y propiedades estáticas
Clases abstractas
Constructores privados.


Public: (por defecto). Sino se le especifica un ambito a  una variable. Por defecto será public. Puede ser accesado desde cualquier parte del programa. Es decir donde la clase esta instanciada, puedo accesar y cambiar los valores 
protected: Solo puede ser accesada dentro de su propia clase, o dentro de sus subclases(herencia). Es decir dentro del constructor o sus funciones de esta clase o sus herederas
private: Solo puede ser accesada dentro de su propia clase. Es decir dentro del constructor o sus funciones de esta clase.

Los metodos tambien tienen un  ambito de public, protected o private. Que significa que es el alcance de este dentro de una APP
*/


/*clases*/
			class Persona {
				//definiendo las propiedades
			    public nombre: string;   //publico
			    protected edad: number; //=30; //solo puede ser accesada desde la misma clase o sus clase que la hereda.
			    private hijos:string[] = ["duvi", "alex", "fidel"]; //solo puede ser cambiada desde la misma clase
			    
			    //constructor de la clase. Es ejecutada cuando se crea una nueva instancia.
			    constructor(nomb: string) {
			        this.nombre = nomb;
			    };

			/*
			Get y sets, es una forma de accesar a propiedades pero de una forma controlada, es decir q no sean publicas. Buenas practicas
   			- En este caso son funciones especiales
		   	 	-     get(publico): "get": aunque le declares que regresa void, TS espera que regrese algo, por tanto no admite void.
		 		-  setter(publico): "set": Es obligatorio pasar un parametro, Y esta generalmente no retorna nada, no se especifica void
			*/


			    /* forma antigua
			    public  getEdad():number {
			        return this.edad;
			    }; 

			    public  setEdad(edad):void {
			        this.edad=edad;
			    };
			    */ 

			    get miEdad():number {  //tiene que retornar un valor del tipo de la función
			        return this.edad;
			    }

			    set  miEdad(edad2:number) { //tiene que tener parametro de forma obligatoria, y no retorna nada, no se pone void
			        this.edad=edad2;
			    }

			    

			   public  getHijos() {
			   	    this.cambiar(); // haciendo referencia a un metodo privado, solo puede ser accesado desde la misma clase
			        return "Hola mis hijos son: " + this.hijos;
			   };


			   private  cambiar() {
			        return "cambio ";
			    }; 

			   protected  protegida() {
			        return "cambio ";
			   }; 
			}

			let minombre = new Persona("osmel");
			minombre.nombre ="otros"; //es la que puede ser cambiada, las protegidas y las privadas no
			console.log(minombre.nombre);
			console.log( minombre.getHijos() ); //un get de forma antigua, ahora solo se usa la palabra reservada get, sin declarar ambito, porque siempre serán publico

			minombre.miEdad = 42; //error poner minombre.miEdad(42 COMO ARGUMENTO)
			
			console.log( minombre.miEdad );



/*herencia*/

			class Alumno extends Persona { //herencia. Cuando la clase hija no define un constructor, 
											//entonces toma por defecto el constructor de la clase padree
			}


			class Trabajador extends Persona { 
				constructor(){
					super("minombre"); //los constructores de clases heredadas, deben tener un llamado super(para poder llamaar al constructor padre)
					super.protegida(); //esta es la forma de llamar a una funcion del padre que esta protegida.
				}
			}


			
			let miAlumno = new Alumno("osmel"); //Cuando la clase hija no define un constructor, entonces toma por defecto el constructor de la clase padree
			let miTrabajo = new Trabajador(); //esta clase, se encarga de llamar a la padre

			console.log(miTrabajo);


/*

type miObjeto= {  
	nombre:string, 
 	edad:number, 
 };




//////////////////////////////////


//////////////////////////////////
Los metodos y propiedades estaticas: Son propias de la clase. No se necesita instanciar la clase para usarla
Se usa de esta manera directamente
	clase.propiedad;
	clase.metodo();


	static: Para crear métodos de clase y variables de clase
	Métodos de clase:
	 - Tienen acceso solo a los atributos de clase.
	 - No es necesario crear un objeto para poder utilizar estos métodos.
	 - Para acceder a estos métodos se invoca de la siguiente manera:  Clase.metodo;
	Atributos de clase: Estos solo reservan un espacio de memoria para la clase. Son variables de la clase, no necesitan que los objetos sean creados para que exista.


		class Persona {
		    static cantidad: number=0;
		    
		    constructor() {
		      
		    };

		    static cantidades() {
		    	this.cantidad++;
		    	return this.cantidad;
		    }
		     
		}

		console.log( Persona.cantidades() );
		console.log( Persona.cantidades() );

//////////////////////////////////Clases Abstractas

Estas clases no se pueden instanciar. Son clases abstractas donde se definen propiedades y metodos abstractos,
 que serviran para sus herederos


			 Las clases abstractas:
			 	 Tiene directrices:
			 -Si en una clase hay un método abstracto, la clase esta obligada a ser declarada abstracta
			 -Todos sus métodos no tienen que ser abstractos.
			- Las subclases que heredan de una clase abstracta, estan obligada a redefinir e implementar los métodos abstractos.
			- Los métodos abstractos no llevan nada en su interior solo se definen
			  

			  Ahora, el ejemplo más claro. Para que se usan?
*/			       
			abstract class Animal {
			    abstract  ruido():void;
			}

			class gato extends Animal {
				ruido():void{
					console.log("Maulla");
				}
			}


			class perro extends Animal {
				ruido():void{
					console.log("Ladra");
				}
			}

			class caballo extends Animal {
				ruido():void{
					console.log("Ruge");
				}
			}


			class persona extends Animal {
				ruido():void{
					console.log("Habla");
				}
			}

			//1- Declaración de un array de objeto de tipo Animal
			     let miAnimal:Animal[]= new Array(3); //solo crea una referencia a un tipo abstracto

					//referencia a diferentes tipos de objetos que son subclases de Animal
					//2- Principio de sustitución
			 		//Estamos almacenando un objeto de la subclase, cuando el programa espera que se almacene un objeto de la superclase

			     	miAnimal[0]= new perro();
			     	miAnimal[1]= new gato();
			     	miAnimal[2]= new caballo();

			//3- casting o refundicion de objetos:
			//Es la forma que tenemos de usar métodos de la subclase, cuando estamos en presencia de una variable de la superClase. Ejemplo:

			     (miAnimal[0] as Animal).ruido(); //casting


			  //Typescript es capaz de saber en tiempo de ejecución a que método llamar al de la clase padre o hijo. Cuando hay metodos redefinidos
			  for (let variableAuxiliar of miAnimal) {
				variableAuxiliar.ruido();
			  }




 // singleton (constructores privados)
 
class Singleton {
    private static instance: Singleton;
    private constructor() {
        // 
    }
    static getInstance() {
        if (!Singleton.instance) {  //si la clase no se ha creado nunca pues entonces que se cree
            Singleton.instance = new Singleton();
            // ... cualquier iniciación va aquí
            console.log('iniciando');
        }
        return Singleton.instance;
    }
    someMethod() { }
}

//let something = new Singleton() // Esto dará Error: el constructor es privado.
  //solo se podrá iniciar una sola vez
let instance = Singleton.getInstance(); // Se instanciará solo de esta manera porque es un metodo estatico de la clase.
