var validaciones;
(function (validaciones) {
    function validarfecha(fecha) {
        if (isNaN(fecha.valueOf())) { //si la fecha no es valida
            return false;
        }
        else {
            return true;
        }
    }
    validaciones.validarfecha = validarfecha;
})(validaciones || (validaciones = {}));
