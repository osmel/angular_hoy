/*
	
	https://www.mmfilesi.com/blog/typescript-5-namespaces/
	https://code.i-harness.com/es/docs/typescript/handbook/namespaces-and-modules

	 * agrupar y encapsular partes del código con la misma funcionalidad. en un espacio de nombre especifico 
	 * para que esté disponible desde fuera algo dentro del namespace hay que "exportarlo".
	 * Diferencia de los namespaces vs  módulos es que en el namespace no necesitamos que las cosas estén agrupadas en el mismo sitio. 
	   -de esta manera podemos separar clases relacionadas en distintos archivos,  lo cual es mucho más elegante y ordenado.
	   - para que esto funcione, debemos añadir un comentario antes de la declaración del namespace, 
	     en el cual indicamos la ruta (path) donde se encuentra el namespace principal. 
	     (En realidad no es un comentario, sino lo que denominan triple-slash directives,
	        /// <reference path="app.ts" />


 

	 //  namespace en un simple archivo
          <script src="app.js"></script>  

      namespace en multiples archivos. Podemos hacerlo de diferentes forma
      1-  llamarlos todos desde el index
	      <script src="validaciones/fecha.js"></script> 
	      <script src="validaciones/texto.js"></script> 
	      <script src="multiples.js"></script> 
	  
	  2- unir todos los ficheros en uno solo "multiples.js"
          tsc --outFile multiples.js validaciones/fecha validaciones/texto multiples

	  3- 	Importarlos todos en el fichero multiples.ts mediante las clausula /// <reference path="fichero.ts" />
			/// <reference path="validaciones/fecha.ts" />
			/// <reference path="validaciones/texto.ts" />
			
			y finalmente ejecutar      
			  tsc --outFile multiples.js multiples



*/

/////////interfaz con tipo dentro una función en un mismo archivo
			namespace validaciones32 {

				export function validartexto2(texto:string):boolean{
					if (texto.length>3) {
						return true;
					} else {
						return false;
					}
				}

				export function validarfecha2(fecha:Date):boolean{  //export para poder accesar desde afuera del namespace
					if ( isNaN(fecha.valueOf()) ) {  //si la fecha no es valida
						return false;
					} else {
						return true;
					}
				}

				/*
				let hoy = new Date();
				console.log(
					 validarfecha(hoy)
				);*/

			}	

			let hoy = new Date();
			console.log(
					 validaciones32.validarfecha2(hoy)
				);
