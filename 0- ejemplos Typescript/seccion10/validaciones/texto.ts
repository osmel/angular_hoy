class otraCLase3 {
	    prueba1(): string{
	        return 'prueba1';
	    }    
	    prueba2(): string{
	        return 'prueba2';
	    }    

}

export  class otraCLase4 {  //default cuando necesites que sea una unica cosa q vaya a exportar y que quieres q sea el por defecto
	    prueba3(): void{
	        console.log('prueba3');
	    }    
	    prueba4(): string{
	        return 'prueba4';
	    }    

}


/* O bien exportamos todo lo que hay en el módulo */
//export *   from './texto';