"use strict";
exports.__esModule = true;
var otraCLase5 = /** @class */ (function () {
    function otraCLase5() {
    }
    otraCLase5.prototype.prueba1 = function () {
        return 'prueba1';
    };
    otraCLase5.prototype.prueba2 = function () {
        return 'prueba2';
    };
    return otraCLase5;
}());
var otraCLase6 = /** @class */ (function () {
    function otraCLase6() {
    }
    otraCLase6.prototype.prueba3 = function () {
        console.log('mi clase 6');
    };
    otraCLase6.prototype.prueba4 = function () {
        return 'prueba4';
    };
    return otraCLase6;
}());
exports["default"] = otraCLase6;
/* O bien exportamos todo lo que hay en el módulo */
//export * as bar  from './texto.ts;
