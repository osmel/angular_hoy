"use strict";
exports.__esModule = true;
var Personas = /** @class */ (function () {
    function Personas(name) {
        this.nombre = name;
    }
    Personas.prototype.getNombre = function () {
        return this.nombre;
    };
    return Personas;
}());
exports.Personas = Personas;
var otraCLase = /** @class */ (function () {
    function otraCLase() {
    }
    otraCLase.prototype.otra1 = function () {
        return 'otra1';
    };
    otraCLase.prototype.otra2 = function () {
        return 'otra2';
    };
    return otraCLase;
}());
