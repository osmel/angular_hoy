class otraCLase5 {
	    prueba1(): string{
	        return 'prueba1';
	    }    
	    prueba2(): string{
	        return 'prueba2';
	    }    

}

export default class otraCLase6 {  //default cuando necesites que sea una unica cosa q vaya a exportar y que quieres q sea el por defecto
	    prueba3(): void{
	        console.log('mi clase 6');
	    }    
	    prueba4(): string{
	        return 'prueba4';
	    }    

}


/* O bien exportamos todo lo que hay en el módulo */
//export * as bar  from './texto.ts;