"use strict";
exports.__esModule = true;
var otraCLase3 = /** @class */ (function () {
    function otraCLase3() {
    }
    otraCLase3.prototype.prueba1 = function () {
        return 'prueba1';
    };
    otraCLase3.prototype.prueba2 = function () {
        return 'prueba2';
    };
    return otraCLase3;
}());
var otraCLase4 = /** @class */ (function () {
    function otraCLase4() {
    }
    otraCLase4.prototype.prueba3 = function () {
        console.log('prueba3');
    };
    otraCLase4.prototype.prueba4 = function () {
        return 'prueba4';
    };
    return otraCLase4;
}());
exports.otraCLase4 = otraCLase4;
/* O bien exportamos todo lo que hay en el módulo */
//export *   from './texto';
