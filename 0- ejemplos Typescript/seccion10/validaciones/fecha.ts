interface IUser{
	    nombre: string;
	    salario?: number; //opcional, no es obligatorio
	    getNombre(): string;
}


class Personas implements IUser{
	    nombre: string;
	    constructor(name: string) {
	        this.nombre= name;
	    }
	    getNombre(): string{
	        return this.nombre;
	    }    
}

class otraCLase {
	    otra1(): string{
	        return 'otra1';
	    }    
	    otra2(): string{
	        return 'otra2';
	    }    

}


export {IUser, Personas}; //multiples elementos exportados

			

