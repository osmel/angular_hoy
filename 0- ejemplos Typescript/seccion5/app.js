/*
- Aprenderemos el ¿por qué siempre compila a JavaScript?
- Para que nos puede servir el archivo de configuración de TypeScript
- Realizaremos depuración de errores directamente a nuestros archivos de TypeScript
- Removeremos todos los comentarios en nuestro archivo de producción.
- Restringiremos al compilador que sólo vea ciertos archivos o carpetas
- Crearemos un archivo final de salida
- Aprenderemos a cambiar la version de JavaScript de salida



***descripción del fichero   "tsconfig.json"
    http://www.typescriptlang.org/docs/handbook/tsconfig-json.html
    

***parametro para compilar, se puede especificar en el "tsconfig.json" o ejecutarlo directamente en la consola
 ejemplo :
            tsc *.ts --watch      		   //para observarlo constantemente
            tsc *.ts --target es6    //para que compile el proyecto a es6

            tsc *.ts --sourceMap true //crea un archivo .map que es como un "json" que me permite en el inspector en "source" hacer depuración marcandolo con un "breakpoint"(punto de quiebre) al .ts que se muestra
            hacemos continuación con las herramientas del  "debuger"


            tsc *.ts --removeComments true //para remover todos los comentarios multilineas que hay en el codigo
                    podemos hacer que ignore determinados comentarios, que necesitamos que no borre (!)
  




  http://www.typescriptlang.org/docs/handbook/compiler-options.html


*/
//////////tsconfig.json//////////////// http://www.typescriptlang.org/docs/handbook/tsconfig-json.html
//  Es un archivo q le indica a typescript, las reglas generales para la compilación
{
    "compilerOptions";
    {
        "target";
        "es5", /* Version de javascript para la que trabajamos: 'ES3' (default), 'ES5', 'ES2015', 'ES2016',
                                                    'ES2017','ES2018' or 'ESNEXT'. */
            "module";
        "commonjs", /* Especifica el importador de modulo: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', or 'ESNext'. */
            "strict";
        true, /* Enable all strict type-checking options. */
            "esModuleInterop";
        true;
        "noImplicitAny";
        true, /*Sino especificamos una propiedad va a ser del tipo any*/
            "sourceMap";
        true, /* Para la depuración genera un fichero '.map'. */
            "strictNullChecks";
        true, /* va a ser más estricto con los valores null e indefinido. Va a evitar que podamos asignar estos
                                                valores a los tipos de variables definidas */
            "removeComments";
        true, /*Remueve los comentarios cuando compila, y entonces en .js no llegan los comentarios*/
            "outFile";
        "build/main.js",
        ; /* archivo de salida que combina un conjunto de archivo en un solo archivo */
    }
    /*
          como le decimos a typescript, compilame todo lo que este dentro de la carpeta app/
          y excluyeme lo que este dentro de node_module
           no es una regla del compilador, pero si es una regla que se incluira dentro de tsconfig.ts
  
    *Estas no son reglas del compilador, pero sirven para incluir o excluir carpetas que no quieren o que quieren
    que compilen, es decir no se crearan los .js en las carpetas excluidas */
    "include";
    [
        "app/**/*" /*incluye todas las carpetas(**) que estan dentro de app y todos los archivos(*)   */
    ],
        "exclude";
    [
        "node_module",
        "src/"
    ];
}
//////////outfile////////////////    
Archivo;
que;
podemos;
generar, que;
Junta;
en;
un;
unico;
archivo, todo;
lo;
que;
especifiquemos;
en;
un;
archivo;
de;
salida;
que;
combina;
todos;
los;
archivos;
Normalmente;
hariamos;
esto
    < script;
src = "app.js" > /script> 
    < script;
src = "app1.js" > /script> 
    < script;
src = "app2.js" > /script> ;
tsc--;
outFile;
nuevoArchivo.js;
app1.ts;
app2.ts;
app2.ts; //mete en main.js todos los archivos que se quieren unir
