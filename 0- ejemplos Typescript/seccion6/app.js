/*Diferencia entre declarar variables con VAR y con LET
Uso de constantes
Plantillas literales
Funciones de flecha
Destructuración de objetos
Destructuración de Arreglos
Nuevo ciclo, el FOR OF
Conocer sobre la programación orientada a objetos
Clases

  Let sustituye al var, y es más comodo utilizarlo por los ambitos
  const algo similar en los ambitos, solo se puede redefinir en los tipos de objetos
*/
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/*uso de let en diferentes ambitos*/
var uno = 1;
//let uno = 2; no se puede redeclarar al no ser que esten en diferentes ambitos. lo correcto sería seguir asignando sin let
uno = 2;
//diferentes ambitos, los valores son diferentes cuando se compila cada ambito le da un nombre diferentes a las variables
if (true) {
    var uno_2 = 3; //uno_1
    if (true) {
        var uno_3 = 4; //uno_2
    }
}
console.log(uno); //valor es 2
/*constantes*/
var opciones = "activo";
// opciones="sad";  esto es un error porque una constante solo puede tomar un único valor
console.log(opciones);
for (var _i = 0, _a = [1, 2, 3, 4]; _i < _a.length; _i++) { //por cada iteración se crea un nuevo ambito, es decir un nuevo scope I, I_1, I_2, I_3
    var I = _a[_i];
    console.log(I);
}
//donde unico me permite cambiar los valores de una constante es en las propiedades de un objeto
var miobjeto = {
    nombre: "osmel",
    edad: 20,
    sexo: true
};
miobjeto.nombre = "calderón";
console.log(miobjeto);
/*template literales*/
function funcion() {
    return 'fidel';
}
var minombre1 = "duvi";
var minombre2 = 'alex';
//barticks
var mensaje = "1. new mensaje\n\t\t\t2. Hola: " + minombre1 + "\n\t\t\t3. Hola: " + minombre2 + "\n\t\t\t3. funci\u00F3n: " + funcion() + "\n\t\t\t3. resultado: " + (6 + 7) + "\n\t\t";
console.log(mensaje);
/*función de flechas*/
var resultado1 = function (a, b) { return a + b; }; //resultado simple
var resultado2 = function (a, b) {
    a = a + 2;
    return a + b;
};
// NOTA***ventaja de usar funciones de flechas					
var objeto_nuevo = {
    nombre: "osmel",
    edad: 41,
    mifunc: function () {
        //this.nombre="pepe";
        /*
            setTimeout(function(){
                console.log(this.nombre);  //devuelve undefined, porque el contexto cambia al ser una función normal, y como es sincronica se pierde el contexto que esta afuera
            },1000);
        */
        var _this = this;
        setTimeout(function () {
            console.log(_this.nombre); //para que no se pierda el contexto que esta fuera de la función asincronica, pues la función la cambiamos por una 
            //funcion de flecha, y por tanto se mantiene vivo el contexto de arriba
        }, 1000);
    }
};
objeto_nuevo.mifunc();
/* Destructuracion de los objetos*/
var objeto = {
    nombre: "osmel",
    edad: 20,
    sexo: true
};
/* Antigua forma de asignar valores seria:*/
var nombre1 = objeto.nombre;
var edad1 = objeto.edad;
var sexo1 = objeto.sexo;
/* Destructuracion para asignar valores de forma multiple:
    let {}  --> no importa el orden siempre q las nuevas propiedades tengan igual nombre
    ejemplo debajo equivale a lo de encima
    cuando usamos : en destructuracion significan alias no tipos
*/
var miNombre = objeto.nombre, edad = objeto.edad, sexo = objeto.sexo; // nombre lo cambio por "miNombre"
console.log(miNombre, edad, sexo); //osmel 20 true
/* Destructuracion de los array*/
var arreglo1 = ["osmel", "duvi", "alex", "fidel"];
/* Es secuencial coincide con las posiciones */
var uno_1 = arreglo1[0], dos_1 = arreglo1[1], tres_1 = arreglo1[2], cuatro_1 = arreglo1[3];
/* para solo referenciar uno, tenemos q dejar vacio a los otros */
var tres_11 = arreglo1[2]; //aqui solo estamos referenciando a "Alex" ->3ra posicion
console.log(tres_11);
var persona1 = {
    nombre: "duvi",
    edad: 11,
};
var persona2 = {
    nombre: "alex",
    edad: 9,
};
var persona3 = {
    nombre: "fide",
    edad: 3,
};
var arreglo = [persona1, persona2, persona3];
for (var i in arreglo) { //ciclo con in 
    console.log(arreglo[i].nombre);
}
for (var i = 0; i <= arreglo.length - 1; i++) { //ciclo estandar
    console.log(arreglo[i].edad);
}
for (var _b = 0, arreglo_1 = arreglo; _b < arreglo_1.length; _b++) { //*****nuevo ciclo  for of
    var variableAuxiliar = arreglo_1[_b];
    console.log(variableAuxiliar.edad);
}
/*clases*/
var Miclase = /** @class */ (function () {
    function Miclase(nombre, edad) {
        this.nombre = nombre;
        this.edad = edad;
    }
    return Miclase;
}());
var otraClase = /** @class */ (function (_super) {
    __extends(otraClase, _super);
    function otraClase(nombre, edad, sexo) {
        var _this = _super.call(this, nombre, edad) || this;
        _this.sexo = sexo;
        return _this;
    }
    return otraClase;
}(Miclase));
//instanciar la clase -> crear el objeto 
var yo = new Miclase("osmel", 42);
var otro = new otraClase("osmel", 42, true);
console.log(otro);
