Crear clases en TypeScript
Constructores
Accesibilidad de las propiedades:
	Públicas
	Privadas
	Protegidas
Métodos de las clases que pueden ser:
	Públicos
	Privados
	Protegidos
Herencia
Llamar funciones del padre, desde los hijos
Getters 
Setters
Métodos y propiedades estáticas
Clases abstractas
Constructores privados.


Public: (por defecto). Sino se le especifica un ambito a  una variable. Por defecto será public. Puede ser accesado desde cualquier parte del programa. Es decir donde la clase esta instanciada, puedo accesar y cambiar los valores 
protected: Solo puede ser accesada dentro de su propia clase, o dentro de sus subclases(herencia). Es decir dentro del constructor o sus funciones de esta clase o sus herederas
private: Solo puede ser accesada dentro de su propia clase. Es decir dentro del constructor o sus funciones de esta clase.

Los metodos tambien tienen un  ambito de public, protected o private. Que significa que es el alcance de este dentro de una APP

type miObjeto= {  
	nombre:string, 
 	edad:number, 
 };


class Persona {
    public nombre: string;
    protected edad: number=30;
    private hijos:string[] = ["duvi", "alex", "fidel"];
    
    constructor(nomb: string) {
        this.nombre = nomb;
    };

   public  getNombre() {
   	  this.cambiar(); // haciendo referencia a un metodo privado
        return "Hola mi nombre es, " + this.nombre;
    };

   private  cambiar() {
        return "cambio ";
    }; 

     protected  protegida() {
        return "cambio ";
    }; 
}

class Alumno extends Persona { //herencia. Cuando la clase hija no define un constructor, entonces toma por defecto el constructor de la clase padree

}


class Trabajador extends Persona { //herencia. Cuando la clase hija no define un constructor, entonces toma por defecto el constructor de la clase padree

	
	constructor(){
		super("minombre"); //los constructores de clases heredadas, deben tener un llamado super(para poder llamaar al constructor padre)
		super.protegida(); //esta es la forma de llamar a una funcion del padre que esta protegida.
	}
}


let minombre = new Persona("osmel");
let miAlumno = new Alumno("osmel"); //Cuando la clase hija no define un constructor, entonces toma por defecto el constructor de la clase padree
let miTrabajo = new Trabajador();

minombre.nombre




//////////////////////////////////

Get y sets, es una forma de accesar a propiedades pero de una forma controlada, es decir q no sean publicas. Buenas practicas
   - En este caso son funciones especiales
   	 	-  el get: aunque le declares que regresa void, TS espera que regrese algo, por tanto no admite void.
 		-  setter. Es obligatorio pasar un parametro, Y esta generalmente no retorna nada


		class Persona {
		    public nombre: string;
		    protected edad: number=30;
		    private hijos:string[] = ["duvi", "alex", "fidel"];
		    
		    private _sexo:boolean;
		    
		    constructor(nomb: string) {
		        this.nombre = nomb;
		        this._sexo =true;
		    };

		    get sexo():boolean {
		    	return this._sexo;
		    }
		     set sexo(misexo:boolean) {
		    	 this._sexo=misexo;
		    }
		}

		let minombre = new Persona("osmel");
		console.log( minombre.sexo); //es un getter
		minombre.sexo=true; //un setter

//////////////////////////////////
Los metodos y propiedades estaticas: Son propias de la clase. No se necesita instanciar la clase para usarla
Se usa de esta manera directamente
	clase.propiedad;
	clase.metodo();


	static: Para crear métodos de clase y variables de clase
	Métodos de clase:
	 - Tienen acceso solo a los atributos de clase.
	 - No es necesario crear un objeto para poder utilizar estos métodos.
	 - Para acceder a estos métodos se invoca de la siguiente manera:  Clase.metodo;
	Atributos de clase: Estos solo reservan un espacio de memoria para la clase. Son variables de la clase, no necesitan que los objetos sean creados para que exista.


		class Persona {
		    static cantidad: number=0;
		    
		    constructor() {
		      
		    };

		    static cantidades() {
		    	this.cantidad++;
		    	return this.cantidad;
		    }
		     
		}

		console.log( Persona.cantidades() );
		console.log( Persona.cantidades() );

//////////////////////////////////Clases Abstractas

Estas clases no se pueden instanciar. Son clases abstractas donde se definen propiedades y metodos abstractos, que serviran para sus herederos


			 Las clases abstractas:
			 	 Tiene directrices:
			 -Si en una clase hay un método abstracto, la clase esta obligada a ser declarada abstracta
			 -Todos sus métodos no tienen que ser abstractos.
			- Las subclases que heredan de una clase abstracta, estan obligada a redefinir e implementar los métodos abstractos.
			- Los métodos abstractos no llevan nada en su interior solo se definen
			  

			  Ahora, el ejemplo más claro. Para que se usan?
			       
			       abstract class Animal {
			    abstract  ruido():void;
			}

			class gato extends Animal {
				ruido():void{
					console.log("Maulla");
				}
			}


			class perro extends Animal {
				ruido():void{
					console.log("Ladra");
				}
			}

			class caballo extends Animal {
				ruido():void{
					console.log("Ruge");
				}
			}


			class persona extends Animal {
				ruido():void{
					console.log("Habla");
				}
			}

			//1- Declaración de un array de objeto de tipo Animal
			     let miAnimal:Animal[]= new Array(3); //solo crea una referencia a un tipo abstracto

					//referencia a diferentes tipos de objetos que son subclases de Animal
					//2- Principio de sustitución
			 		//Estamos almacenando un objeto de la subclase, cuando el programa espera que se almacene un objeto de la superclase

			     	miAnimal[0]= new perro();
			     	miAnimal[1]= new gato();
			     	miAnimal[2]= new caballo();

			//3- casting o refundicion de objetos:
			//Es la forma que tenemos de usar métodos de la subclase, cuando estamos en presencia de una variable de la superClase. Ejemplo:

			     (miAnimal[0] as Animal).ruido(); //casting


			  //Typescript es capaz de saber en tiempo de ejecución a que método llamar al de la clase padre o hijo. Cuando hay metodos redefinidos
			  for (let variableAuxiliar of miAnimal) {
				variableAuxiliar.ruido();
			  }