- Objetos básicos
- Crear objetos con tipos específicos
- Crear métodos dentro de objetos
- Tipos personalizados
- Crear variables que soporten varios tipos a la vez.
- Comprobar el tipo de un objeto.
  
  http://www.typescriptlang.org/docs/handbook/classes.html

//puedo definirlo así

let mivariable: { nombre:string, edad:number, hijos:string[], getNombre:()=>string } = {
    nombre: "osmel",
    edad: 41,
    hijos: ["duvi", "alex", "fidel"],
    
    getNombre(){
    	return this.nombre;
    }
}
////////////////////////
//type: Nos permite crear tipos personalizados
type miObjeto= {  nombre:string, 
 	edad:number, 
 	hijos:string[], 
 	getNombre:()=>string 
 };

let miotravariable: miObjeto = {
    nombre: "osmel",
    edad: 41,
    hijos: ["duvi", "alex", "fidel"],
    
    getNombre(){
    	return this.nombre;
    }
}

////////////////////////////
////////////////////////clases

class Persona {
    nombre: string;
    edad: number=30;
    hijos:string[] = ["duvi", "alex", "fidel"];
    
    constructor(nomb: string) {
        this.nombre = nomb;
    };

    getNombre() {
        return "Hola mi nombre es, " + this.nombre;
    };
}

let minombre = new Persona("osmel");