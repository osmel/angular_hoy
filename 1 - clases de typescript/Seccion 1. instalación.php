https://www.typescriptlang.org/index.html#download-links

Instalar o actualizar si esta instalada
  sudo npm install -g typescript  (-g instalar de forma global)
version
  tsc -v
/////////////////////////////////////////

compilar 
  tsc proyecto.ts   
Es necesario compilar el archivo .ts, para que genere un archivo .js porque los navegadores solo entiende .js,       no        .ts


 En TS existe el modo observador(es decir ts va a estar pendiente a cualquier cambio para convertir el fichero en .js automaticamente)   --watch  o -w. Va a iniciar un servicio, que va a estar pendiente a cualquier cambio
 
 tsc proyecto.ts   --watch 

/////////////////////////////////////////
crear un proyecto TS(este crea un fichero tsconfig.json)
 cd typescript
 tsc -init

para escuchar todos los archivos, puedo hacerlo con 
tsc *.ts -w  (esto es posible porq hay un proyecto ts creado con init)


/////////////////////////////////////////Primer proyecto
mkdir ejercicios
cd ejercicios
tsc -init  //creando el proyecto. crea un "tsconfig.json"

<!-- index.html -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Introducción</title>
  </head>
  <body>
        <script src="app.js"></script> 
  </body>
</html>


<!-- app.ts -->
  console.log('probando'); 
   nota:compilamos el archivo para que se genere un app.js 

  tsc app.ts   (compilamos todos app.ts)
  tsc *.ts     (compilamos todos los .ts) 
  tsc *.ts -w   (--watch compilamos todos los .ts y les hacemos seguimientos, es decir modo observador)

