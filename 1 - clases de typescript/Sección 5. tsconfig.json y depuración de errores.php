- Aprenderemos el ¿por qué siempre compila a JavaScript?
- Para que nos puede servir el archivo de configuración de TypeScript
- Realizaremos depuración de errores directamente a nuestros archivos de TypeScript
- Removeremos todos los comentarios en nuestro archivo de producción.
- Restringiremos al compilador que sólo vea ciertos archivos o carpetas
- Crearemos un archivo final de salida
- Aprenderemos a cambiar la version de JavaScript de salida

//////////tsconfig.json//////////////// http://www.typescriptlang.org/docs/handbook/tsconfig-json.html
  Es un archivo q le indica a typescript, las reglas generales para la compilación

  {
  "compilerOptions": {
    "target": "es5",                          /* Version de javascript para la que trabajamos: 'ES3' (default), 'ES5', 'ES2015', 'ES2016', 
    											'ES2017','ES2018' or 'ESNEXT'. */
    "module": "commonjs",                     /* Especifica el importador de modulo: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', or 'ESNext'. */
    "strict": true,                           /* Enable all strict type-checking options. */
    "esModuleInterop": true    
    "noImplicitAny": true,     /*Sino especificamos una propiedad va a ser del tipo any*/
    "sourceMap": true,                     /* Para la depuración genera un fichero '.map'. */
    "strictNullChecks": true,              /* va a ser más estricto con los valores null e indefinido. Va a evitar que podamos asignar estos 
    										valores a los tipos de variables definidas */

	"removeComments": true,      /*Remueve los comentarios cuando compila, y entonces en .js no llegan los comentarios*/    				

	"outFile": "build/main.js", /* archivo de salida que combina un conjunto de archivo en un solo archivo */

  }, /*Estas no son reglas del compilador, pero sirven para incluir o excluir carpetas que no quieren o que quieren que compilen */
  
  "include": [
  	"app/**/*"  /*incluye todas las carpetas(**) que estan dentro de app y todos los archivos(*)   */
  ],
  "exclude": [
    "node_module",
  	"src/"
  ]

}

***NOTA: TODAS estas configuraciones se pueden correr desde la consola ejemplo, para quitar los comentarios usaremos
tsc fichero.ts --removeComments
  podemos hacer que ignore determinados comentarios, que necesitamos que no borre (!)
  /*!
  */

tsc fichero.ts --target es6 //para que compile el proyecto a es6

//////////Depuración del codigo tsc//////////////// 
  47 
  es posible con un archivo .map
   declro este en tsconfig
   "sourceMap": true,

   y me voy a "source" en el inspector y con un dobleCLick, puedo hacer una marca o breackPoint


//////////outfile////////////////    
  Archivo que podemos generar, que Junta en un unico archivo, todo lo que especifiquemos
  en un archivo de salida que combina todos los archivos

Normalmente hariamos esto
  <script src="app.js"></script> 
  <script src="app1.js"></script> 
  <script src="app2.js"></script> 


 tsc --outFile main.js app1.ts  app2.ts  app2.ts   //mete en main.js todos los archivos que se quieren unir
