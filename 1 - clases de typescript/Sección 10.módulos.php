https://programacionymas.com/blog/modulos-javascript-commonjs-amd-ecmascript

TypeScript nos da la posibilidad de crear aplicaciones que permitan importar funciones, clases, propiedades, variables o constantes de otros archivos del proyecto sin necesidad de agregar las referencias en nuestras páginas HTML.

La necesidad de los módulos
Importadores de módulos, puntualmente SystemJS
Haremos las configuraciones necesarias para poder trabajar con módulos
Importar y exportar módulos
Consideraciones que se deben de tener en cuenta al trabajar con módulos



SystemJs: -cargar módulos en el cliente
Se encargará de cargar los módulos (.ts) por demanda y obtener el archivo js cuando sea necesario en el proyecto

https://blog.makeitreal.camp/manejadores-de-paquetes-en-nodejs/

	Si vas a utilizar el paquete(libreria) en un proyecto debes instalarlo de forma local, 
	en la carpeta donde se encuentren los archivos del proyecto.




//actualizar la version npm
	sudo npm install -g npm 

//instalar (debe crearse una carpeta node_modules y un fichero package-lock.json en la misma carpeta en 
  que se instala, sino checar que en otras carpetas a un nivel por encima, ya existe el node_modules)

	sudo npm install systemjs  //este me crea un package.json
		o más especifico
	sudo npm install systemjs@0.19.41



https://www.mmfilesi.com/blog/typescript-5-modulos/

Dentro de un archivo, solo aquello que se "exporta" es accesible desde fuera (aunque sí desde dentro) y para usarlo en otro archivo hay que "importarlo". 

//////////Exportación//////////////

foo.ts
			interface Foo {
				bazinga: string;
			}
			otro: function() {
				console.log('foo');
			}

			noexporta: function() {
				console.log('foo');
			}

			class Bazinga {
				bar: string;
				constructor(bar) {
					this.bar = bar;
				}
			}

			/* O bien exportamos los elementos uno a uno */
			export { otro };
			export { Bazinga, Foo }; //varios elementos

			/* O bien exportamos todo lo que hay en el módulo */
			export * from './foo';

//////////Importación//////////////


app.ts
	import { otro } from './foo';
	// esto funciona
	otro();

	// no funciona porque no fue exportado, ni importado
	noexporta();


	//Importar por separado cada parametro
		import { variables, clases, interfaces, etc } from './ficheroAImportar';

	//Importar usando comodines (*) metiendo el contenido en un alias que usaremos a futuro para los elementos del modulo (en este caso usamos Bar)
	
		import * as Bar from './foo';
			let instFoo = new Bar.Foo();
			instFoo.meth();	



Introducción a system.js
	Como ya sabemos, typeScript solo sirve en desarrollo, cuando estamos escribiendo código, pues luego se transpila, se traduce, en javaScript normal. Según el formato en que estemos volcando el código final traducirá el sistema de módulos de una manera u otra. Si es para ecma6, usará el sistema de módulos nativo de js; si es para ecma5, utilizará common js, pero podemos modificar estos valores por defecto en las compilerOptions del tsconfig.json. La clave a definir se denomina module y los valores que puede tener son:

	'none'
	'commonjs'
	'amd' (el de require)
	'system'
	'umd'
	'es6'
	'es2015'


	Según el modo escogido, tendremos o no que añadir al proyecto alguna librería complementaria. 

	System: - es compatible con ecma5
		    - permite que ts concatene todos los archivos en uno solo final (indicando true en el parámetro outFile del tsconfig.json). 

 1 - descargamos la librería systemjs con npm desde el directorio donde tengamos nuestro proyecto ts.
      npm install systemjs



 index.html: 
    
<!--llamar al paquete systemjs -->
<script src="./node_modules/systemjs/dist/system.js"></script> 

<script>
		 /*1- Denifimos la configuración. */
				SystemJS.config({
				 baseUrl:'/'
				});

		/*2- Que solo añada las terminación .js a todos los archivos */
				System.defaultJSExtensions = true;

		/*3- Cargamos el archivo principal (que funciona como una promesa */
				SystemJS.import('./app.js')
				   .then(null, console.error.bind(console));

</script> 



NOTA: Como system js carga los archivos mediante ajax, para que funcione debemos levantar un servidor, ya sea este un "apache", un "http-server", un "lite-server" o el que sea, pues de lo contrario nos dará un problema de cors.

