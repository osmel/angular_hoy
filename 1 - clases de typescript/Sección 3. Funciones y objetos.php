Declaraciones básicas de funciones
Parámetros obligatorios
Parámetros opcionales
Parámetros por defecto
Parámetros REST
Tipo de datos "Function"

///////////////////////////////////////////// http://www.typescriptlang.org/docs/handbook/functions.html

//funcion nombrada
function imprime():string {     //funcion retornando una cadena
	return 'cadena';
}

//funcion anonima
let mifuncion1 = function():string { 
	return "otro datos";	
}


tipos de parametros
	  -Obligatorio    -->nombre:string
	  -opcional( ? )  -->edad?:string  //puede mandarlo o no
	  -por defecto    -->sexo:boolean=true
	  -parametros rest  --> ...losDemasParametros:string[]

	function imprime(nombre:string,edad?:string, sexo:boolean=true, ...losDemasParametros:string[] ):string {    
		
		console.log( losDemasParametros.join(" ") ); //unimos con un espacio

		if (edad){ //para controlar si es indefinido

		}

		return 'cadena';
	}

    imprime('osmel', 20, true, "uno", "dos", "tres");  // "uno", "dos", "tres" -->los recibe como un arreglo

/////////////////////////////////////////////
Tipo de datos "Function"
 Tipo funcion (31)    

		  let mifuncion = function(x: number, y: number): number { return x + y; };

		  function sumar(x: number, y: number): number   { 
		  	return x + y; 
		  };
		  function saludar(msg: string): void   { 
		  	console.log( msg );
		  };
		  
		  let mifuncion = sumar;
		   console.log(  mifuncion(2,3)  );
		  let mifuncion = saludar;
		  mifuncion("saludando");


		///////Definiendo reglas para las funciones...
		/////podemos hacer restricciones a las funciones, para q se limiten a solo el tipo que queremos

		let mifuncion: (a:number, b:number) => number;   //solo acepta numero y retorna numero
		//esta si me la va aceptar con la restriccion de encima, porque cumple con las expectativa q espera la función
		mifuncion = sumar;
		console.log(  mifuncion(2,3)  );