Sección 2, Clase 9
 Tipos de datos  http://www.typescriptlang.org/docs/handbook/basic-types.html		  

		Primitivos 
			Booleanos. (true o false)
			Números.
			Strings. "", '', backtick ´´
			Null -->tipo especial
			Undefined -->tipo especial

			Tipo Any.
			Arreglos.
			Tuplas.
			Enumeraciones
			Retorno void

		Compuestos	
		   Objetos Literales ( propiedades y metodos)
		   Clases   (class)
		   Funciones  (function)


		Se pueden crear tipos nuevos
		  Interfaces
		  tipos genericos

   
//////////////////////
Booleanos
		let mivariable: boolean = false; (forma recomendada)
		let mivariable: boolean;
		let mivariable = true; (no recomendada)

Números
		let decimal: number = 6;
		let hex: number = 0xf00d;
		let binary: number = 0b1010;
		let octal: number = 0o744;

string
		let color: string = "blue";
		color = 'red';

		//backtick respeta espacio y todo, ademas usa valores como plantillas	
		let fullName: string = `Bob Bobbington`; 
		let age: number = 37;
		let sentence: string = `Hello, my name is ${ fullName }.  
								I'll be ${ age + 1 } years old next month.`; 

Any (puede ponerse cualquier valor, va mutando de variable según le vas asociando nuevos valores)
	let mivariable: any;

	mivariable="texto";
	console.log(mivariable.charAt(0)); //t
	mivariable=150.555;
	console.log(mivariable.toFixed(2)); //150.56
	mivariable=true;
	console.log(mivariable);  //true
Tuberias
	mivariable: number | string = "texto"; //o pudo ser un número

Array
		let lista: number[] = [1, 2, 3];
		let lista: Array<number> = [1, 2, 3];  //usa un tipo genérico de matriz Array<elemType>

		let lista = [1, 2, 3];  //aqui deduce qe es un arreglo 

Tuplas (es el arreglo, pero con varios tipos)
	   
	     // Declarando tipo de tuplas
		let x: [string, number]= ["hello", 10];
		x[0] = 'texto';
		x[1] = 2;
		

Enum (son pares de valores ordenados, donde me puedo regerir tanto al numero como a la cadena)

	  enum Color {Red, Green, Blue} //toma valores por defecto  Red=0, Green=1, Blue=2
	  object {0:"Red", 1:"Green",2:"Blue",Blue:2,Green:1,Red:0}
	  console.log(Color[1] ); //Green
	  console.log(Color.Green); //1

	  let c: Color = Color.Green;  //este es 1
	  let mivariable: number = Color.blue;  //este es 2
	  console.log(Color); 

	  enum Color {Red = 1, Green = 2, Blue = 4}  //aqui especifique los valores
	  let c: Color = Color.Green;   //esto es 2

	  enum Color {Red = 1, Green, Blue=10}  //Green es 2, porque lo que hace es sumarle un 1 al valor antecesor
		let colorName: string = Color[2]; 

		console.log(colorName);


Void   //opuesto al any=cualquier tipo, y void es ningún tipo

		function warnUser(): void {   //esta función no regresa nada, es decir regresa undefined
		    console.log("texto");
		    //es incorrecto usar un "return"
		}		

Never	 //representa un valor que nunca puede suceder
			//es decir, si la aplicación llega a este punto, debe terminar. Porque significa que algo fallo
			function error(message: string): never {
			    throw new Error(message);
			}

			Hace un punto critico en nuestro programa.
			error('mi aplicacion debe terminar');



casteando valores
	  let mivariable: any = "osmel calderon";			
	  let largo: number = (<string>mivariable).length;  //casteando como generico
	  let largo: number = (mivariable as string).length;  //otra forma de castear

Null y Undefined

		let indefinido: undefined = undefined;
		let nulo: null = null;

		indefinido=null; //es valido






//////////////////////////checando el tipo de datos
let cosa:any =123;
console.log(typeof cosa); //esto regresa el tipo de datos en string
if (typeof cosa =="number") {
	console.log("cosa es un número");
}