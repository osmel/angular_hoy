Diferencia entre declarar variables con VAR y con LET
Uso de constantes
Plantillas literales
Funciones de flecha
Destructuración de objetos
Destructuración de Arreglos
Nuevo ciclo, el FOR OF
Conocer sobre la programación orientada a objetos
Clases

  Let sustituye al var, y es más comodo utilizarlo por los ambitos
  const algo similar en los ambitos, solo se puede redefinir en los tipos de objetos



/*uso de let en diferentes ambitos*/

      let uno:number = 1;
      //let uno = 2; no se puede redeclarar al no ser que esten en diferentes ambitos. lo correcto sería seguir asignando sin let
        uno=2; 

      //diferentes ambitos, los valores son diferentes cuando se compila cada ambito le da un nombre diferentes a las variables
      if (true) {
        let uno:number = 3;  //uno_1
        if (true) {
          let uno:number = 4; //uno_2
        }
      }

      console.log(uno); //valor es 2


/*constantes*/
    const opciones:string ="activo";
      // opciones="sad";  esto es un error porque una constante solo puede tomar un único valor

    console.log(opciones);


    for (const I of [1,2,3,4]) { //por cada iteración se crea un nuevo ambito, es decir un nuevo scope I, I_1, I_2, I_3
      console.log(I);
    }


    //donde unico me permite cambiar los valores de una constante es en las propiedades de un objeto

    const miobjeto = {
       nombre: "osmel",
       edad : 20,
       sexo : true
    }

    miobjeto.nombre="calderón";

    console.log(miobjeto);


/*template literales*/

    function funcion():string {
      return 'fidel';

    }

    let minombre1:string = "duvi";
    let minombre2:string = 'alex';
       
       //barticks
    let mensaje:string = `1. new mensaje
      2. Hola: ${minombre1}
      3. Hola: ${minombre2}
      3. función: ${funcion()}
      3. resultado: ${6+7}
    `;

    console.log(mensaje);


/*función de flechas*/

      let resultado1 = (a,b) => a+b;  //resultado simple

      let resultado2 = (a,b) => {     //resultado apartir de otras operaciones {}
                  a= a+2; 
                  return a+b;
                }

      // NOTA***ventaja de usar funciones de flechas          

      let objeto_nuevo = {
          nombre: "osmel",
          edad: 41,
          mifunc: function(){
            //this.nombre="pepe";
            /*
              setTimeout(function(){
                console.log(this.nombre);  //devuelve undefined, porque el contexto cambia al ser una función normal, y como es sincronica se pierde el contexto que esta afuera
              },1000);
            */

          

          
            setTimeout( () => {
              console.log(this.nombre);  //para que no se pierda el contexto que esta fuera de la función asincronica, pues la función la cambiamos por una 
                            //funcion de flecha, y por tanto se mantiene vivo el contexto de arriba
            },1000);
             
          }

      }

      objeto_nuevo.mifunc();



/* Destructuracion de los objetos*/
      let objeto = {
         nombre: "osmel",
         edad : 20,
         sexo : true
      }

      /* Antigua forma de asignar valores seria:*/
         let nombre1 = objeto.nombre;
         let edad1 = objeto.edad;
         let sexo1 = objeto.sexo;

      /* Destructuracion para asignar valores de forma multiple:
        let {}  --> no importa el orden siempre q las nuevas propiedades tengan igual nombre
        ejemplo debajo equivale a lo de encima
        cuando usamos : en destructuracion significan alias no tipos
      */

      let {nombre:miNombre, edad, sexo} = objeto;   // nombre lo cambio por "miNombre"
      console.log(miNombre, edad, sexo); //osmel 20 true



/* Destructuracion de los array*/

      let arreglo1:string[] = ["osmel", "duvi", "alex", "fidel"];

      /* Es secuencial coincide con las posiciones */
      let [uno_1, dos_1, tres_1, cuatro_1] = arreglo1;


      /* para solo referenciar uno, tenemos q dejar vacio a los otros */
      let [, , tres_11, ] = arreglo1;  //aqui solo estamos referenciando a "Alex" ->3ra posicion

      console.log(tres_11);



///////////////////////////Nuevo ciclo, el "FOR OF"
type miObjeto= {  
	nombre:string, 
 	edad:number, 
 };

let persona1: miObjeto = {
    nombre: "duvi",
    edad: 11,
}

let persona2: miObjeto = {
    nombre: "alex",
    edad: 9,
}

let persona3: miObjeto = {
    nombre: "fide",
    edad: 3,
}


let arreglo:miObjeto[]=[persona1, persona2, persona3]; 

for (let i in arreglo ){
	console.log( arreglo[i].nombre );
}

for (let i=0; i<=arreglo.length-1;i++ ){
	console.log( arreglo[i].edad ); 
}

//nuevo ciclo
for (let variableAuxiliar of arreglo ){
	console.log( variableAuxiliar.edad ); 
}