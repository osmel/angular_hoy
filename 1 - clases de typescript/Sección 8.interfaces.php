¿Por qué es necesario una interfaz?
¿Cómo creamos una interfaz básica?
Crear propiedades opcionales
Crear métodos
Asignar interfaces a las clases


//////////////////////////////////Interface en los parametros de una funcion

interface General{
	nombre:string,
	poder?:string	//?->opcional, esto permite que pasen o no esta propiedad
	regenerar(nombreReal:string):void;
}


//objeto del tipo interfaces
let MiPersona:General = {  //del tipo interface
	 nombre:"osmel",
	 regenerar(nombreReal:string){
	 	 console.log('se ha regenerado'+nombreReal);
	 }
};

function enviar(per: General) { //del tipo interface
	console.log(per.nombre);
	per.regenerar("osmel");
}

function enviarotro(per: General) { //del tipo interface
	console.log(per.nombre);
	per.regenerar("alex");
}



enviar(MiPersona);

enviarotro(MiPersona);



https://www.uno-de-piera.com/interfaces-en-typescript/
https://desarrolloweb.com/articulos/definicion-interfaces-typescript.html

Las interfaces en typescript nos permiten crear contratos que otras clases deben firmar para poder utilizarlos, al igual que en otros lenguajes de programación como java o php, en typescript también podemos implementar más de una interfaz en la misma clase.




//////////////////////////////////Interface en las clases

interface IUsuario{
    nombre: string;
    getnombre(): string;
}


class Usuario implements IUsuario{
    nombre: string;
    constructor(nombre: string) {
        this.nombre= nombre;
    }
    getnombre(): string{
        return this.nombre;
    }    
}
var user = new Usuario("osmel");
console.log(user.getnombre());



//////////////////////////////////Interface en las funciones


interface dosNumeros{
	
	(num1:number,num2:number):number;
}

let sumar:dosNumeros;

sumar = function(num1:number,num2:number){
   return num1+num2;
}

let restar:dosNumeros;

restar = function(num1:number,num2:number){
   return num1-num2;
}












https://www.uno-de-piera.com/interfaces-en-typescript/
https://desarrolloweb.com/articulos/definicion-interfaces-typescript.html

https://medium.com/@jesusmurfontanals/typescript-para-que-sirven-los-types-y-interfaces-y-en-que-se-diferencian-bee0af17fffa

https://www.mmfilesi.com/blog/typescript-4-interfaces/

http://www.luisevalencia.com/2017/07/04/typescript-dia-3-classes-interfaces-y-extension-de-tipos-2/
https://blog.ng-classroom.com/blog/tips/typescript-fundamentos/
http://java-white-box.blogspot.com/2012/05/java-player-que-es-el-polimorfismo-en.html

https://code.i-harness.com/es/docs/typescript/handbook/interfaces



¿ Qué es una interfaz ?
  - Técnicamente, las interfaces son un mecanismo de la programación orientada a objetos que trata de suplir la carencia de herencia múltiple. 
  - Una clase puede extender otra clase, heredando sus propiedades y métodos y declarar que implementa cualquier número de interfaces. 
   
  - porque son como un contrato?
   	 _ La diferencia de las clases que extiendes con respecto a las interfaces es que las interfaces no contienen implementación de sus métodos, por lo que la clase que implementa una interfaz debe escribir el código de todos los métodos que contiene. Por este motivo, se dice que las interfaces son como un contrato, en el que se especifica las cosas que debe contener una clase para que pueda implementar una interfaz o cumplir el contrato declarado por esa interfaz.
   	_ Las interfaces en typescript nos permiten crear contratos que otras clases deben firmar para poder utilizarlos, al igual que en otros lenguajes de programación como java o php, en typescript también podemos implementar más de una interfaz en la misma clase.


  - En TypeScript una interfaz puede definir propiedades, mientras que en otros lenguajes las interfaces sólo definen métodos.

Declarar una interfaz en TypeScript
	Las interfaces en TypeScript se declaran de manera bastante similar a la de las clases, indicando la lista de propiedades y métodos que contendrán. Solo hay un detalle fundamental, que las propiedades no pueden tener valores y los métodos no pueden tener código para su implementación.



//////////////////
* Podemos crear herencia entre interfaces
* podemos implementar múltiples interfaces en la misma clase
* las interfaces es que las interfaces no contienen implementación de sus métodos, por lo que la clase que implementa una interfaz debe escribir el código de todos los métodos que contiene.
* Puede definir propiedades(no pueden tener valores), y métodos(no pueden estar implementadas, no puede tener codigo).
* Usamos Inombre para identificarla
* Si no se cumple el contrato de la interfaz(sino se implementan las propiedades y metodos heredados), entonces nuestro editor se quejará (si está preparado para mostrar los errores de código TypeScript), o el compilador nos lo advertirá.





interface IUniversidad{
  ano_graduado:number
}

interface IProfesion extends IUniversidad{ //herencia entre interfaces
	 graduado:string;
}

interface IEntidad{
	 ubicacion:string;
}


interface IUser{
    nombre: string;
    salario?: number; //opcional, no es obligatorio
    getNombre(): string;
}


class Personas implements IUser{
    nombre: string;
    constructor(name: string) {
        this.nombre= name;
    }
    getNombre(): string{
        return this.nombre;
    }    
}

class Alumno extends Personas {

}

class Trabajador extends Personas implements IProfesion, IEntidad{  //multiples implementaciones de multiples interfaces
	graduado:string='informatica';
	ubicacion:string='cdmx';
	ano_graduado:number = 2000;

    constructor(name: string) {
        super(name);

    }

}

var user = new User("iparra");
console.log(user);



La interfaz como un nuevo tipo: Se puede definir tambien para usarse como un nuevo tipo.
   actúa como validadora de la variable, de que todo se haga conforme a lo esperado (según la interfaz).
    Se trata de un compromiso, que sirve para:
    	contrato de código es estricto
		Dar consistencia. 
        Clarificar en el caso de las funciones a los parámetros, así como sus tipos (tanto de entrada como de salida)
	    Crear funciones personalizadas y tipos de array
        Definir archivos de definición de tipos para librerias y frameworks
        las posibilidades de error se reducen, tanto por los tipos devueltos como por los parámetros que requieren. De todo esto se encargan las interfaces, lo que hace que nuestro código sea más estricto a la hora de enfocar un proyecto web grande.



interface IEtiqueta {
  fecha?:string; //este no es obligatorio
  etiqueta: string;
  tamano:number;
}

function printLabel(objEtiqueta: IEtiqueta) {
  console.log(objEtiqueta.etiqueta);
}

let myObj = {tamano: 10, etiqueta: "Size 10 Object"};
printLabel(myObj);
