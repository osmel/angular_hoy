	
	class Figura<t extends number | string>  {   //puede enviar numero o string




class Figura<t extends number>  {
	base:T;
	altura:T
	area():number {
	   return this.base *this.altura
	}
}

let cuadrado = new Figura();    //let cuadrado = new Figura<number>();  --> solo puede manejar numeros
cuadrado.base = 10;
cuadrado.altura = 10;
console.log( cuadrado.area() );




/*
sino ponemos genericos, ts no nos avisa en tiempo de compilación de los errores, solo nos envia error
en tiempo de ejecución
  ejemplo 
  console.log( regresar("osmel calderon").toFixed(2)  );
  por tanto vamos a convertir nuestra función en funcion generica
    function regresar<T>(arg:T){     
		return arg;
	}

*/


function regresar<T>(arg:T){
		return arg;
}

console.log( regresar(153.3333).toFixed(2)  );
console.log( regresar("osmel calderon").charAt(1) );
console.log( regresar( new Date() ).getTime()  );

